from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from clavier.models import Serie, Audio, Image, Video
from .models import Profile
from django_recaptcha.fields import ReCaptchaField

class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())
    # captcha = ReCaptchaField()

    class Meta:
        model = User
        
    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username = username, password = password)

            if not user:
                raise forms.ValidationError("Ce nom d'utilisateur n'existe pas !")
            
            if not user.check_password(password):
                raise forms.ValidationError("Ce mot de passe est incorrect !")
        
        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField(required=True)
    captcha = ReCaptchaField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2'] # paramétre l'affichage des champs (ordre et contenu)
        labels = {
            'username':"Nom d'utilisateur",
            'first_name':"Prénom", 
            'last_name':"Nom de famille", 
            'email':"E-mail", 
            'password1':"Mot de passe", 
            'password2':"Confirmation du mot de passe"
        }

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email'] # Ils ne pourront modifier que l'username et l'email pour l'instant

class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']


class CreateSerieForm(forms.ModelForm):
    class Meta:
        model = Serie
        fields = '__all__' # paramètre l'affichage des champs (ordre et contenu)


class UploadAudioForm(forms.ModelForm):
    class Meta:
        model = Audio
        fields = ['nom', 'description', 'upload_by', 'source', 'used_by', 'file']

class UploadVideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ['nom', 'description', 'upload_by', 'source', 'used_by', 'file']

class UploadImageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['nom', 'description', 'upload_by', 'source', 'used_by', 'file']