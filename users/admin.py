from django.contrib import admin
from .models import Profile, Trace, SynthVocTrace, SynthVocRecord, appstats

# TUTO : https://www.youtube.com/watch?v=g5DTIiFAiSk 

class TracesAdmin(admin.ModelAdmin):
    list_display = ('user', 'appli', 'date', 'userAgent', 'userIp', 'content')
    list_filter = ('user', 'date', 'userAgent', 'appli',)
    # fields = ('nom', 'description', 'auteur', 'images', 'phono', 'mots',)

class TracesSynthVoc(admin.ModelAdmin):
    list_display = ('user', 'appli', 'date', 'userAgent', 'userIp', 'lenssml')
    list_filter = ('user', 'date', 'userAgent', 'appli',)
    # fields = ('nom', 'description', 'auteur', 'images', 'phono', 'mots',)

class AppstatsAdmin(admin.ModelAdmin):
    list_display = ('date', 'app', 'module', 'ip', 'agent', 'country', 'lang')
    list_filter = ('date', 'app', 'module', 'ip', 'agent', 'country', 'lang')

admin.site.register(Profile)
admin.site.register(Trace, TracesAdmin)
admin.site.register(SynthVocTrace, TracesSynthVoc)
admin.site.register(SynthVocRecord)
admin.site.register(appstats, AppstatsAdmin)