from django.db import models
from django.contrib.auth.models import User
from PIL import Image
import datetime

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='profile_pics/default.png', upload_to='profile_pics')
    lastTrace = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return f'Profil de {self.user.username}'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size) # resize image with output_size values
            img.save(self.image.path)

class Trace(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    userAgent = models.CharField(max_length=200, default='Anonymous')
    userIp = models.CharField(max_length=50, default='Anonymous')
    date = models.DateTimeField(auto_now_add=True, auto_now=False)
    appli = models.TextField()
    content = models.TextField(default='{}')

    def __str__(self):
        return f'Trace n°{self.id} \n\tapp: {self.appli} \n\tuser: {self.user.username} \n\tdate: {self.date} \n\tcontent: {self.content} \n\tip: {self.userIp} \n\tagent: {self.userAgent}'

class SynthVocTrace(models.Model):
    user = models.CharField(max_length=50, default='Anonymous')
    userAgent = models.CharField(max_length=200, default='Anonymous')
    userIp = models.CharField(max_length=50, default='Anonymous')
    date = models.DateTimeField(auto_now_add=True, auto_now=False)
    appli = models.TextField()
    lenssml = models.IntegerField()

    def __str__(self):
        return f'Trace n°{self.id} \n\tapp: {self.appli} \n\tuser: {self.user.username} \n\tdate: {self.date} \n\tip: {self.userIp} \n\tagent: {self.userAgent}'

class SynthVocRecord(models.Model):
    voix = models.CharField(max_length=1)
    lang = models.CharField(max_length=15)
    ipa = models.CharField(max_length=400)
    audio = models.TextField()
    cptEcoute = models.IntegerField()

    def __str__(self):
        return f'VocRecord n°{self.id} \n\t[{self.ipa}] {self.lang} {self.voix}\n\tCompteur d\'écoutes: {self.cptEcoute}'

class appstats(models.Model):
    date = models.DateTimeField(auto_now_add=True, auto_now=False)
    app = models.CharField(max_length=30)
    module = models.CharField(max_length=30)
    ip = models.CharField(max_length=30)
    agent = models.CharField(max_length=200, default='Anonymous')
    country = models.CharField(max_length=50, default='')
    lang = models.CharField(max_length=10)

    def __str__(self):
        return f'appstat\t[{self.app}] {self.module} {self.lang}'