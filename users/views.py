from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.http import JsonResponse, HttpResponse
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.contrib.auth.models import Group, User

from .forms import UserLoginForm, UserRegisterForm, CreateSerieForm, UserUpdateForm, ProfileUpdateForm, UploadAudioForm, UploadImageForm, UploadVideoForm
from clavier.models import Serie, Audio, Image, Video, SerieTag, SerieVisibility
from .models import Trace, SynthVocTrace, SynthVocRecord, appstats
from .decorators import unauthenticated_user, allowed_users

from user_agents import parse
import json, datetime, csv, os, requests, random, string
import numpy as np

from pydub import AudioSegment
import base64, io

##
## Ce fichier liste les views relatives aux comptes utilisateurs,
## à la création et la gestions des séries, l'enregistrement des traces utilisateurs
##


##########
########## LOGIN, REGISTER etc.
########## 
##########

# Page de LOGIN
@unauthenticated_user
def loginView(request):
    nextPage = request.GET.get('next')
    print("NextPage=",nextPage)
    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username = username, password = password)
        
        if user is not None:
            login(request, user)

            if nextPage:
                return redirect(nextPage)
            return redirect('/')
    
    context = {
        'form': form,
    }
    return render(request, 'users/login.html', context)

# Page d'inscription
@unauthenticated_user
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            print("Enregistrement valide")
            user = form.save()
            username = form.cleaned_data.get('username')

            # Association du nouveau compte au groupe "student" par défaut
            group = Group.objects.get(name="student")
            user.groups.add(group)

            messages.success(request, f'Votre compte a été créé. Bienvenue '+username+' !')

            print("Préparation envoi email")
            template = render_to_string('emails/register_confirmation.html', {'name':form.cleaned_data.get('username')})
            email = EmailMessage(
                'Confirmation de création de compte PhonoGraphe',
                template,
                settings.EMAIL_HOST_USER,
                [form.cleaned_data.get('email'), "contact@alem-app.fr"],
            )
            email.fail_silently = False
            email.send()
            print("Email envoyé !")

            return redirect('login')
        else:
            print("Enregistrement non valide !!")
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form':form})

# Page de profil utilisateur
@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES,  instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Vos informations ont été mises à jour !')
            return redirect('profile')

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)
    
    return render(request, 'users/profile.html', {'u_form': u_form, 'p_form': p_form, 'titrePage':'page_perso'})

# Suppression d'un compte utilisateur
@login_required
def delete_account(request):
    try:
        u = User.objects.get(username = request.user.username)
        u.delete()
        messages.success(request, "Le compte de "+ request.user.username +" a bien été supprimé.")
        return render(request, 'clavier.html')

    except Exception as e:
        return render(request, 'users/profile.html', {'err':e.message})


##########
########## Gestion des séries
########## (=activité)
##########

# Suppression d'une série
@login_required
@allowed_users(allowed_roles=['teacher','admin'])
def delSerie(request):
    colis = json.loads(request.body)
    s = colis['serieCode']
    serie = Serie.objects.filter(code=s).first()
    print("demande suppression série",serie.code,serie.nom)
    serie.delete()
    return JsonResponse({'deleted':True, 'reponse':'supprimé!'})

# Création d'une série
@login_required
@allowed_users(allowed_roles=['admin','teacher'])
def createSerie(request):
    return render(request, 'users/createSerie.html')

# Initialisation d'une nouvelle série
@login_required
@allowed_users(allowed_roles=['admin','teacher'])
def saveNewSerie(request):
    colis = json.loads(request.body)
    code = colis['code']
    nom = colis['nom']
    desc = colis['description']

    rep = { 'rep':False, 'msg':'' }

    if checkIfCodeNotUsed(code):
        newSerie = Serie()
        newSerie.code = code
        newSerie.nom = nom
        newSerie.description = desc
        newSerie.auteur = request.user
        newSerie.tag = SerieTag.objects.get(nom="draft") #draft by default
        newSerie.visibility = SerieVisibility.objects.get(nom="private") #private by default
        
        newSerie.save()
        rep['rep'] = True
    else:
        rep['msg'] = "Ce code de série est déjà utilisé ! Veuillez modifier le code de la série."

    return JsonResponse(rep)

# Dupliquer une série
@login_required
@allowed_users(allowed_roles=['admin','teacher'])
def copySerie(request, pk=""):
    s = Serie.objects.filter(code=pk).first()

    code = ''.join(random.choices(string.ascii_lowercase + string.ascii_uppercase + string.digits, k=6))

    while not checkIfCodeNotUsed(code):
        code = ''.join(random.choices(string.hexdigits, k=6))

    newSerie = Serie()
    newSerie.code = code
    newSerie.nom = "Copie de "+s.nom
    newSerie.description = ""
    newSerie.auteur = request.user
    newSerie.tag = SerieTag.objects.get(nom="draft") #draft by default
    newSerie.visibility = SerieVisibility.objects.get(nom="private") #private by default

    newSerie.audioDeb = s.audioDeb
    newSerie.imageDeb = s.imageDeb
    newSerie.videoDeb = s.videoDeb
    newSerie.graphieDeb = s.graphieDeb
    
    newSerie.audioFin = s.audioFin
    newSerie.imageFin = s.imageFin
    newSerie.videoFin = s.videoFin
    newSerie.graphieFin = s.graphieFin
    newSerie.phonoFin = s.phonoFin
    
    newSerie.syntheseVocale = s.syntheseVocale
    newSerie.panneauPartiel = s.panneauPartiel
    newSerie.phonVisibles = s.phonVisibles
    
    newSerie.mots = s.mots
    
    newSerie.save()

    return HttpResponseRedirect('/player/serie-'+code+'/edit')

# Vérifier que le nom donné à une nouvelle série n'existe pas déjà
def checkIfCodeNotUsed(code):
    if Serie.objects.filter(code=code).first(): return False
    else: return True

# Charger une série existante dans l'éditeur
@login_required
@allowed_users(allowed_roles=['admin','teacher'])
def editSerie(request, pk=""):
    s = Serie.objects.filter(code=pk).first()
    print("Demande édition série", s.nom, ' (', pk, ')')

    lang = s.lang if hasattr(s, "lang") else "null"
    tag = s.tag.nom if hasattr(s, "tag") else ""
    visib = s.visibility.nom if hasattr(s, "visibility") else "null"
    serie = {
        "nom":s.nom,
        "dateCreation": s.dateCreation.strftime("%Y-%m-%d %H:%M:%S"),
        "dateModification": s.dateModification.strftime("%Y-%m-%d %H:%M:%S"),
        "code": s.code,
        "description": s.description,
        "auteur": s.auteur.username,
        "lang": lang,
        "tag": tag,
        "visibility": visib,

        "audioDeb": s.audioDeb,
        "imageDeb": s.imageDeb,
        "videoDeb": s.videoDeb,
        "graphieDeb": s.graphieDeb,
        
        "audioFin": s.audioFin,
        "imageFin": s.imageFin,
        "videoFin": s.videoFin,
        "graphieFin": s.graphieFin,
        "phonoFin": s.phonoFin,
        
        "syntheseVocale": s.syntheseVocale,
        "panneauPartiel": s.panneauPartiel,
        "phonVisibles": s.phonVisibles,
        
        "mots": s.mots
    }

    return render(request, 'users/editSerie.html', {"serie": serie, "serieJson": json.dumps(serie)})

# Enregistrement de la série
@login_required
@allowed_users(allowed_roles=['admin','teacher'])
def saveSerie(request):
    colis = json.loads(request.body)
    s = colis['serie']
    serieOrigin = colis['serieOrigin']

    data = { 'saved':False, 'msg':"" }
    
    saveIt = False

    print("demande saveSerie",s['code'], serieOrigin)
    
    # Récupère la série existante avec le nom d'origine (dans le cas où on a changé le code, il faut pouvoir updater la série d'origine et pas en créer une nouvelle)
    existingSerie = Serie.objects.filter(code=serieOrigin).first()

    if existingSerie:
        # Si on a bien une série avec ce nom (d'origine) dans la base
        if s['dateCreation'] == existingSerie.dateCreation.strftime("%Y-%m-%d %H:%M:%S"):
            # Si cette série a bien la même date de création que la série envoyée par le client... c'est que c'est bien la série à updater
            
            if s['code'] != serieOrigin:
                # Si le code a changé entre sérieOrigine et série actuelle : Check si une série existe déjà avec le nouveau nom
                withNewName = Serie.objects.filter(code=s['code']).first()
                if withNewName:
                    print("serie already exists with this new name!")
                    data['msg'] = "Une autre série du même nom existe déjà ! Choisissez un autre code de série."
                    return JsonResponse(data)
            
            print("OK edit existing serie", existingSerie.code, ' (', serieOrigin, ')')
            saveIt = True
            targetSerie = existingSerie
        else :
            # Sinon c'est que le client a mis un code déjà existant dans une autre série : donc STOP
            print("serie exists with other creation date!")
            data['msg'] = "Une autre série du même code existe déjà ! Choisissez un autre code de série."
            return JsonResponse(data)

        if saveIt:
            targetSerie.nom = s["nom"]
            targetSerie.code = s["code"]
            targetSerie.description = s["description"]
            targetSerie.auteur = User.objects.filter(username=s["auteur"]).first()
            targetSerie.audioDeb = s["audioDeb"]
            targetSerie.imageDeb = s["imageDeb"]
            targetSerie.videoDeb = s["videoDeb"]
            targetSerie.graphieDeb = s["graphieDeb"]
            targetSerie.lang = s["lang"]
            targetSerie.visibility = SerieVisibility.objects.filter(nom=s["visibility"]).first()
            
            targetSerie.audioFin = s["audioFin"]
            targetSerie.imageFin = s["imageFin"]
            targetSerie.videoFin = s["videoFin"]
            targetSerie.graphieFin = s["graphieFin"]
            targetSerie.phonoFin = s["phonoFin"]
            
            targetSerie.syntheseVocale = s["syntheseVocale"]
            targetSerie.panneauPartiel = s["panneauPartiel"]
            targetSerie.phonVisibles = s["phonVisibles"]
            
            targetSerie.mots = json.dumps(s["mots"])
            
            targetSerie.save()
            print("saveSerie",s['code'], serieOrigin)

            data['saved'] = True
            return JsonResponse(data)
    else:
        data['msg'] = "Cette série n'existe pas !"
        
    return JsonResponse(data)

###
### GESTION MÉDIATHÈQUE
###

# Lister les média de la bibliothèque
@login_required
def listMediaFiles(request):
    colis = json.loads(request.body)
    targetDir = colis['targetDir']
    if targetDir == "audio":
        tgtdr = "../media/audio-uploads"
    elif targetDir == "image":
        tgtdr = "../media/image-uploads"
    elif targetDir == "video":
        tgtdr = "../media/video-uploads"
    else:
        tgtdr = ""

    if len(tgtdr)>0:
        listfi = []
        for root, dirs, files in os.walk(tgtdr, topdown=False):
            for fil in files:
                listfi.append([ root+'/'+fil ])
            
        data = {
            "listfiles": listfi
        }
        # exportAllSeries()
        return JsonResponse(data)

# Upload d'un nouveau fichier audio
def uploadAudio(request):
    if request.method == 'POST':
        print(request.FILES)

        namelist = request.FILES

        data = {"chemins":[]}

        for i in namelist:
            name = str(namelist[i])
            size = namelist[i].size

            print("Demande audio entrant :", name, "|taille:", size)
            # Limite à 2Mo (2000000 octets)
            if size < 2000000:
                print("OK. Upload de", name, "|UploadedBy:",request.user.username,request.user.id)
                audio = Audio()
                audio.nom = name
                audio.auteur = request.user.id
                audio.file = namelist[i]
                audio.save()
                print(audio.file.url)
                data['chemins'].append(audio.file.url)
            else:
                print("Le fichier est trop gros ! (limite:2Mb)")
                data['chemins'].append("ECHEC (fichier trop gros) "+audio.file.url)
        
        print(data)
        return JsonResponse(data)

# Upload d'un nouveau fichier vidéo
def uploadVideo(request):
     if request.method == 'POST':
        print(request.FILES)

        namelist = request.FILES

        data = {"chemins":[]}

        for i in namelist:
            name = str(namelist[i])
            size = namelist[i].size

            print("Demande video entrant :", name, "|taille:", size)
            # Limite à 2Mo (2000000 octets)
            if size < 2000000:
                print("OK. Upload de", name, "|UploadedBy:",request.user.username,request.user.id)
                video = Video()
                video.nom = name
                video.auteur = request.user.id
                video.file = namelist[i]
                video.save()
                print(video.file.url)
                data['chemins'].append(video.file.url)
            else:
                print("Le fichier est trop gros ! (limite:2Mb)")
                data['chemins'].append("ECHEC (fichier trop gros) "+video.file.url)
        
        print(data)
        return JsonResponse(data)

# Upload d'un nouveau fichier image
def uploadImage(request):
    if request.method == 'POST':
        print(request.FILES)

        namelist = request.FILES

        data = {"chemins":[]}

        for i in namelist:
            name = str(namelist[i])
            size = namelist[i].size

            print("Demande image entrant :", name, "|taille:", size)
            # Limite à 2Mo (2000000 octets)
            if size < 2000000:
                print("OK. Upload de", name, "|UploadedBy:",request.user.username,request.user.id)
                image = Image()
                image.nom = name
                image.auteur = request.user.id
                image.file = namelist[i]
                image.save()
                print(image.file.url)
                data['chemins'].append(image.file.url)
            else:
                print("Le fichier est trop gros ! (limite:2Mb)")
                data['chemins'].append("ECHEC (fichier trop gros) "+image.file.url)
        
        print(data)
        return JsonResponse(data)


# Supprsession d'un fichier de la médiathèque
def removeFile(request):
    colis = json.loads(request.body)
    fileName = colis['filePath']
    mediaType = colis['mediaType']

    print("Demande suppression de",fileName, mediaType)

    os.remove(fileName)

    return JsonResponse({})


# Enregistrer un audio directement avec le PhonoGraphe
@allowed_users(allowed_roles=['admin','teacher'])
def addRec(request):
    colis = json.loads(request.body)
    filename = colis["filename"]
    
    if checkIfSameNameExists(filename+".mp3"):
        print("Le nom de fichier", filename, "existe déjà!")
        return JsonResponse({'status': 'error', 'message': "Ce nom de fichier existe déjà !"})

    try:
        # Decode base64 audio data
        audio_data = base64.b64decode(colis['audio'])

        # Convert binary audio data to AudioSegment
        audio_segment = AudioSegment.from_file(io.BytesIO(audio_data), format="mp3")

        # Save the audio as MP3
        mp3_file_path = f"../media/audio-uploads/{ filename }.mp3"
        audio_segment.export(mp3_file_path, format="mp3")

        print("SUCCESS")
        return JsonResponse({'status': 'success', "message":"Enregistré !"})
    
    except Exception as e:
        print("ERROR")
        return JsonResponse({'status': 'error', 'message': str(e)})

    return JsonResponse({'status': 'error', 'message': 'Invalid request method'})

# Check if a file with this filename exists in audio_uploads folder
def checkIfSameNameExists(filename):
    for file in os.listdir('../media/audio-uploads/'):
        if file == filename:
            return True
    return False    


####
#### GESTION DES TRACES UTILISATEUR
####
####

# Récupération IP client
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

# Enregistrer une nouvelle trace utilisateur
def saveTrace(request):
    colis = json.loads(request.body)
    newTrace = Trace()
    newTrace.user = request.user
    newTrace.userAgent = parse(request.META['HTTP_USER_AGENT'])
    newTrace.userIp = get_client_ip(request)
    newTrace.appli = colis['appli']
    action = colis['action']
    newTrace.content = json.dumps(action, ensure_ascii=False)
    newTrace.save()

    print(newTrace)

    data = {
        'saved': True
    }
    return JsonResponse(data)

# Enregistrer une trace de synthèse vocale
def saveTraceSynthVoc(colis):
    newTrace = SynthVocTrace()
    newTrace.user = colis['user']
    newTrace.userAgent = colis['userAgent']
    newTrace.userIp = colis['userIp']
    newTrace.appli = colis['appli']
    newTrace.lenssml = colis['lenssml']
    newTrace.save()
    print("Trace synthèse vocale enregistrée", colis['lenssml'])

# Lister les traces enregistrées
def getTracesStats(request):
    listTraces = Trace.objects.all()
    cptTraces = 0
    dicoTraces = {} # user2nbTrace
    for trace in listTraces:
        cptTraces += 1
        if trace.user.username not in dicoTraces.keys():
            dicoTraces[trace.user.username] = 0
        dicoTraces[trace.user.username] += 1
    
    listVocSynth = SynthVocTrace.objects.all()
    cptVS = 0
    lenVS = 0
    dicoVS = {} # user2(nbTrace, lenssml)
    listUsersAnonymous = []

    for trace in listVocSynth:
        cptVS += 1
        lenVS += trace.lenssml

        if trace.user == 'AnonymousUser':
            if trace.userIp not in listUsersAnonymous:
                listUsersAnonymous.append(trace.userIp)

        if trace.user not in dicoVS.keys():
            dicoVS[trace.user] = {"Nombre de requêtes envoyées":0, "Nombre de caractères":0}
        dicoVS[trace.user]["Nombre de requêtes envoyées"] += 1
        dicoVS[trace.user]["Nombre de caractères"] += trace.lenssml
    
    data = {
        "Nombre de traces enregistrées" : cptTraces,
        "Détails des traces" : dicoTraces,
        "Nombre de requêtes de synthèse vocale" : cptVS,
        "Nombre total de caractères requêtés pour la synthèse vocale" : lenVS,
        "Détails des requêtes de synthèse vocale" : dicoVS,
        "Nombre d'adresses IP différentes pour AnonymousUser" : len(listUsersAnonymous)
    }

    return JsonResponse(data)


####
#### GESTION UTILISATEURS
#### ET EXPORT DES TRACES
####

# Page qui liste les utilisateurs
@allowed_users(allowed_roles=['admin','teacher'])
def usersPage(request):
    users = User.objects.all()
    for user in users:
        lastTrace = Trace.objects.filter(user=user).last()
        if lastTrace:
            user.profile.lastTrace = lastTrace.date
        print(user, user.profile.lastTrace)
    return render(request, 'users/usersPage.html', {'users':users})

# Afficher les info d'un utilisateur
@allowed_users(allowed_roles=['admin','teacher'])
def userInfo(request, username):
    user = User.objects.filter(username=username).first()
    print("Récup info de", user.username)

    # Afficher l'historique des traces de l'utilisateur
    # Formattage du temps : https://docs.djangoproject.com/en/3.1/ref/templates/builtins/#std:templatefilter-date
    traces = Trace.objects.filter(user=user.id)
    nbTraces = len(traces)
    lastTrace = traces[nbTraces-1].date if nbTraces > 0 else 0
    daylist = np.arange('2021-02-15', np.datetime64('today', 'D')+np.timedelta64(1, 'D'), dtype='datetime64[D]')
    day2traces = {}

    # Initialisation liste vide
    for day in daylist:
        day2traces[str(day)] = 0
    
    # Remplissage avec les dates des traces
    for trace in traces:
        day2traces[trace.date.strftime("%Y-%m-%d")] += 1

    dataUser = {
        'user': user,
        'traces': traces,
        'day2traces': json.dumps(day2traces),
        'nbTraces': nbTraces,
        'lastTrace': lastTrace
    }
    return render(request, 'users/userInfo.html', {'data': dataUser})

# Télécharger les traces utilisateur en CSV
@allowed_users(allowed_roles=['admin','teacher'])
def downloadTraces(request):
    colis = json.loads(request.body)
    username = colis['username']
    date1 = datetime.datetime.strptime(colis['date1'], '%d/%m/%Y')
    date2 = datetime.datetime.strptime(colis['date2'], '%d/%m/%Y')

    user = User.objects.filter(username=username).first()
    print("Téléchargement des traces de", user.username)
    print("Période souhaitée :", date1, date2)

    traces = Trace.objects.filter(user=user.id).filter(date__range=(date1,date2+datetime.timedelta(days=1)))

    response = HttpResponse(content_type='text/csv')

    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))
    
    writer.writerow(['id', 'date', 'application', 'contenu', 'ip', 'agent'])
    for trace in traces:
        writer.writerow([
            trace.id,
            trace.date,
            trace.appli,
            trace.content,
            trace.userIp,
            trace.userAgent
        ])

    return response

# Télécharger les traces de synthèse vocale
@allowed_users(allowed_roles=['admin','teacher'])
def getSynthVocTraces(request):
    print("Téléchargement csv synthVocTraces...")
    synthVocTraces = SynthVocTrace.objects.all()
    response = HttpResponse(content_type='text/csv')

    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))
    
    writer.writerow(['id', 'user', 'date', 'appli', 'lenssml'])
    for trace in synthVocTraces:
        writer.writerow([
            trace.id,
            trace.user,
            trace.date,
            trace.appli,
            trace.lenssml
        ])

    return response


###
### MODULE DE STATISTIQUES D'UTILISATION
### DES APPLIS ALEM
###
# Module d'enregistrement d'utilisation des applications ALeM
# Permet d'afficher les stats d'utilisation des applis : https://phonographe.alem-app.fr/stats/ (attention c'est lourd...)

# Enregistrement d'une trace d'utilisation
def addStat(request):
    colis = json.loads(request.body)
    app = colis["app"]
    module = colis["module"]
    lang = colis["lang"]

    if get_client_ip(request)!="127.0.0.1":
        newStat = appstats()

        newStat.app =  app
        newStat.module =  module
        newStat.ip =  get_client_ip(request)
        newStat.agent = parse(request.META['HTTP_USER_AGENT'])
        newStat.country = getCountry(get_client_ip(request))
        newStat.lang =  lang
        
        newStat.save()

    return JsonResponse({})

# Identification du pays d'origine de la requête
def getCountry(ip):
    endpoint = f'https://ipinfo.io/{ip}/json'
    response = requests.get(endpoint, verify = True)

    if response.status_code != 200:
        return 'Status:', response.status_code, 'Problem with the request. Exiting.'
        exit()

    data = response.json()
    return data['country']

# Afficher la page de statistiques (vide)
@allowed_users(allowed_roles=['admin','teacher'])
def appStatVis(request):
    return render(request, 'users/appStatVis.html')

# Charger les statistiques dans la page de stats (c'est long)
@allowed_users(allowed_roles=['admin','teacher'])
def getAppStats(request):
    try:
        statlist = []
        for s in appstats.objects.all():
            statlist.append({
                "date": s.date,
                "app": s.app,
                "module": s.module,
                "ip": s.ip,
                "agent": s.agent,
                "country": s.country,
                "lang": s.lang
            })

        daylist = np.arange('2022-08-07', np.datetime64('today', 'D')+np.timedelta64(1, 'D'), dtype='datetime64[D]')
        
        day2traces = {}
        # Initialisation liste vide
        for day in daylist:
            day2traces[str(day)] = 0

        data = { "appdata": statlist, "day2traces":day2traces }
        return JsonResponse(data)
    finally:
        statlist = []
        day2traces = {}

# Charger les stats spécifiques à la synthèse vocale phonologique
@allowed_users(allowed_roles=['admin','teacher'])
def getSynthVocRecords(request):
    print("Requête json synthVocRecords...")
    
    data = {
        "nbRecords": 0,
        "nbFreq": 0,
        "ipa2freq": {},
        "lang2freq": {}
    }
    try:
        # Remplissage avec les dates des traces
        for record in SynthVocRecord.objects.all():
            data['nbRecords'] += 1
            data['nbFreq'] += record.cptEcoute+1 # compteur démarre à 0 lors de synthèse Polly
            
            if record.lang == "enbr": lang = "en"
            else: lang = record.lang

            if lang not in data['ipa2freq'].keys():
                data['ipa2freq'][lang] = {}
            if record.ipa not in data["ipa2freq"][lang].keys():
                data["ipa2freq"][lang][record.ipa] = 0
            data["ipa2freq"][lang][record.ipa] += record.cptEcoute+1

            if lang not in data["lang2freq"].keys():
                data["lang2freq"][lang] = 0
            data["lang2freq"][lang] += record.cptEcoute+1

        return JsonResponse(data)
    finally:
        data = {}

# Afficher les stats de synthèse vocale en JSON
@allowed_users(allowed_roles=['admin','teacher'])
def getSynthVocTracesJson(request):
    print("Requête json synthVocTraces...")
    
    data = {
        "nbTraces":0,
        "lenssmlThisMonth": 0,
        "user2nbTraces": {},
        "user2Lenssml": {},
        "app2nbTraces": {},
        "app2Lenssml": {},
        "day2nbTracesLenssml": {},
    }

    try:
        thisMonth = datetime.datetime.now().strftime("%Y%m")
        
        # initialisation de day2nbTraces
        daylist = np.arange('2021-02-15', np.datetime64('today', 'D')+np.timedelta64(1, 'D'), dtype='datetime64[D]')
        day2traces = {}
        # Initialisation liste vide
        for day in daylist:
            day2traces[str(day)] = [0, 0] # nbTraces, lenssml
        
        # Remplissage avec les dates des traces
        for trace in SynthVocTrace.objects.all():
            day2traces[trace.date.strftime("%Y-%m-%d")][0] += 1
            day2traces[trace.date.strftime("%Y-%m-%d")][1] += trace.lenssml
            data['nbTraces'] += 1

            if trace.date.strftime("%Y%m")==thisMonth:
                data['lenssmlThisMonth'] += trace.lenssml
            
            if trace.user not in data["user2nbTraces"].keys():
                data["user2nbTraces"][trace.user] = 0
                data["user2Lenssml"][trace.user] = 0
            data["user2nbTraces"][trace.user] += 1
            data["user2Lenssml"][trace.user] += trace.lenssml

            if trace.appli not in data["app2nbTraces"].keys():
                data["app2nbTraces"][trace.appli] = 0
                data["app2Lenssml"][trace.appli] = 0
            data["app2nbTraces"][trace.appli] += 1
            data["app2Lenssml"][trace.appli] += trace.lenssml

        data["day2nbTracesLenssml"] = day2traces

        return JsonResponse(data)
    finally:
        data = []
        day2traces = {}
