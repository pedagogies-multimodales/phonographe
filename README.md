# Dépôt du code de l'application PhonoGraphe

![LogoPhonoGraphe](static/im/logo_phonographe3.png){width=500px}

Le PhonoGraphe fait partie des applications développées par le groupe de recherche ALeM (Apprentissage des Langues et Multimodalité) au laboratoire LIDILEM (Université Grenoble Alpes) et ICAR (Université Lyon 2 Lumière).

L'application PhonoGraphe est présentée ici : https://alem.hypotheses.org/outils-alem-app/phonographe

L'application elle-même est accessible ici : https://phonographe.alem-app.fr/

L’application est composée du clavier phonologique pour écrire en couleur (page principale) et d’une interface d’activités appelée PhonoPlayer. Le PhonoPlayer est accessible à partir du bouton « jouer » dans la barre de navigation. L’utilisateur accède à une page listant les séries disponibles. En cliquant sur l’une d’elles, il peut démarrer l’activité, et selon ses droits, la modifier ou en créer d’autres.

# Caractéristiques techniques

L’application PhonoGraphe est codée en Python v3.10.12 avec le framework Django v3.1.12.

# Détails de l'arborescence

Les fichiers de paramétrage global (urls, settings etc.) sont dans `clavierPhono`. Il y a deux apps différentes, la première est `clavier` et gère l'affichage des pages et des fonctions de base du PhonoGraphe (clavier en couleurs). L'autre app est `users` et gère les fonctionnalités plus récentes : base d'authentification utilisateurs, gestion des médias, des séries, des traces et des statistiques d'utilisation.