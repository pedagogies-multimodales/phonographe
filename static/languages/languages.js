var langJson = {
    "sp_soustitre": {
        "fr": "<span class='phon_k'>C</span><span class='phon_l'>l</span><span class='phon_a'>a</span><span class='phon_v'>v</span><span class='phon_j'>i</span><span class='phon_e'>er</span><span> </span><span class='phon_f'>ph</span><span class='phon_o'>o</span><span class='phon_n'>n</span><span class='phon_o'>o</span><span class='phon_g'>g</span><span class='phon_r_maj'>r</span><span class='phon_a'>a</span><span class='phon_f'>ph</span><span class='phon_e'>é</span><span class='phon_m'>m</span><span class='phon_i'>i</span><span class='phon_k'>que</span><span> </span><span class='phon_p'>p</span><span class='phon_u'>ou</span><span class='phon_r_maj'>r</span><span> </span><span class='phon_e'>é</span><span class='phon_k'>c</span><span class='phon_r_maj'>r</span><span class='phon_i'>i</span><span class='phon_r_maj'>re</span><span> </span><span class='phon_a_maj_nas'>en</span><span> </span><span class='phon_k'>c</span><span class='phon_u'>ou</span><span class='phon_l'>l</span><span class='phon_9'>eu</span><span class='phon_r_maj'>rs</span>",
        "en": "<span class='phon_arobase schwa'>a</span><span> </span><span class='phon_f'>ph</span><span class='phon_arobase schwa'>o</span><span class='phon_n'>n</span><span class='phon_q_maj stress2'>o</span><span class='phon_g'>g</span><span class='phon_r_slash'>r</span><span class='phon_cbrack'>a</span><span class='phon_f'>ph</span><span class='phon_i_long stress1'>e</span><span class='phon_m'>m</span><span class='phon_i_maj schwa'>i</span><span class='phon_k'>c</span><span> </span><span class='phon_k'>k</span><span class='phon_i_long stress1'>ey</span><span class='phon_b'>b</span><span class='phon_o_maj_long'>oar</span><span class='phon_d'>d</span><span> </span><span class='phon_t'>t</span><span class='phon_arobase schwa'>o</span><span> </span><span class=''><span class='phon_r_slash'>wr</span><span class='phon_ai_maj stress1'>i</span><span class='phon_t'>te</span></span><span> </span><span class='phon_i_maj'>i</span><span class='phon_n'>n</span><span> </span><span class=''><span class='phon_k'>c</span><span class='phon_v_maj stress1'>o</span><span class='phon_l'>l</span></span><span class='phon_schwa schwa'>or</span><span class='phon_z'>s</span>",
        "zh": "<span class='phon_k'>C</span><span class='phon_l'>l</span><span class='phon_a'>a</span><span class='phon_v'>v</span><span class='phon_j'>i</span><span class='phon_e'>er</span><span> </span><span class='phon_f'>ph</span><span class='phon_o'>o</span><span class='phon_n'>n</span><span class='phon_o'>o</span><span class='phon_g'>g</span><span class='phon_r_maj'>r</span><span class='phon_a'>a</span><span class='phon_f'>ph</span><span class='phon_e'>é</span><span class='phon_m'>m</span><span class='phon_i'>i</span><span class='phon_k'>que</span><span> </span><span class='phon_p'>p</span><span class='phon_u'>ou</span><span class='phon_r_maj'>r</span><span> </span><span class='phon_e'>é</span><span class='phon_k'>c</span><span class='phon_r_maj'>r</span><span class='phon_i'>i</span><span class='phon_r_maj'>re</span><span> </span><span class='phon_a_maj_nas'>en</span><span> </span><span class='phon_k'>c</span><span class='phon_u'>ou</span><span class='phon_l'>l</span><span class='phon_9'>eu</span><span class='phon_r_maj'>rs</span>",
        "dz": "<span class='phon_k noUnderLine arabe artitre'>ﻛ</span><span class='phon_l noUnderLine arabe artitre'>ﻠ</span><span class='phon_a_long noUnderLine arabe artitre'>ﺎ</span><span class='phon_a noTextClip noUnderLine arabe artitre'>َ</span><span class='phon_v noUnderLine arabe artitre'>ﻓ</span><span class='phon_i noTextClip noUnderLine arabe artitre'>ِ</span><span class='phon_i_long noUnderLine arabe artitre'>ﻲ</span><span class='phon_e noTextClip noUnderLine arabe artitre'>ِ&zwj;</span><span class='phon_j noTextClip noUnderLine arabe artitre'>ّ</span><span class='punct arabe artitre'> </span><span class='phon_f noUnderLine arabe artitre'>ﻓ</span><span class='phon_o noUnderLine arabe artitre'>ﻮ</span><span class='phon_n noUnderLine arabe artitre'>ﻧ</span><span class='phon_o noUnderLine arabe artitre'>ﻮ</span><span class='phon_g noUnderLine arabe artitre'>ﻏ</span><span class='phon_r noUnderLine arabe artitre'>ﺮ</span><span class='phon_a noTextClip noUnderLine arabe artitre'>َ</span><span class='phon_a_long noUnderLine arabe artitre'>ا</span><span class='phon_f noUnderLine arabe artitre'>ﻓ</span><span class='phon_i noTextClip noUnderLine arabe artitre'>ِ</span><span class='phon_i_long noUnderLine arabe artitre'>ﻲ</span><span class='punct arabe artitre'> </span><span class='phon_l noUnderLine arabe artitre'>ﻟ</span><span class='phon_k noUnderLine arabe artitre'>ﻜ</span><span class='phon_a noTextClip noUnderLine arabe artitre'>َ</span><span class='phon_t noUnderLine arabe artitre'>ﺘ</span><span class='phon_i noTextClip noUnderLine arabe artitre'>ِ</span><span class='phon_i_long noUnderLine arabe artitre'>ﻴ</span><span class='phon_b noUnderLine arabe artitre'>ﺒ</span><span class='phon_a noTextClip noUnderLine arabe artitre'>َ</span><span class='phon_a noUnderLine arabe artitre'>‍‍‍‌ﺔ</span><span class='punct arabe artitre'> </span><span class='phon_b noUnderLine arabe artitre'>ﺑ</span><span class='phon_l noUnderLine arabe artitre'>ﺎﻟ</span><span class='phon_e_maj noUnderLine arabe artitre'>ﺎ</span><span class='phon_e_maj noTextClip noUnderLine arabe artitre'>ٔ</span><span class='phon_l noUnderLine arabe artitre'>ﻟ</span><span class='phon_w noUnderLine arabe artitre'>ﻮ</span><span class='phon_e_maj noTextClip noUnderLine arabe artitre'>َ</span><span class='phon_e_maj_long noUnderLine arabe artitre'>‍ﺍ</span><span class='phon_n noUnderLine arabe artitre'>ﻥ</span>",
        "shy": "<span class='phon_k'>C</span><span class='phon_l'>l</span><span class='phon_a'>a</span><span class='phon_v'>v</span><span class='phon_j'>i</span><span class='phon_e'>er</span><span> </span><span class='phon_f'>ph</span><span class='phon_o'>o</span><span class='phon_n'>n</span><span class='phon_o'>o</span><span class='phon_g'>g</span><span class='phon_r_maj'>r</span><span class='phon_a'>a</span><span class='phon_f'>ph</span><span class='phon_e'>é</span><span class='phon_m'>m</span><span class='phon_i'>i</span><span class='phon_k'>que</span><span> </span><span class='phon_p'>p</span><span class='phon_u'>ou</span><span class='phon_r_maj'>r</span><span> </span><span class='phon_e'>é</span><span class='phon_k'>c</span><span class='phon_r_maj'>r</span><span class='phon_i'>i</span><span class='phon_r_maj'>re</span><span> </span><span class='phon_a_maj_nas'>en</span><span> </span><span class='phon_k'>c</span><span class='phon_u'>ou</span><span class='phon_l'>l</span><span class='phon_9'>eu</span><span class='phon_r_maj'>rs</span>",
        "de": "<span class='phon_f unstressed'>Ph</span><span class='phon_o_maj unstressed'>o</span><span class='phon_n unstressed'>n</span><span class='phon_o_maj unstressed'>o</span><span class='phon_g unstressed'>g</span><span class='phon_r_maj unstressed'>r</span><span class='phon_a_maj_long stress1'>a</span><span class='phon_f unstressed'>ph</span><span class='phon_i_maj unstressed'>i</span><span class='phon_s_maj unstressed'>sch</span><span class='phon_schwa schwa'>e</span><span class='punct unstressed'> </span><span class='phon_t unstressed'>T</span><span class='phon_a unstressed'>a</span><span class='phon_s unstressed'>s</span><span class='phon_t unstressed'>t</span><span class='phon_a unstressed'>a</span><span class='phon_t unstressed'>t</span><span class='phon_u_long stress1'>u</span><span class='phon_r_maj unstressed'>r</span><span class='punct unstressed'> </span><span class='phon_u_maj unstressed'>u</span><span class='phon_m unstressed'>m</span><span class='punct unstressed'> </span><span class='phon_i_maj unstressed'>i</span><span class='phon_n unstressed'>n</span><span class='punct unstressed'> </span><span class='phon_f unstressed'>F</span><span class='phon_a_maj_long stress1'>a</span><span class='phon_r_maj unstressed'>r</span><span class='phon_b unstressed'>b</span><span class='phon_schwa schwa'>e</span><span class='phon_n unstressed'>n</span><span class='punct unstressed'> </span><span class='phon_ts unstressed'>z</span><span class='phon_u_maj unstressed'>u</span><span class='punct unstressed'> </span><span class='phon_s_maj unstressed'>sch</span><span class='phon_r_maj unstressed'>r</span><span class='phon_ai_maj stress1'>ei</span><span class='phon_b unstressed'>b</span><span class='phon_schwa schwa'>e</span><span class='phon_n unstressed'>n</span>"
    },
    "sp_btnspace": {
        "fr": "Espace",
        "en": "Space",
        "zh": "Espace",
        "dz": "Espace",
        "shy": "Espace",
        "de": "Espace"
    },
    "sp_btnerase": {
        "fr": "Effacer",
        "en": "Erase",
        "zh": "Effacer",
        "dz": "Effacer",
        "shy": "Effacer",
        "de": "Effacer"
    },
    "sp_btneraseall": {
        "fr": "Effacer tout",
        "en": "Erase all",
        "zh": "Effacer tout",
        "dz": "Effacer tout",
        "shy": "Effacer tout",
        "de": "Effacer tout"
    },
    "sp_confirmErase": {
        "fr": "Êtes-vous sûr de vouloir réinitialiser la page ?",
        "en": "Are you sure you want to erase the whole content of this page?",
        "zh": "Êtes-vous sûr de vouloir réinitialiser la page ?",
        "dz": "Êtes-vous sûr de vouloir réinitialiser la page ?",
        "shy": "Êtes-vous sûr de vouloir réinitialiser la page ?",
        "de": "Êtes-vous sûr de vouloir réinitialiser la page ?"
    },
    "sp_poptitle": {
        "fr": "Copier/coller le résultat",
        "en": "Copy/paste the result",
        "zh": "Copier/coller le résultat",
        "dz": "Copier/coller le résultat",
        "shy": "Copier/coller le résultat",
        "de": "Copier/coller le résultat"
    },
    "sp_popmain": {
        "fr": "Copier/coller le texte ci-dessous dans LibreOffice, Word ou dans un e-mail, par exemple.",
        "en": "Copy/paste the text below into OpenOffice, Word or e-mail for example.",
        "zh": "Copier/coller le texte ci-dessous dans LibreOffice, Word ou dans un e-mail, par exemple.",
        "dz": "Copier/coller le texte ci-dessous dans LibreOffice, Word ou dans un e-mail, par exemple.",
        "shy": "Copier/coller le texte ci-dessous dans LibreOffice, Word ou dans un e-mail, par exemple.",
        "de": "Copier/coller le texte ci-dessous dans LibreOffice, Word ou dans un e-mail, par exemple."
    },
    "sp_popcopy1": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copier",
        "dz": "Copier",
        "shy": "Copier",
        "de": "Copier"
    },
    "sp_popcopy2": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copier",
        "dz": "Copier",
        "shy": "Copier",
        "de": "Copier"
    },
    "sp_popcopy3": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copier",
        "dz": "Copier",
        "shy": "Copier",
        "de": "Copier"
    },
    "sp_popcopy4": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copier",
        "dz": "Copier",
        "shy": "Copier",
        "de": "Copier"
    },
    "sp_popsavetitle": {
        "fr": "Page enregistrée !",
        "en": "Page saved!",
        "zh": "Page enregistrée !",
        "dz": "Page enregistrée !",
        "shy": "Page enregistrée !",
        "de": "Page enregistrée !"
    },
    "sp_popsavemain": {
        "fr": "Pour accéder au contenu ultérieurement, ou partager la page avec quelqu'un, utilisez le lien suivant :",
        "en": "To access this page later, or share its content with someone else, use the link below:",
        "zh": "Pour accéder au contenu ultérieurement, ou partager la page avec quelqu'un, utilisez le lien suivant :",
        "dz": "Pour accéder au contenu ultérieurement, ou partager la page avec quelqu'un, utilisez le lien suivant :",
        "shy": "Pour accéder au contenu ultérieurement, ou partager la page avec quelqu'un, utilisez le lien suivant :",
        "de": "Pour accéder au contenu ultérieurement, ou partager la page avec quelqu'un, utilisez le lien suivant :"
    },
    "sp_popsavecopy": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copier",
        "dz": "Copier",
        "shy": "Copier",
        "de": "Copier"
    },
    "sp_popsavecopylink": {
        "fr": "Copier le lien",
        "en": "Copy the link",
        "zh": "Copier le lien",
        "dz": "Copier le lien",
        "shy": "Copier le lien",
        "de": "Copier le lien"
    },
    "sp_popsaveopen": {
        "fr": "Ouvrir",
        "en": "Open",
        "zh": "Ouvrir",
        "dz": "Ouvrir",
        "shy": "Ouvrir",
        "de": "Ouvrir"
    },
    "sp_header": {
        "fr": "Cette application est un prototype en cours de conception. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Accéder au forum</b></a> pour échanger et faire des suggestions.",
        "en": "This application still is a prototype under development. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Access to the forum</b></a> to share and make suggestions.",
        "zh": "Cette application est un prototype en cours de conception. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Accéder au forum</b></a> pour échanger et faire des suggestions.",
        "dz": "Cette application est un prototype en cours de conception. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Accéder au forum</b></a> pour échanger et faire des suggestions.",
        "shy": "Cette application est un prototype en cours de conception. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Accéder au forum</b></a> pour échanger et faire des suggestions.",
        "de": "Cette application est un prototype en cours de conception. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Accéder au forum</b></a> pour échanger et faire des suggestions."
    },
    "sp_footfirefox": {
        "fr": "Application optimisée pour Mozilla Firefox.",
        "en": "Application optimized for Mozilla Firefox.",
        "zh": "Application optimisée pour Mozilla Firefox.",
        "dz": "Application optimisée pour Mozilla Firefox.",
        "shy": "Application optimisée pour Mozilla Firefox.",
        "de": "Application optimisée pour Mozilla Firefox."
    },
    "sp_footcode": {
        "fr": "Code source",
        "en": "Source code",
        "zh": "Code source",
        "dz": "Code source",
        "shy": "Code source",
        "de": "Code source"
    },
    "sp_footjournal": {
        "fr": "Journal des modifications",
        "en": "Modifications log",
        "zh": "Journal des modifications",
        "dz": "Journal des modifications",
        "shy": "Journal des modifications",
        "de": "Journal des modifications"
    },
    "sp_footlicence": {
        "fr": "Code open source sous licence CC BY-NC-SA 4.0",
        "en": "Open source code under CC BY-NC-SA 4.0 licence",
        "zh": "Code open source sous licence CC BY-NC-SA 4.0",
        "dz": "Code open source sous licence CC BY-NC-SA 4.0",
        "shy": "Code open source sous licence CC BY-NC-SA 4.0",
        "de": "Code open source sous licence CC BY-NC-SA 4.0"
    },
    "sp_footcopyr": {
        "fr": "Panneaux phonologiques & disposition des graphies",
        "en": "Phonemic charts & spelling charts",
        "zh": "Panneaux phonologiques & disposition des graphies",
        "dz": "Panneaux phonologiques & disposition des graphies",
        "shy": "Panneaux phonologiques & disposition des graphies",
        "de": "Panneaux phonologiques & disposition des graphies"
    },
    "sp_info": {
        "fr": "Le PhonoGraphe est un outil inspiré de la pédagogie Gattegno. Il évolue en fonction de vos besoins, n'hésitez pas à nous en faire part sur <a href='https://groups.google.com/forum/#!forum/alem-app'>le forum ALeM</a>, toute suggestion de votre part est bienvenue ! Plus d'infos sur <a href='https://alem.hypotheses.org/outils-alem-app/phonographe'>notre site</a>. Pour demander un compte administrateur et créer vos propres activités, contactez <a href='mailto:sylvain.coulange@univ-grenoble-alpes.fr'>Sylvain Coulange</a>.",
        "en": "The PhonoGraphe is a tool inspired from the Gattegno pedagogy. It evolves depending on your needs, please let us know your suggestions on <a href='https://groups.google.com/forum/#!forum/alem-app'>our Forum</a>, any suggestion from you will be welcome! Further information on <a href='https://alem.hypotheses.org/outils-alem-app/phonographe'>our website</a>. If you need a admin account to create your own activities, contact <a href='mailto:sylvain.coulange@univ-grenoble-alpes.fr'>Sylvain Coulange</a>.",
        "zh": "Le PhonoGraphe est un outil inspiré de la pédagogie Gattegno. Il évolue en fonction de vos besoins, n'hésitez pas à nous en faire part sur <a href='https://groups.google.com/forum/#!forum/alem-app'>le forum ALeM</a>, toute suggestion de votre part est bienvenue ! Plus d'infos sur <a href='https://alem.hypotheses.org/outils-alem-app/phonographe'>notre site</a>. Pour demander un compte administrateur et créer vos propres activités, contactez <a href='mailto:sylvain.coulange@univ-grenoble-alpes.fr'>Sylvain Coulange</a>.",
        "dz": "Le PhonoGraphe est un outil inspiré de la pédagogie Gattegno. Il évolue en fonction de vos besoins, n'hésitez pas à nous en faire part sur <a href='https://groups.google.com/forum/#!forum/alem-app'>le forum ALeM</a>, toute suggestion de votre part est bienvenue ! Plus d'infos sur <a href='https://alem.hypotheses.org/outils-alem-app/phonographe'>notre site</a>. Pour demander un compte administrateur et créer vos propres activités, contactez <a href='mailto:sylvain.coulange@univ-grenoble-alpes.fr'>Sylvain Coulange</a>.",
        "shy": "Le PhonoGraphe est un outil inspiré de la pédagogie Gattegno. Il évolue en fonction de vos besoins, n'hésitez pas à nous en faire part sur <a href='https://groups.google.com/forum/#!forum/alem-app'>le forum ALeM</a>, toute suggestion de votre part est bienvenue ! Plus d'infos sur <a href='https://alem.hypotheses.org/outils-alem-app/phonographe'>notre site</a>. Pour demander un compte administrateur et créer vos propres activités, contactez <a href='mailto:sylvain.coulange@univ-grenoble-alpes.fr'>Sylvain Coulange</a>.",
        "de": "Le PhonoGraphe est un outil inspiré de la pédagogie Gattegno. Il évolue en fonction de vos besoins, n'hésitez pas à nous en faire part sur <a href='https://groups.google.com/forum/#!forum/alem-app'>le forum ALeM</a>, toute suggestion de votre part est bienvenue ! Plus d'infos sur <a href='https://alem.hypotheses.org/outils-alem-app/phonographe'>notre site</a>. Pour demander un compte administrateur et créer vos propres activités, contactez <a href='mailto:sylvain.coulange@univ-grenoble-alpes.fr'>Sylvain Coulange</a>."
    },
    "sp_tuto": {
        "fr": "Accéder aux tutoriels Phonographe",
        "en": "Access Phonographe's tutorials",
        "zh": "访问教程",
        "dz": "Accéder aux tutoriels Phonographe",
        "shy": "Accéder aux tutoriels Phonographe",
        "de": "Accéder aux tutoriels Phonographe"
    },
    "sp_playerHeader": {
        "fr": "Prototype d'activités de pointage phonologique",
        "en": "",
        "zh": "Prototype d'activités de pointage phonologique",
        "dz": "Prototype d'activités de pointage phonologique",
        "shy": "Prototype d'activités de pointage phonologique",
        "de": "Prototype d'activités de pointage phonologique"
    },
    "sp_playerHomeListSeriesTitle": {
        "fr": "Séries disponibles",
        "en": "",
        "zh": "Séries disponibles",
        "dz": "Séries disponibles",
        "shy": "Séries disponibles",
        "de": "Séries disponibles"
    },
    "sp_profileHeader": {
        "fr": "Page personnelle",
        "en": "Profile page",
        "zh": "Page personnelle",
        "dz": "Page personnelle",
        "shy": "Page personnelle",
        "de": "Page personnelle"
    },
    "ti_playIpa": {
        "fr": "Écouter !",
        "en": "Listen!",
        "zh": "Écouter !",
        "dz": "Écouter !",
        "shy": "Écouter !",
        "de": "Écouter !"
    },
    "debitParole": {
        "fr": "Cliquez pour modifier la vitesse (0-150%)",
        "en": "Click to change speech rate (0-150%)",
        "zh": "Cliquez pour modifier la vitesse (0-150%)",
        "dz": "Cliquez pour modifier la vitesse (0-150%)",
        "shy": "Cliquez pour modifier la vitesse (0-150%)",
        "de": "Cliquez pour modifier la vitesse (0-150%)"
    },
    "ti_savePage": {
        "fr": "Enregistrer cette page",
        "en": "Save this page",
        "zh": "Enregistrer cette page",
        "dz": "Enregistrer cette page",
        "shy": "Enregistrer cette page",
        "de": "Enregistrer cette page"
    },
    "ti_newPage": {
        "fr": "Ouvrir une nouvelle page",
        "en": "Open a new window",
        "zh": "Ouvrir une nouvelle page",
        "dz": "Ouvrir une nouvelle page",
        "shy": "Ouvrir une nouvelle page",
        "de": "Ouvrir une nouvelle page"
    },
    "ti_copy": {
        "fr": "Copier/Coller le contenu",
        "en": "Copy/paste this content",
        "zh": "Copier/Coller le contenu",
        "dz": "Copier/Coller le contenu",
        "shy": "Copier/Coller le contenu",
        "de": "Copier/Coller le contenu"
    },
    "ti_changeBgColor": {
        "fr": "Changer la couleur du fond",
        "en": "Change background color",
        "zh": "Changer la couleur du fond",
        "dz": "Changer la couleur du fond",
        "shy": "Changer la couleur du fond",
        "de": "Changer la couleur du fond"
    },
    "div2png": {
        "fr": "Exporter comme image",
        "en": "Export as an image",
        "zh": "Exporter comme image",
        "dz": "Exporter comme image",
        "shy": "Exporter comme image",
        "de": "Exporter comme image"
    },
    "btnContentEditable": {
        "fr": "(dé)vérrouiller la saisie au clavier",
        "en": "(un)lock keyboard input",
        "zh": "(dé)vérrouiller la saisie au clavier",
        "dz": "(dé)vérrouiller la saisie au clavier",
        "shy": "(dé)vérrouiller la saisie au clavier",
        "de": "(dé)vérrouiller la saisie au clavier"
    },
    "ti_writeEnter": {
        "fr": "Aller à la ligne",
        "en": "Next line",
        "zh": "Aller à la ligne",
        "dz": "Aller à la ligne",
        "shy": "Aller à la ligne",
        "de": "Aller à la ligne"
    },
    "ti_previous": {
        "fr": "Aller à la graphie précédente",
        "en": "Go to previous",
        "zh": "Aller à la graphie précédente",
        "dz": "Aller à la graphie précédente",
        "shy": "Aller à la graphie précédente",
        "de": "Aller à la graphie précédente"
    },
    "ti_next": {
        "fr": "Aller à la graphie suivante",
        "en": "Got to next",
        "zh": "Aller à la graphie suivante",
        "dz": "Aller à la graphie suivante",
        "shy": "Aller à la graphie suivante",
        "de": "Aller à la graphie suivante"
    },
    "ti_last": {
        "fr": "Aller à la dernière graphie",
        "en": "Go to last",
        "zh": "Aller à la dernière graphie",
        "dz": "Aller à la dernière graphie",
        "shy": "Aller à la dernière graphie",
        "de": "Aller à la dernière graphie"
    },
    "ti_maj": {
        "fr": "Majuscules/minuscules",
        "en": "Upper/lower-case",
        "zh": "Majuscules/minuscules",
        "dz": "Majuscules/minuscules",
        "shy": "Majuscules/minuscules",
        "de": "Majuscules/minuscules"
    },
    "ti_minClav": {
        "fr": "Réduire le clavier",
        "en": "Get the keyboard smaller",
        "zh": "Réduire le clavier",
        "dz": "Réduire le clavier",
        "shy": "Réduire le clavier",
        "de": "Réduire le clavier"
    },
    "ti_maxClav": {
        "fr": "Agrandir le clavier",
        "en": "Get the keyboard bigger",
        "zh": "Agrandir le clavier",
        "dz": "Agrandir le clavier",
        "shy": "Agrandir le clavier",
        "de": "Agrandir le clavier"
    },
    "selectPanneau": {
        "fr": "Changer de panneau phonologique",
        "en": "Choose the phonetic chart",
        "zh": "Changer de panneau phonologique",
        "dz": "Changer de panneau phonologique",
        "shy": "Changer de panneau phonologique",
        "de": "Changer de panneau phonologique"
    },
    "selectFidel": {
        "fr": "Changer de liste des graphies",
        "en": "Choose the spelling list",
        "zh": "Changer de liste des graphies",
        "dz": "Changer de liste des graphies",
        "shy": "Changer de liste des graphies",
        "de": "Changer de liste des graphies"
    },
    "ti_toggleFullScreen": {
        "fr": "Mode plein écran",
        "en": "Full screen mode",
        "zh": "Full screen mode",
        "dz": "Full screen mode",
        "shy": "Full screen mode",
        "de": "Full screen mode"
    },
    "ti_checkWord": {
        "fr": "Vérifier dans le dictionnaire de WikiColor",
        "en": "Look up in WikiColor dictionary",
        "zh": "Look up in WikiColor dictionary",
        "dz": "Look up in WikiColor dictionary",
        "shy": "Look up in WikiColor dictionary",
        "de": "Look up in WikiColor dictionary"
    },
    "sp_retour": {
        "fr": "Retour",
        "en": "Back",
        "zh": "Retour",
        "dz": "Retour",
        "shy": "Retour",
        "de": "Retour"
    },
    "sp_consignePlayer": {
        "fr": "Pointez le mot que vous entendez",
        "en": "Point the word you hear",
        "zh": "Pointez le mot que vous entendez",
        "dz": "Pointez le mot que vous entendez",
        "shy": "Pointez le mot que vous entendez",
        "de": "Pointez le mot que vous entendez"
    },
    "sp_demarrerPlayer": {
        "fr": "Démarrer !",
        "en": "Start!",
        "zh": "Démarrer !",
        "dz": "Démarrer !",
        "shy": "Démarrer !",
        "de": "Démarrer !"
    },
    "sp_validerPlayer": {
        "fr": "Valider",
        "en": "Confirm",
        "zh": "Valider",
        "dz": "Valider",
        "shy": "Valider",
        "de": "Valider"
    },
    "sp_continuerPlayer": {
        "fr": "Continuer",
        "en": "Continue",
        "zh": "Continuer",
        "dz": "Continuer",
        "shy": "Continuer",
        "de": "Continuer"
    },
    "sp_nextWordPlayer": {
        "fr": "Mot suivant",
        "en": "Next word",
        "zh": "Mot suivant",
        "dz": "Mot suivant",
        "shy": "Mot suivant",
        "de": "Mot suivant"
    },
    "sp_reponsesPlayer": {
        "fr": "Réponses",
        "en": "Show answers",
        "zh": "Réponses",
        "dz": "Réponses",
        "shy": "Réponses",
        "de": "Réponses"
    },
    "sp_reponsesPlayerBravoDiv": {
        "fr": "Réponses possibles",
        "en": "Show all correct answers",
        "zh": "Réponses possibles",
        "dz": "Réponses possibles",
        "shy": "Réponses possibles",
        "de": "Réponses possibles"
    },
    "sp_writeWordPlayer": {
        "fr": "Écrivez un mot à pointer :",
        "en": "Write a word to point:",
        "zh": "Écrivez un mot à pointer :",
        "dz": "Écrivez un mot à pointer :",
        "shy": "Écrivez un mot à pointer :",
        "de": "Écrivez un mot à pointer :"
    },
    "sp_tryPlayer": {
        "fr": "OK",
        "en": "Try!",
        "zh": "OK",
        "dz": "OK",
        "shy": "OK",
        "de": "OK"
    },
    "sp_randomWordPlayer": {
        "fr": "Mot aléatoire !",
        "en": "Random word!",
        "zh": "Mot aléatoire !",
        "dz": "Mot aléatoire !",
        "shy": "Mot aléatoire !",
        "de": "Mot aléatoire !"
    },
    "sp_editDicoPlayer": {
        "fr": "Éditer le dictionnaire",
        "en": "Edit the dictionary",
        "zh": "Éditer le dictionnaire",
        "dz": "Éditer le dictionnaire",
        "shy": "Éditer le dictionnaire",
        "de": "Éditer le dictionnaire"
    },
    

}