################
#
# phonoWalker.py
#
# Views complémentaires pour les séries automatiques
#
import re, random

# First read Britfon dictionary
# https://github.com/JoseLlarena/Britfone/tree/master

vowelList = ["æ", "ɐ", "ɒ", "ɛ", "i", "ɪ", "ʊ", "ɜː", "ɔː", "iː", "ɑː", "uː", "aɪ", "aʊ", "eɪ", "əʊ", "ɛə", "ɪə", "ɔɪ", "ʊə", "ə"]

lang2listphon = {} # contiendra la liste des phonèmes par langue

# fonction d'initialisation de phonWeights qui se remplira automatiquement à la lecture du dictionnaire
def initLang2phonWeights():
    lang2phonWeights = {
        "fr":{},
        "en":{}
    }
    return lang2phonWeights

lang2phonWeights = initLang2phonWeights()

def getnSyll(t):
    nsyll = 0
    for phon in t:
        if phon.replace("ˈ","").replace("ˌ","") in vowelList:
            nsyll+=1
    return nsyll

# ENGLISH DICTIONARY (WORD FREQUENCY)
def parseCELEXwordfreq():
    print("Parsing CELEX EFW...")
    # 1.    IdNum
    # 2.    Word
    # 3.    IdNumLemma
    # 4.    Cob     raw frequency among 17,900,000 words
    # 5.    CobDev
    # 6.    CobMln
    # 7.    CobLog  <<--- let's take this one 0-6 (log10 of CobMln, full corpus)
    # 8.    CobW
    # 9.    CobWMln
    # 10.   CobWLog
    # 11.   CobS     spoken frequency 1-1,300,000 → 60 = the word appears 60 times among a corpus of 1300000 words
    # 12.   CobSMln  spoken frequency 1-1,000,000
    # 13.   CobSLog  spoken frequency 0-6 (log10 of CoCobSMlnbS)
    word2freq = {}
    with open("../media/dictionaries/CELEX_dictionary_EFW.CD", "r") as infile:
        infile.readline()
        for line in infile:
            line = line.strip()
            l = line.split('\\')
            word = l[1]
            freq = l[6]

            if word not in word2freq.keys():
                word2freq[word] = 0
            if float(freq) > word2freq[word]:
                word2freq[word] += float(freq)
    
    return word2freq

# ENGLISH DICTIONARY (PHONOLOGY)
def parseBritfone():
    print("Parsing Britfone dictionary...")
    word2trans = {}
    cptt = 0
    listphon = []
    with open('../media/dictionaries/britfone.main.3.0.1.csv', 'r') as infile:
        for line in infile:
            line = line.strip()
            l = line.split(',')
            
            n = re.sub(r'\(\d+\)', '', l[0])
            w = n.lower().strip()
            t = l[1].strip().split(' ')

            if w not in word2trans.keys():
                word2trans[w] = []
            
            if t not in word2trans[w]:
                word2trans[w].append(t)
                cptt += 1
                for car in t:
                    # car = car.replace("ˌ","").replace("ˈ","")
                    if car not in listphon:
                        listphon.append(car)

    return word2trans, listphon


def makeTrans2wordsEnglish():
    print("Compiling phonological dictionary from Britfone...")
    trans2words = {}
    with open('../media/dictionaries/britfone.main.3.0.1.csv', 'r') as infile:
        for line in infile:
            line = line.strip()
            l = line.split(',')
            t = l[1].strip()
            n = re.sub(r'\(\d+\)', '', l[0])
            w = n.lower()

            if t not in trans2words.keys():
                trans2words[t] = []

            if w not in trans2words[t]:
                trans2words[t].append(w)
    
    return trans2words


def makeTrans2freqWordsEnglish():
    lang = "en"
    word2freq = parseCELEXwordfreq()
    word2trans, lang2listphon[lang] = parseBritfone()
    trans2words = makeTrans2wordsEnglish()
    trans2freqWords = {}
    for tstr, ww in trans2words.items():
        t = tstr.split(" ")
        for phon in t:
            if phon not in lang2phonWeights[lang].keys():
                lang2phonWeights[lang][phon] = 0
            lang2phonWeights[lang][phon]+=1

        maxFreq = 0
        for w in ww:
            if w in word2freq.keys() and word2freq[w]>maxFreq:
                maxFreq = word2freq[w]
        
        trans2freqWords[tstr] = [maxFreq, ww]
    return trans2freqWords


def makeTrans2freqWordsFrench():
    lang = "fr"
    trans2freqWords = {}
    lang2listphon[lang] = []
    print("Lecture de Lexique383 et compilation dictionnaire...")

    with open('../media/dictionaries/Lexique383_formated.csv', 'r') as infile:
        for line in infile:
            line = line.strip()
            l = line.split(';')
            if len(l)==3:
                ortho, api, freq = l
                
                for phon in api:
                    if phon not in lang2listphon[lang]:
                        lang2listphon[lang].append(phon)

                    if phon not in lang2phonWeights[lang].keys():
                        lang2phonWeights[lang][phon] = 0
                    lang2phonWeights[lang][phon]+=1

                if api not in trans2freqWords.keys():
                    trans2freqWords[api] = [freq, [ortho]]
                else:
                    if freq > trans2freqWords[api][0]:
                        trans2freqWords[api][0] = freq
                    if ortho not in trans2freqWords[api][1]:
                        trans2freqWords[api][1].append(ortho)

    return trans2freqWords


lang2trans2freqWords = {
    "fr": makeTrans2freqWordsFrench(),
    "en": makeTrans2freqWordsEnglish()
}


def subphon(current_phonlist, lang, iErreur=""):
    altfreqlist = []
    for i,phon in enumerate(current_phonlist):
        if i == iErreur:
            # Skip if this phon should be maintained in the alteration
            continue
        for altenative in lang2listphon[lang]:
            if altenative == phon:
                continue
            alttrans = current_phonlist[:i] + [altenative] + current_phonlist[i+1:]
            if " ".join(alttrans) in lang2trans2freqWords[lang].keys():
                altfreqlist.append([ " ".join(alttrans) ] + lang2trans2freqWords[lang][" ".join(alttrans)] )
    return altfreqlist


def addphon(current_phonlist, lang, iErreur=""):
    altfreqlist = []
    for i,phon in enumerate(current_phonlist+[""]):
        if i == iErreur:
            # Skip if this phon should be maintained in the alteration
            continue
        for altenative in lang2listphon[lang]:
            if altenative == phon:
                continue
            alttrans = current_phonlist[:i] + [altenative] + current_phonlist[i:]
            if " ".join(alttrans) in lang2trans2freqWords[lang].keys():
                altfreqlist.append( [" ".join(alttrans)] + lang2trans2freqWords[lang][" ".join(alttrans)] )
    return altfreqlist


def delphon(current_phonlist, lang, iErreur=""):
    altfreqlist = []
    for i,phon in enumerate(current_phonlist):
        if i == iErreur:
            # Skip if this phon should be maintained in the alteration
            continue
        alttrans = current_phonlist[:i] + current_phonlist[i+1:]
        if " ".join(alttrans) in lang2trans2freqWords[lang].keys():
            altfreqlist.append([ " ".join(alttrans)]+ lang2trans2freqWords[lang][" ".join(alttrans)] )
    return altfreqlist



def fromTransAndHistGetNext(previousTranscription, myHistory, lang):
    xFirstCandidates = 6 # Select one random candidate from this number among sorted potential next candidates
    history = [ w for x in myHistory for w in x["words"]]
    
    current_phonlist = previousTranscription

    ### PRÉPARATION DES CANDIDATS POSSIBLES POUR LA SUITE

    # Altération
    altfreqlist = addphon(current_phonlist, lang)
    altfreqlist+= subphon(current_phonlist, lang)
    
    nextOptions = []
    for alt, freq, words in sorted(altfreqlist, key=lambda t:t[1], reverse=True):
        if not any(word in words for word in history):
            nextOptions.append([alt, freq, words])
            print(alt, freq, words)
    
    # If no word in nextOptions, try more alterations
    if len(nextOptions)==0:
        print("Searching for more options...")
        i = 0
        while len(nextOptions)==0 and i >= len(current_phonlist):
            i+=1
            altfreqlist += subphon(current_phonlist[:-i], lang) 
        # add deletion of phoneme as well
        altfreqlist += delphon(current_phonlist, lang)

        # if len(nextOptions)>0:
        for alt, freq, words in sorted(altfreqlist, key=lambda t:t[1], reverse=True):
            if not any(word in words for word in history):
                nextOptions.append([alt, freq, words])
                print(alt.replace(" ",""), freq, words)


    # Sélection du mot suivant
    if len(nextOptions)>0:
        while len(nextOptions) < xFirstCandidates:
            xFirstCandidates -= 1
        randomCandidate = random.randrange(xFirstCandidates)-1 # Select random from xFirstCandidates first candidates
        nextword = nextOptions[randomCandidate][0].split(" ") 
        print("Next:",nextword, nextOptions[randomCandidate][2])
    else:
        print("Nothing next! Call previous word in history...")
        return 0
    
    print(nextword)
    print(lang2trans2freqWords[lang][' '.join(nextword)])
    return [nextword, lang2trans2freqWords[lang][' '.join(nextword)][1]]



def getWordsFromTrans(apiList, lang):
    return [apiList, lang2trans2freqWords[lang][' '.join(apiList)][1]]


def getLang2phonWeights(lang):
    return lang2phonWeights[lang]