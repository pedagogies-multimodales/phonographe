import json,re

### il y a tantôt des caractères api (Brooks) tantôt des classes (Fidels scsv)
### on veut des caractères api en clé pour le scsv final
api2class = {}
with open('../../../phon2graph/data/api2class.json','r') as filein:
	api2class = json.load(filein)
	
# inversion de api2class
class2api = {}
for api,cla in api2class.items():
	if cla not in class2api.keys():
		class2api[cla] = api
	else:
		print("Classe déjà existante pour",cla,api)

# ajouts de clés
class2api["phon_schwi"] = 'ɪ'
class2api["phon_schwa"] = 'ə'
class2api["phon_schwu"] = 'u'
class2api["phon_ju"] = 'ju'

##
## Ça commence ici :

fidelpsukPath = "fidel_pronsciEnBr.json"
fidelpsusPath = "fidel_pronsciEnUs.json"
fidelbrooksukPath = "Fidel_en_Brooks.csv"

#########################################
fidelpsuk = {}
with open(fidelpsukPath,'r') as filein:
	fidelpsuk = json.load(filein)
cpti = 0
cptj = 0
for i,j in fidelpsuk.items():
	cpti+=1
	for spell in j:
		cptj+=1
print(cpti,"phonemes and",cptj,"spellings detected for Fidel PronSci UK.")
	
	
########################################
fidelpsus = {}
with open(fidelpsusPath,'r') as filein:
	fidelpsus = json.load(filein)
cpti = 0
cptj = 0
for i,j in fidelpsus.items():
	cpti+=1
	for spell in j:
		cptj+=1
print(cpti,"phonemes and",cptj,"spellings detected for Fidel PronSci US.")


#########################################
fidelbrooksuk = {}
with open(fidelbrooksukPath,'r') as filein:
	for line in filein:
		line = line.strip()
		l = line.split(';')
		if len(l) == 9:
			# ci;∫;M;100;;audacious;magician;specious;commercial
			if len(l[1])>0:
				if l[1] not in fidelbrooksuk.keys():
					fidelbrooksuk[l[1]] = []
				fidelbrooksuk[l[1]].append(l[0])
cpti = 0
cptj = 0
for i,j in fidelbrooksuk.items():
	cpti+=1
	for spell in j:
		cptj+=1
print(cpti,"phonemes and",cptj,"spellings detected for Fidel Brooks UK.")
	
######################
######################			
# MERGING

megaFidel = {}
for phon,transs in fidelbrooksuk.items():
	if phon not in megaFidel.keys():
		megaFidel[phon] = []
	for trans in transs:
		if trans not in megaFidel[phon] and not re.match(r'.*\..*',trans):
			megaFidel[phon].append(trans)

for phon,lines in fidelpsuk.items():
	phon = class2api[phon]
	if phon not in megaFidel.keys():
		megaFidel[phon] = []
	for line in lines:
		for trans in line:
			if trans[0] not in megaFidel[phon] and len(trans)==3 and trans[2]>0:
				megaFidel[phon].append(trans[0])

for phon,lines in fidelpsus.items():
	print(phon)
	phon = class2api[phon]
	print(phon)
	if phon not in megaFidel.keys():
		megaFidel[phon] = []
	for line in lines:
		for trans in line:
			if trans[0] not in megaFidel[phon] and len(trans)==3 and trans[2]>0:
				megaFidel[phon].append(trans[0])


cpti = 0
cptj = 0
for i,j in megaFidel.items():
	print(i)
	cpti+=1
	for spell in j:
		print('\t',spell)
		cptj+=1
print(cpti,"phonemes and",cptj,"spellings for the merged Fidel.")


print(fidelpsus["phon_schwar"])
print(megaFidel["əɹ"])
# EXPORT
with open('fidel_globalEnglish.scsv','w') as outfile:
	for i,j in megaFidel.items():
		outfile.write("{}:".format(i))
		for index,trans in enumerate(j):
			outfile.write("{}".format(trans))
			if index == len(j)-1:
				outfile.write("\n")
			else:
				outfile.write(",")

