###############################################
# Générateur de Fidel à parti d'un fichier txt
#
# EXEMPLE DE FORMAT DU FICHIER EN ENTRÉE :
#     !phon_t
#     t3 ts tt t’ pt th
#     te _ tte _ pte the
#     tes _ ttes _ ptes thes
#     tent _ ttent _ ptent ths
#     cht _ chts _ ‿3 d
#    
#     !phon_d
#     d3 dd d’
#     de ds _
#     des _ _
#     dent _ _
#
# (Indiquer les trous par "_" ; indiquer le style (taille etc) par un numéro)




# Nom du fichier en entrée (sans extension)
fileName = "fidel_do"

# Style par défaut (pour toutes les graphies sans indication de style)
style_default = 1





import re
import json

fidel = {}

with open(fileName+'.txt', 'r') as f:
    search = re.findall(r'!(phon_\w+)\n([^!]*)\n',f.read())
    for i in search:
        phon = i[0]
        graphList = i[1]
        fidel[phon] = []

        graphList = graphList.split('\n')
        for line in graphList:
            if len(line) > 0:
                row = []
                line = line.split(' ')
                for graph in line:
                    graph = graph.replace("'","’")
                    if graph == '_':
                        g = ''
                        t = 0
                        v = 0
                        row.append((g,t,v))
                    elif len(graph) > 0:
                        searchphon = re.match(r'.*\$(phon_.+)',graph)
                        if searchphon:
                            p = searchphon.group(1)
                            graph = graph.replace("$"+p,'')

                        searchtaille = re.match(r'\D+(\d)',graph)
                        if searchtaille:
                            g = graph[:-1]
                            t = int(searchtaille.group(1))
                        else:
                            g = graph
                            t = style_default
                        v = 1

                        if searchphon:
                            row.append((g,t,v,p))
                        else:
                            row.append((g,t,v))
                        
                fidel[phon].append(row)
            

for i,j in fidel.items():
    print(i)
    for x in j:
        print(x)

with open(fileName+'.json', 'w') as outf:
    json.dump(fidel, outf, ensure_ascii=False)
                

