!phon_i_long
e2 ee2 ea2
ie2 _ _
ei eo ey
ae oe ay
i _ _

!phon_i_majarobase
ear2 eer2 ere2 _
ea ee e eo
ier eir ir e're
ie _ _ _

!phon_i_maj
i2 y2 _
ei e ee
ie o u

!phon_ei_maj
a2 ai2 ay2 _ _
ea ei ey e _
ee eigh et er ez
ae aigh ao au eh

!phon_earobase
air2 are2 ear2 _ _
ai a ea ar ae
eir ere heir er ayer
ey're e hei aire ayor

!phon_e
e2 ea2 a2 ai2
ei eo ae ay
ie oe u ieu$phon_ef

!phon_au_maj
ow2 ou2
_ hou
_ ough

!phon_cbrack
a2 _
ai i
ah al

!phon_3_long
er2 ir2 or2 ur2
ear _ _ _
ere irr our urr
err yrrh yr olo
eur eu _ _

!phon_arobaseu_maj
o2 oa2 oe2 ow2
ou au eau owe
ough oo aoh ew
ol ot eo oh

!phon_u_long
ue2 ou2 oo2
u2 ew2 o2
ui eu oe
ough oeu oup
wo _ _

!phon_u_majarobase
our oor ure
ou oo u
ou're uo eu

!phon_ju_majarobase
ure
u
eu

!phon_ju_long
u2 ue2 ew2
ui eu ewe
ut eue eau

!phon_u_maj
u2 oo2
ou o
_ or

!phon_o_maj_long
or2 oor2 aw2 al2 a2 _
ore2 our2 ar2 au2 ough2 _
o ou awe aor augh _
oo oar ort aur ure _
ou're oa orps aul u uo

!phon_o_maji_maj
oi2 oy2
aw _

!phon_q_maj
o2 a2 _ _
ou au ho _
ow eau oh e

!phon_a_maj_long
ar2 a2 al2 _
_ _ _ _
are au ear er
arre aar arrh our
aa ah _ _

!phon_ai_maj
i2 igh2 ie2 y2
ei eigh eye ye
ais ae aye ay
is ai I _

!phon_wa_maj_long
oir oi
oire ois

!phon_wai_maj
oi

!phon_wv_maj
o

!phon_v_maj
u2 ou2 o2
_ oe oo

!phon_j
y2 i r j ll
u2$phon_ju_long ue2$phon_ju_long ew2$phon_ju_long _ ure$phon_ju_majarobase
ui$phon_ju_long eu$phon_ju_long ewe$phon_ju_long _ u$phon_ju_majarobase
ut$phon_ju_long eue$phon_ju_long eau$phon_ju_long _ eu$phon_ju_majarobase

!phon_r_slash
r2 rr2 wr2 ◌r2 rh re

!phon_w
w2 wh2 o ou ju u
_ _ _ oir$phon_wa_maj_long oi$phon_wa_maj_long oi$phon_wai_maj
_ _ _ oire$phon_wa_maj_long ois$phon_wa_maj_long o$phon_wv_maj

!phon_p
p2 pe2
pp2 ppe
ph pt
gh bp

!phon_b
b2 be2
bb2 _
bu pb

!phon_m
m2 me2 _
mm2 _ _
mn mb _
'm gm m$phon_arobasem

!phon_arobasem
m

!phon_f
f2 fe2
ff2 ffe
ph2 ft
gh ve

!phon_v
v2 ve2
vv 've
f _

!phon_t_maj
th2
h

!phon_d_maj
th2
the

!phon_t
t2 te2 ed2 _
tt2 tte d _
pt bt ct z$phon_ts
't th cht zz$phon_ts

!phon_d
d2 de2 ed2
dd _ _
'd ld dh

!phon_n
_ n2 ne2 kn2
_ nn2 _ gn2
_ nd ◌n pn
n$phon_arobasen dne gne mn

!phon_arobasen
n

!phon_ts_maj
ch2 tch2 t2
che tsch c

!phon_dz_maj
j2 dge2 g2
dj dg ge
ch d gg

!phon_l
l2 le2 _
ll2 _ ◌l
'll lle ◌ll
'll$phon_arobasel le$phon_arobasel l$phon_arobasel

!phon_arobasel
'll le l

!phon_s
s2 se2 c2 ce2
ss2 sse sc sce
sw st cc tz
's sth ps z

!phon_z
s2 se2 z2 ze2
ss ts cs zz
's 's$phon_schwiz 's$phon_schwaz x

!phon_schwaz
's

!phon_schwiz
's

!phon_h
h2
wh

!phon_ts
z
zz

!phon_s_maj
sh2 t2
s ss
ch sc
che sch

!phon_z_maj
s2 _
ge t
z j

!phon_ks
x2
xe
cc
xc

!phon_gz
x
xh

!phon_k
x2$phon_ks k2 ke2 c2 ck2
xe$phon_ks kk qu cc ch
cc$phon_ks kh que cu che
xc$phon_ks _ cqu _ cch
x$phon_ks_maj x$phon_gz_maj qu$phon_kw cqu$phon_kw cu$phon_kw

!phon_g
g2 gg2
gu gh
gue ckgu

!phon_kw
qu2
cqu
cu

!phon_n_maj
ng2 n2
ngue nd

!phon_ks_maj
x

!phon_gz_maj
x

!phon_schwi
a2 e2 ee2 ey2 i2 y2
ae ay ei ia is ois
ai ea hi ie oe u

!phon_schwa
a2 ar2 _ e2 _ er2 i2 io2
ae ai au eau eo ere ha hi
ah are ea ei eou eur he ia
o2 or2 ou2 our2 re2 u2 ure2 _
ie iou o' oi ough ro y _
ier ir oar oo r ur yr _

!phon_schwju
eu
u
uu

!phon_schwu
eu$phon_schwju u2
u$phon_schwju ou
uu$phon_schwju uo
