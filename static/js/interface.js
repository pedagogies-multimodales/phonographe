const thisURL = window.location.href;
console.log("url:",thisURL);

var thisPageLang = "";
let famille = "bouches";


// set page target language
function setLangFromUrl() {
    var pageLang = thisURL.match(/.*\/(fr|en|zh|dz|shy|de)/);
    if (pageLang) { 
        console.log("Langue indiquée par l'url:",pageLang[1]);
        thisPageLang = pageLang[1];
        if (pageLang[1]=="fr") selectLang("fr");
        if (pageLang[1]=="en") selectLang("en");
        if (pageLang[1]=="zh") selectLang("zh");
        if (pageLang[1]=="dz") selectLang("dz");
        if (pageLang[1]=="shy") selectLang("shy");
        if (pageLang[1]=="de") selectLang("de");
    } else { 
        console.log("Chargement langue par défaut (fr)");
        thisPageLang = "fr"
        selectLang("fr");
    }
}

function selectLang(lang, p="default", f="default"){
    console.log('SelectLang()',lang);
    document.getElementById('choixLang').value = lang;
    var phonolist = document.getElementById('selectPanneau');
    var fidellist = document.getElementById('selectFidel');
    
    phonolist.innerHTML = '';
    fidellist.innerHTML = '';
    if (lang=="fr"){
        var phonoFrDo = document.createElement('option');
        phonoFrDo.value = "phonoFrDo";
        phonoFrDo.innerHTML = "Panneaux Fr A. Do";
        phonolist.appendChild(phonoFrDo);

        var phonoFrKin = document.createElement('option');
        phonoFrKin.value = "svgFrKinephones";
        phonoFrKin.innerHTML = "Panneau Fr Kinéphones";
        phonolist.appendChild(phonoFrKin);

        var phonoFrGat = document.createElement('option');
        phonoFrGat.value = "svgFrGattegno";
        phonoFrGat.innerHTML = "Panneau Fr Gattegno";
        phonolist.appendChild(phonoFrGat);

        var newFid = document.createElement('option');
        newFid.value = "fidelPS";
        newFid.innerHTML = "Fidel PronSci";
        fidellist.appendChild(newFid);

        var newFid = document.createElement('option');
        newFid.value = "fidelDo";
        newFid.innerHTML = "Fidel A. Do";
        fidellist.appendChild(newFid);

        if (pageId == '0') window.history.pushState("", "", "/fr");
        interface("fr");


    } else if (lang=="en"){
        var phonoEnAlem = document.createElement('option');
        phonoEnAlem.value = "phonoEnAlem";
        phonoEnAlem.innerHTML = "ALeM British";
        phonolist.appendChild(phonoEnAlem);

        var phonoEnPronSciBr = document.createElement('option');
        phonoEnPronSciBr.value = "phonoEnPronSciBr";
        phonoEnPronSciBr.innerHTML = "PronSci British";
        phonolist.appendChild(phonoEnPronSciBr);

        var newFid = document.createElement('option');
        newFid.value = "fidelEnPSUK";
        newFid.innerHTML = "PronSci British";
        fidellist.appendChild(newFid);

        if (pageId == '0') window.history.pushState("", "", "/en");
        interface("en");

    } else if (lang == "zh") {
        var phonoZhJiao = document.createElement('option');
        phonoZhJiao.value = "phonoZhJi";
        phonoZhJiao.innerHTML = "Mandarin Jiao";
        phonolist.appendChild(phonoZhJiao);

        var phonoZhP7 = document.createElement('option');
        phonoZhP7.value = "phonoZhP7";
        phonoZhP7.innerHTML = "Mandarin LLCER";
        phonolist.appendChild(phonoZhP7);

        var newFid = document.createElement('option');
        newFid.value = "cedict";
        newFid.innerHTML = "CC-CEDICT";
        fidellist.appendChild(newFid);

        if (pageId == '0') window.history.pushState("", "", "/zh");
        interface("zh");

    } else if (lang == "dz") {
        var phonoDzSb2 = document.createElement('option');
        phonoDzSb2.value = "phonoDzSb2";
        phonoDzSb2.innerHTML = "Arabe algérien S. Benbouaziz v2";
        phonolist.appendChild(phonoDzSb2);

        var phonoDzSb3 = document.createElement('option');
        phonoDzSb3.value = "phonoDzSb3";
        phonoDzSb3.selected = "True";
        phonoDzSb3.innerHTML = "Arabe algérien S. Benbouaziz v3";
        phonolist.appendChild(phonoDzSb3);

        var newFid = document.createElement('option');
        newFid.value = "fidelSb";
        newFid.innerHTML = "Fidel S. Benbouaziz";
        fidellist.appendChild(newFid);

        if (pageId == '0') window.history.pushState("", "", "/dz");
        interface("dz");

    } else if (lang == "shy") {
        // var phonoShypSb4 = document.createElement('option');
        // phonoShypSb4.value = "phonoShypSb4";
        // phonoShypSb4.innerHTML = "Chaoui S. Benbouaziz v4";
        // phonolist.appendChild(phonoShypSb4);

        var phonoShyfSb4 = document.createElement('option');
        phonoShyfSb4.value = "phonoShyfSb4";
        phonoShyfSb4.selected = "True";
        phonoShyfSb4.innerHTML = "Chaoui Fidel S. Benbouaziz v4";
        phonolist.appendChild(phonoShyfSb4);

        if (pageId == '0') window.history.pushState("", "", "/shy");
        interface("shy");
    
    } else if (lang == "de") {
        var phonoDeAlem = document.createElement('option');
        phonoDeAlem.value = "phonoDeAlem";
        phonoDeAlem.innerHTML = "ALeM Deutsch v05b";
        phonolist.appendChild(phonoDeAlem);

        var fidelDeAlem = document.createElement('option');
        fidelDeAlem.value = "fidelDeAlem";
        fidelDeAlem.innerHTML = "Fidel ALeM Deutsch";
        fidellist.appendChild(fidelDeAlem);

        if (pageId == '0') window.history.pushState("", "", "/de");
        interface("de");
    }
    selectPanneau(p);
    selectFidel(f);
    addStat("phonographe",lang);
}

function selectPanneau(p){
    console.log("selectPanneau",p,thisPageLang);
    if (p=="default" && thisPageLang=="fr") {
        p = "phonoFrDo";
    } else if (p=="default" && thisPageLang=="en") {
        p = "phonoEnAlem";
    } else if (p=="default" && thisPageLang=="zh") {
        p = "phonoZhJi"
    } else if (p=="default" && thisPageLang=="dz") {
        p = "phonoDzSb3"
    } else if (p=="default" && thisPageLang=="shy") {
        if (modeGraphies==0) p = "phonoShypSb4";
        else p = "phonoShyfSb4";
    } else if (p=="default" && thisPageLang=="de") {
        p = "phonoDeAlem";
    }

    var svgFrKinephones = document.getElementById('svgFrKinephones'); // Panneau FR Kinephones
    var svgFrGattegno = document.getElementById('svgFrGattegno'); // Panneau FR Gattegno
    var svgEnPronSciBr = document.getElementById('svgEnPronSciBr'); // Panneau EN PronSci
    
    var svgEnAlem = document.getElementById('svgEnAlem'); // Panneau EN ALeM (fond couleurs)
    var pngCalqEnAlemLignes = document.getElementById('pngCalqEnAlemLignes'); // Panneau EN ALeM (lignes)
    var pngPochoirEnAlem = document.getElementById('pngPochoirEnAlem'); // Panneau EN ALeM (formes bouches)
    var svgClickEnAlem = document.getElementById('svgClickEnAlem'); // Panneau EN ALeM (zones clickables)

    var svgClick = document.getElementById('svgClick'); // svg clickable de surface pour Fr A DO
    var pngCalq = document.getElementById('pngCalq'); // png calque 04 pour Fr A Do
    var pngPochoir = document.getElementById('pngPochoir'); // png calque 01 pour Fr A Do
    var svgFond = document.getElementById('svgFond'); // svg fond couleurs pour Fr A Do
    var doCalques = document.getElementById('doCalques'); // boutons calques

    var svgZhLy = document.getElementById('svgZhLy'); // Panneau Mandarin Lyssenko
    var svgZhMa = document.getElementById('svgZhMa'); // Panneau Mandarin Aurélie Mariscalchi
    var svgZhJi = document.getElementById('svgZhJi'); // Panneau Mandarin Shuman Jiao
    var svgZhP7 = document.getElementById('svgZhP7'); // Panneau Mandarin P7 2021

    var svgDzSb2 = document.getElementById('svgDzSb2'); // Panneau Arabe Algérien Sarra Benbouaziz v2
    var svgDzSb3 = document.getElementById('svgDzSb3'); // Panneau Arabe Algérien Sarra Benbouaziz v3

    var svgShypSb4 = document.getElementById('svgShypSb4'); // Panneau Chaoui Sarra Benbouaziz v4
    var svgShyfSb4 = document.getElementById('svgShyfSb4'); // Panneau Fidel Chaoui Sarra Benbouaziz v4

    var svgBackDeAlem = document.getElementById('svgBackDeAlem'); // Panneau DE ALeM (fond couleurs)
    var pngPochoirDeAlem = document.getElementById('pngPochoirDeAlem'); // Panneau DE ALeM (formes bouches)
    var svgClickDeAlem = document.getElementById('svgClickDeAlem'); // Panneau DE ALeM (zones clickables)

    function resetPanneaux() {

        // INTERLANGUE
        famille = "formes";

        // FR DO
        doCalques.style.display = 'none';
        pngCalq.style.display = 'none';
        svgClick.style.display = 'none';
        pngPochoir.style.display = 'none';
        svgFond.style.display = 'none';

        // FR Kinéphones
        svgFrKinephones.style.display = 'none';

        // FR Gattegno
        svgFrGattegno.style.display = 'none';

        // EN ALEM
        svgEnAlem.style.display = 'none';
        pngCalqEnAlemLignes.style.display = 'none';
        pngPochoirEnAlem.style.display = 'none';
        svgClickEnAlem.style.display = 'none';
        // EN PronSci British
        svgEnPronSciBr.style.display = 'none';

        // ZH Lyssenko
        svgZhLy.style.display = 'none';
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'none';
        // ZH S. Jiao
        svgZhJi.style.display = 'none';
        // ZH P7
        svgZhP7.style.display = 'none';

        // DZ SB v2
        svgDzSb2.style.display = 'none';
        // DZ SB v3
        svgDzSb3.style.display = 'none';

        // SHY SB v4
        svgShypSb4.style.display = 'none';
        // SHY Fidel SB v4
        svgShyfSb4.style.display = 'none';

        // DE ALEM
        svgBackDeAlem.style.display = 'none';
        pngPochoirDeAlem.style.display = 'none';
        svgClickDeAlem.style.display = 'none';

        if (modeGraphies==1) {
            document.getElementById('graphies').style.display = 'block'
            document.getElementById('clavier').classList.remove('clavierSeul')
            document.getElementById('clavier').classList.add('clavier')
            document.documentElement.style.setProperty('--clavSize', '50%');
            document.documentElement.style.setProperty('--graphSize', '50%');
            showGraphies();
        }

        document.getElementById('selectFidel').style.display = 'block';

    }


    if (p == 'phonoFrDo') {
        resetPanneaux();

        // FR DO
        doCalques.style.display = 'block';
        pngCalq.style.display = 'block';
        svgClick.style.display = 'block';
        pngPochoir.style.display = 'block';
        svgFond.style.display = 'block';

        famille="bouches";

    } else if (p == 'svgFrKinephones') {
        resetPanneaux();

        // FR Kinéphones
        svgFrKinephones.style.display = 'block';

    } else if (p == 'svgFrGattegno') {
        resetPanneaux();

        // FR Gattegno
        svgFrGattegno.style.display = 'block';

    } else if (p == 'phonoEnAlem') {
        resetPanneaux();

        // EN ALEM
        svgEnAlem.style.display = 'block';
        pngCalqEnAlemLignes.style.display = 'block';
        pngPochoirEnAlem.style.display = 'block';
        svgClickEnAlem.style.display = 'block';
    
    } else if (p == 'phonoEnPronSciBr') {
        resetPanneaux();

        // EN PronSci British
        svgEnPronSciBr.style.display = 'block';
    
    } else if (p == 'phonoZhLy') {
        resetPanneaux();

        // ZH Lyssenko
        svgZhLy.style.display = 'block';

    } else if (p == 'phonoZhMa') {
        resetPanneaux();
        
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'block';

    } else if (p == 'phonoZhJi') {
        resetPanneaux();
        
        // ZH S. Jiao
        svgZhJi.style.display = 'block';
        
    } else if (p == 'phonoZhP7') {
        resetPanneaux();

        // ZH P7
        svgZhP7.style.display = 'block';
    } else if (p == 'phonoDzSb2') {
        resetPanneaux();

        // DZ SB v2
        svgDzSb2.style.display = 'block';
    } else if (p == 'phonoDzSb3') {
        resetPanneaux();

        // DZ SB v2
        svgDzSb3.style.display = 'block';
    } else if (p == 'phonoShypSb4') {
        resetPanneaux();

        document.getElementById('modePhono').checked = false;
        document.getElementById('graphies').style.display = 'none'
        document.getElementById('clavier').classList.remove('clavier')
        document.getElementById('clavier').classList.add('clavierSeul')

        document.documentElement.style.setProperty('--clavSize', '60%');
        document.documentElement.style.setProperty('--graphSize', '40%');

        hideGraphies();

        // SHY SB v4
        svgShypSb4.style.display = 'block';

        document.getElementById('selectFidel').style.display = 'none';

    } else if (p == 'phonoShyfSb4') {
        resetPanneaux();

        document.getElementById('modePhono').checked = true;
        document.getElementById('graphies').style.display = 'none'
        document.getElementById('clavier').classList.remove('clavier')
        document.getElementById('clavier').classList.add('clavierSeul')

        document.documentElement.style.setProperty('--clavSize', '60%');
        document.documentElement.style.setProperty('--graphSize', '40%');

        hideGraphies();

        // SHY Fidel SB v4
        svgShyfSb4.style.display = 'block';

        document.getElementById('selectFidel').style.display = 'none';

    } else if (p == 'phonoDeAlem') {
        resetPanneaux();

        // DE Alem
        svgBackDeAlem.style.display = 'block';
        pngPochoirDeAlem.style.display = 'block';
        svgClickDeAlem.style.display = 'block';

        famille="bouches";
    
    }
}

function selectFidel(f){
    console.log("selecFidel",f,thisPageLang);
    if (f=="default" && thisPageLang=="fr") {
        f = "fidelDo";
    } else if (f=="default" && thisPageLang=="en") {
        f = "fidelEnPSUK";
    } else if (f=="default" && thisPageLang=="zh") {
        f = "cedict";
    } else if (f=="default" && thisPageLang=="dz") {
        f = "fidelSb";
    } else if (f=="default" && thisPageLang=="shy") {
        f = "";
    } else if (f=="default" && thisPageLang=="de") {
        f = "fidelDeAlem";
    }
    fidel = dicoFidels[f];
    document.getElementById('selectFidel').value = f;
    if (thisPageLang=="zh") initGraph();
    if (thisPageLang!="zh" && document.getElementById('hanDiv')!=null) document.getElementById('hanDiv').innerHTML = "";
    if (currentPhon && modeGraphies==1 && thisPageLang!="shy" && thisPageLang!="zh") recupPhon(currentPhon);
}

function interface(lang) {
    console.log("Langue d'interface:",lang);
    thisPageLang = lang;
    var langspanList = document.getElementsByClassName("langspan");
    var langtitleList = document.getElementsByClassName("langtitle");

    for (i=0; i<langspanList.length; i++) {
        span = langspanList[i];
        span.innerHTML = langJson[span.id][lang];
    }
    for (i=0; i<langtitleList.length; i++) {
        title = langtitleList[i];
        title.title = langJson[title.id][lang];
    }

    function initInterface() {
        document.getElementById('barreTitre').classList.remove('ms-auto');
        document.getElementById('barreTitre').classList.add('me-auto');
        document.getElementById('ti_maj').style.display = "block";

        document.getElementById('svgEnPronSciBr').style.display = "none";
        document.getElementById('btnSwitchStress').style.display = "none";
        document.getElementById('btnStress1').style.display = "none";
        document.getElementById('btnStress2').style.display = "none";

        document.getElementById('textZone').style.direction = "ltr";
        document.getElementById('textZoneBack').classList.remove('justify-content-end')
        document.getElementById('textZoneBack').classList.add('justify-content-between')

        // Position curseur par défaut : sur la droite
        document.documentElement.style.setProperty('--startPointLtr', 'solid');
        document.documentElement.style.setProperty('--startPointRtl', 'hidden');

        document.getElementById("msg_dz").style.display = "none";

        var punctAr1s = document.querySelectorAll('.punctAr1');
        for (el of punctAr1s) el.style.display = "none";

        var punctAr0s = document.querySelectorAll('.punctAr0');
        for (el of punctAr0s) el.style.display = "block";
    }

    if (lang == "en") {
        initInterface();

        document.getElementById('btnSwitchStress').style.display = "";
        document.getElementById('btnStress1').style.display = "";
        document.getElementById('btnStress2').style.display = "";

        // Indiquer le lien de Page Vierge
        document.getElementById('hrefNewPage').href = thisRawURL+'en/';


    } else if (lang == "zh") {
        initInterface();

        document.getElementById('ti_maj').style.display = "none";
        
        // Indiquer le lien de Page Vierge
        document.getElementById('hrefNewPage').href = thisRawURL+'zh/';


    } else if (lang == "dz") {
        initInterface();

        document.getElementById('barreTitre').classList.remove('me-auto');
        document.getElementById('barreTitre').classList.add('ms-auto');

        document.getElementById('ti_maj').style.display = "none";
        
        // Indiquer le lien de Page Vierge
        document.getElementById('hrefNewPage').href = thisRawURL+'dz/';

        document.getElementById('textZone').style.direction = "rtl";
        document.getElementById('textZoneBack').classList.remove('justify-content-between')
        document.getElementById('textZoneBack').classList.add('justify-content-end')

        document.documentElement.style.setProperty('--startPointLtr', 'hidden');
        document.documentElement.style.setProperty('--startPointRtl', 'solid');

        document.getElementById("msg_dz").style.display = "block";

        var punctAr1s = document.querySelectorAll('.punctAr1');
        for (el of punctAr1s) el.style.display = "block";

        var punctAr0s = document.querySelectorAll('.punctAr0');
        for (el of punctAr0s) el.style.display = "none";

    } else if (lang == "shy") {
        initInterface();

        document.getElementById('ti_maj').style.display = "none";
        
        // Indiquer le lien de Page Vierge
        document.getElementById('hrefNewPage').href = thisRawURL+'shy/';
    
    } else if (lang == "de") {
        initInterface();

        document.getElementById('btnSwitchStress').style.display = "";
        document.getElementById('btnStress1').style.display = "";
        
        // Indiquer le lien de Page Vierge
        document.getElementById('hrefNewPage').href = thisRawURL+'de/';

    } else { // "fr" par défaut
        initInterface();
        
        // Indiquer le lien de Page Vierge
        document.getElementById('hrefNewPage').href = thisRawURL+'fr/';
    }
}