var appdata = [];
var stats = {};
var filter= {app:[],mod:[],pay:[],lan:[],use:[],age:[],dev:[],nav:[]}
var initfilter = {app:[],mod:[],pay:[],lan:[],use:[],age:[],dev:[],nav:[]}

getStats()
getSynthVocTraces()
getSynthVocRecords()

async function getStats() {
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    
    // ENVOI
	const response = await fetch('/_getAppStats/', options);
    appdata = await response.json();
    for (let i=0; i<appdata.appdata.length; i++) {
        appdata.appdata[i].country = code2country[appdata.appdata[i].country];
    }
    console.log(appdata);

    document.getElementById('nbTracesTotal').innerText = appdata.appdata.length;

    for (var {date:d, app:a, module:m, ip:i, agent:ag, country:c, lang:l} of appdata.appdata) {
        if (!filter.app.includes(a)) {
            filter.app.push(a);
            initfilter.app.push(a);
        } 
        if (!filter.mod.includes(m)) {
            filter.mod.push(m);
            initfilter.mod.push(m);
        } 
        if (!filter.pay.includes(c)) {
            filter.pay.push(c);
            initfilter.pay.push(c);
        } 
        if (!filter.lan.includes(l)) {
            filter.lan.push(l);
            initfilter.lan.push(l);
        } 
        if (!filter.use.includes(i)) {
            filter.use.push(i);
            initfilter.use.push(i);
        } 
        // if (!filter.age.includes(ag)) {
        //     filter.age.push(ag);
        //     initfilter.age.push(ag);
        // } 
        var [d,s,n] = agentParser(ag);
        if (!filter.dev.includes(d)) {
            filter.dev.push(d);
            initfilter.dev.push(d);
        } 
        if (!filter.nav.includes(n)) {
            filter.nav.push(n);
            initfilter.nav.push(n);
        } 
    }
    makeCharts(filter)

}

function agentParser(ag){
    var dsn = ag.split(' / ');
    if(dsn.length==3) {
        var [d,s,n] = dsn;
        n = n.replace(/\d|\./g,'').trim();
        return [d,s,n]
    } else {
        console.log("Agent imparsable :",ag)
        return [ag,"",""]
    }
}

function select(param) {
    if ( initfilter.app.includes(param) ) {
        if ( filter.app.length == 1 ) filter.app = initfilter.app;
        else {
            filter.app = [param]
        }
    } else if ( initfilter.mod.includes(param) ) {
        if ( filter.mod.length == 1 ) filter.mod = initfilter.mod;
        else {
            filter.mod = [param]
        }
    } else if ( initfilter.pay.includes(param) ) {
        if ( filter.pay.length == 1 ) filter.pay = initfilter.pay;
        else {
            filter.pay = [param]
        }
    } else if ( initfilter.lan.includes(param) ) {
        if ( filter.lan.length == 1 ) filter.lan = initfilter.lan;
        else {
            filter.lan = [param]
        }
    } else if ( initfilter.use.includes(param) ) {
        if ( filter.use.length == 1 ) filter.use = initfilter.use;
        else {
            filter.use = [param]
        }
    // } else if ( initfilter.age.includes(param) ) {
    //     if ( filter.age.length == 1 ) filter.age = initfilter.age;
    //     else {
    //         filter.age = [param]
    //     }
    } else if ( initfilter.dev.includes(param) ) {
        if ( filter.dev.length == 1 ) filter.dev = initfilter.dev;
        else {
            filter.dev = [param]
        }
    } else if ( initfilter.nav.includes(param) ) {
        if ( filter.nav.length == 1 ) filter.nav = initfilter.nav;
        else {
            filter.nav = [param]
        }
    }
    updateCharts(filter);
}

var chartApps;
var chartModules;
var chartPays;
var chartLangues;
var chartUsers;
var chartAgents;
var chartMachines;
var chartNavs;
var chartTime;

function makeCharts(filter) {
    stats = summarize(filter, appdata.appdata, appdata.day2traces);

    chartApps = makeChart('chartApps', stats.app2freq);
    chartModules = makeChart('chartModules', stats.mod2freq);
    chartPays = makeChart('chartPays', stats.pay2freq);
    chartLangues = makeChart('chartLangues', stats.lan2freq);
    chartUsers = makeChart('chartUsers', stats.use2freq, false);
    // chartAgents = makeChart('chartAgents', stats.age2freq, false);
    chartMachines = makeChart('chartMachines', stats.dev2freq, false);
    chartNavs = makeChart('chartNavs', stats.nav2freq);

    chartTime = makeTimeChart('chartTime', stats.day2traces);
}

function updateCharts(filter) {
    for (x in appdata.day2traces) appdata.day2traces[x] = 0;
    stats = summarize(filter, appdata.appdata, appdata.day2traces);

    upd(chartApps, stats.app2freq);
    upd(chartModules, stats.mod2freq);
    upd(chartPays, stats.pay2freq);
    upd(chartLangues, stats.lan2freq);
    upd(chartUsers, stats.use2freq);
    // upd(chartAgents, stats.age2freq);
    upd(chartMachines, stats.dev2freq);
    upd(chartNavs, stats.nav2freq);   
    upd(chartTime, stats.day2traces);
}

function upd(chart, s) {
    chart.data.labels = Object.keys(s);
    chart.data.datasets[0].data = Object.values(s);
    chart.update();
}

function summarize(filter, data, day2traces){
    var app2freq = {};
    var mod2freq = {};
    var pay2freq = {};
    var lan2freq = {};
    var use2freq = {};
    var age2freq = {};
    var dev2freq = {};
    var nav2freq = {};
    var cpt = 0;

    for (var {date:d, app:a, module:m, ip:i, agent:ag, country:c, lang:l} of data) {
        var [de,s,n] = agentParser(ag);
        if (filter.app.includes(a) && filter.mod.includes(m) && filter.pay.includes(c) && filter.lan.includes(l) && filter.use.includes(i) && filter.dev.includes(de) && filter.nav.includes(n)) {
            if (!Object.keys(app2freq).includes(a)) app2freq[a]=0;
            app2freq[a]++;
    
            if (!Object.keys(mod2freq).includes(m)) mod2freq[m]=0;
            mod2freq[m]++;
    
            if (!Object.keys(pay2freq).includes(c)) pay2freq[c]=0;
            pay2freq[c]++;
    
            if (!Object.keys(lan2freq).includes(l)) lan2freq[l]=0;
            lan2freq[l]++;
    
            if (!Object.keys(use2freq).includes(i)) use2freq[i]=0;
            use2freq[i]++;
    
            // if (!Object.keys(age2freq).includes(ag)) age2freq[ag]=0;
            // age2freq[ag]++;

            if (!Object.keys(dev2freq).includes(de)) dev2freq[de]=0;
            dev2freq[de]++;

            if (!Object.keys(nav2freq).includes(n)) nav2freq[n]=0;
            nav2freq[n]++;
    
            var df = d.slice(0,10);
            if (Object.keys(day2traces).includes(df)) day2traces[df]++;
            
            cpt++
        }
    }
    document.getElementById('nbTraces').innerText = cpt;
    document.getElementById('nbUsers').innerText = Object.keys(use2freq).length;
    return {
        'app2freq':app2freq,
        'mod2freq':mod2freq,
        'pay2freq':pay2freq,
        'lan2freq':lan2freq,
        'use2freq':use2freq,
        // 'age2freq':age2freq,
        'dev2freq':dev2freq,
        'nav2freq':nav2freq,
        'day2traces':day2traces
    }
}


function makeChart(canvasName, stats, legend=true) {
    var data = {
        labels: Object.keys(stats),
        datasets: [{
        label: canvasName,
        data: Object.values(stats),
        backgroundColor: COLORS,
        hoverOffset: 4
        }]
    };
    
    if (["svappChart","svapplenssmlChart","svuserChart","svuserlenssmlChart", "svlangChart"].includes(canvasName)) {
        var options = {
            plugins: {
                legend: {
                    display: legend,
                    position: 'bottom'
                }
            }
        }
    } else {
        var options = {
            plugins: {
                legend: {
                    display: legend,
                    position: 'bottom'
                }
            },
            onClick: (e, activeEls) => {
                let datasetIndex = activeEls[0].datasetIndex;
                let dataIndex = activeEls[0].index;
                let datasetLabel = e.chart.data.datasets[datasetIndex].label;
                let value = e.chart.data.datasets[datasetIndex].data[dataIndex];
                let label = e.chart.data.labels[dataIndex];
                console.log("In click", datasetLabel, label, value);
                select(label);
            }
        }
    }

    const config = {
        type: 'doughnut',
        data: data,
        options: options
    };
    
    const myChart = new Chart(
        document.getElementById(canvasName),
        config
      );

    return myChart
}


// DISPLAY TIME CHART
function makeTimeChart(canvasName, day2traces) {
    const labels = Object.keys(day2traces);
    
      const data = {
        labels: labels,
        datasets: [{
          label: CHARTS[canvasName],
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgb(255, 99, 132)',
          data: Object.values(day2traces),
        }]
      };
    
      const config = {
        type: 'line',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                  type: 'linear',
                  display: true,
                  position: 'left',
                },
              }
          }
      };

      const myChart = new Chart(
        document.getElementById(canvasName),
        config
      );

    return myChart
}

async function getSynthVocTraces() {
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    
    // ENVOI
	const response = await fetch('/_getSynthVocTracesJson/', options);
    var data = await response.json();
    console.log(data);

    document.getElementById('nbTracesSynthVoc').innerText = data.nbTraces;
    document.getElementById('lenssmlThisMonth').innerText = data.lenssmlThisMonth;

    svappChart = makeChart('svappChart', data.app2nbTraces, true);
    svapplenssmlChart = makeChart('svapplenssmlChart', data.app2Lenssml, true);
    svuserChart = makeChart('svuserChart', data.user2nbTraces, false);
    svuserlenssmlChart = makeChart('svuserlenssmlChart', data.user2Lenssml, false);

    var day2nbTraces = {}
    var day2lenssml = {}
    Object.entries(data.day2nbTracesLenssml).forEach((k) => {
        day2nbTraces[k[0]] = k[1][0]
        day2lenssml[k[0]] = k[1][1]
    })
    console.log(day2nbTraces)
    console.log(day2lenssml)
    svDaychart = makeTimeChart('svDaychart', day2nbTraces);
    svDaychart.data.datasets.push({
            label: "nb caractères ssml",
            backgroundColor: 'green',
            borderColor: 'green',
            data: Object.values(day2lenssml),
            yAxisID: 'y1',
    })
    svDaychart.options.scales['y1'] = {
        type: 'linear',
        display: true,
        position: 'right',
        // grid line settings
        grid: {
          drawOnChartArea: false, // only want the grid lines for one axis to show up
        },
      }
    svDaychart.update()
}

async function getSynthVocRecords() {
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    
    // ENVOI
	const response = await fetch('/_getSynthVocRecords/', options);
    var data = await response.json();
    console.log(data);

    document.getElementById('nbFreqSynthVoc').innerText = data.nbFreq;

    svlangChart = makeChart('svlangChart', data.lang2freq, true);

    var itemsfr = Object.keys(data.ipa2freq['fr']).map(function(key) { return [key, data.ipa2freq['fr'][key]]; });
    itemsfr.sort(function(first, second) { return second[1] - first[1]; });

    var itemsen = Object.keys(data.ipa2freq['en']).map(function(key) { return [key, data.ipa2freq['en'][key]]; });
    itemsen.sort(function(first, second) { return second[1] - first[1]; });

    document.getElementById('topfr').innerHTML = ""
    itemsfr.slice(0,20).forEach((el)=> {
        document.getElementById('topfr').innerHTML += el[0]+' : '+el[1]+'<br>'
    })

    document.getElementById('topen').innerHTML = ""
    itemsen.slice(0,20).forEach((el)=> {
        document.getElementById('topen').innerHTML += el[0]+' : '+el[1]+'<br>'
    })
    
}


let svappChart;
let svapplenssmlChart;
let svuserChart;
let svuserlenssmlChart;
let svDaychart;

let svlangChart;



//////////////////////////////////

const COLORS = [
    '#4dc9f6',
    '#f67019',
    '#f53794',
    '#537bc4',
    '#acc236',
    '#166a8f',
    '#00a950',
    '#58595b',
    '#8549ba'
  ];

const CHARTS = {
    'chartTime': 'Nombre de traces',
    'svDaychart': 'Nombre de requêtes Polly',
}