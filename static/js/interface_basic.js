selectLang("fr");
function selectLang(lang){
    console.log('SelectLang()',lang);
    if (lang=="fr"){
        interface("fr");
    } else if (lang=="en"){
        interface("en");
    }
}

interface("fr");
function interface(lang) {
    console.log("Langue d'interface:",lang);
    thisPageLang = lang;
    var langspanList = document.getElementsByClassName("langspan");
    if (lang == "en") {
        for (i=0; i<langspanList.length; i++) {
            span = langspanList[i];
            span.innerHTML = langJson[span.id]["en"];
        }

    } else { // "fr" par défaut
        for (i=0; i<langspanList.length; i++) {
            span = langspanList[i];
            span.innerHTML = langJson[span.id]["fr"];
        }
    }
}