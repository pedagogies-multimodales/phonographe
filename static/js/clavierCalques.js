////////////////// AFFICHAGE DES CALQUES /////////////////
var rien = document.getElementById('btn-rien');
var vsm = document.getElementById('btn-vsm');
var bch = document.getElementById('btn-bch');
var cpsg = document.getElementById('btn-cpsg');
var pngPochoir = document.getElementById('pngPochoir');
var pngCalq = document.getElementById('pngCalq');


setKeyboards('black');

function setKeyboards(bgColor) {
    if (bgColor == 'black') {
        // Clavier par défaut :
        pngPochoir.src="/static/png/01.png";
        pngCalq.src="/static/png/04.png";
        rien.addEventListener('click', function(){pngCalq.src='';btnFocus(rien);famille="formes";saveTrace(`shiftKeyboard ${famille}`); showPanneau()});
        vsm.addEventListener('click', function(){pngCalq.src="/static/png/03.png";btnFocus(vsm);famille="formes";saveTrace(`shiftKeyboard ${famille}`); showPanneau()});
        bch.addEventListener('click', function(){pngCalq.src="/static/png/04.png";btnFocus(bch);famille="bouches";saveTrace(`shiftKeyboard ${famille}`); showPanneau()});
        cpsg.addEventListener('click', function(){pngCalq.src="/static/png/06.png";btnFocus(cpsg);famille="coupes";saveTrace(`shiftKeyboard ${famille}`); showPanneau()});
    } else if (bgColor == 'white') {
        // Clavier par défaut :
        pngPochoir.src="/static/png/02.png";
        pngCalq.src="/static/png/05.png";
        rien.addEventListener('click', function(){pngCalq.src='';btnFocus(rien);famille="formes"});
        vsm.addEventListener('click', function(){pngCalq.src="/static/png/03.png";btnFocus(vsm);famille="formes";saveTrace(`shiftKeyboard ${famille}`); showPanneau()});
        bch.addEventListener('click', function(){pngCalq.src="/static/png/05.png";btnFocus(bch);famille="bouches";saveTrace(`shiftKeyboard ${famille}`); showPanneau()});
        cpsg.addEventListener('click', function(){pngCalq.src="/static/png/06.png";btnFocus(cpsg);famille="formes";saveTrace(`shiftKeyboard ${famille}`); showPanneau()});
    };
};

function btnFocus(btn){
    if(rien.classList.contains('btnfocus')){
        rien.classList.toggle('btnfocus');
    };
    if(vsm.classList.contains('btnfocus')){
        vsm.classList.toggle('btnfocus');
    };
    if(bch.classList.contains('btnfocus')){
        bch.classList.toggle('btnfocus');
    };
    if(cpsg.classList.contains('btnfocus')){
        cpsg.classList.toggle('btnfocus');
    };
    btn.classList.toggle('btnfocus');
};
//////////////////////////////////////////////////////////

// Modifier l'encrage et la taille du clavier
function rapetisserPanneau() {
        var clavSize = getComputedStyle(document.documentElement).getPropertyValue('--clavSize');
        var graphSize = getComputedStyle(document.documentElement).getPropertyValue('--graphSize');
        document.documentElement.style.setProperty('--clavSize', parseInt(clavSize.slice(0,2)) - 10 + '%');
        document.documentElement.style.setProperty('--graphSize', parseInt(graphSize.slice(0,2)) + 10 + '%');
}

function agrandirPanneau() {
        var clavSize = getComputedStyle(document.documentElement).getPropertyValue('--clavSize');
        var graphSize = getComputedStyle(document.documentElement).getPropertyValue('--graphSize');
        document.documentElement.style.setProperty('--clavSize', parseInt(clavSize.slice(0,2)) + 10 + '%');
        document.documentElement.style.setProperty('--graphSize', parseInt(graphSize.slice(0,2)) - 10 + '%');
}

function showPanneau() {
    document.getElementById('doCalques').style = ''
    document.getElementById('cachePanneau') ? document.getElementById('cachePanneau').style.display = 'none' : "do nothing"
}