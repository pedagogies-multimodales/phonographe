
async function addStat(modul, lang=thisPageLang) {

	// ON EMBALLE TOUT ÇA
    var colis = {
		app: "phonographe",
        module: modul,
		lang: lang
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	await fetch('/addStat/', options);
}