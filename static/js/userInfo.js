$(document).ready( function () {
    // Formattage du tableau
    // https://datatables.net/manual/options
    $('#tracesTable').DataTable();

    // Date picker
    // https://jqueryui.com/datepicker/
    let date_options = {
        firstDay: 1, // premier jour semaine = lundi
        dateFormat: "dd/mm/yy",
        minDate: new Date(2021, 0, 1),
        maxDate: "+0w",
        currentText: "Aujourd'hui"
    }
    $( "#datepicker1" ).datepicker(date_options);
    $( "#datepicker1" ).datepicker("setDate", new Date(2021, 0, 1))
    $( "#datepicker2" ).datepicker(date_options);
    $( "#datepicker2" ).datepicker("setDate", new Date())
} );


async function downloadTraces(){
    let date1 = document.getElementById('datepicker1').value
    let date2 = document.getElementById('datepicker2').value

    // ON EMBALLE TOUT ÇA
    var colis = {
        username,
        date1,
        date2
    };

    let filename = `traces_${username}_${date1.replace(/\//g,'-')}_${date2.replace(/\//g,'-')}.csv`
    
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
    const response = await fetch('/downloadTraces/', options)
    let data = await response.text()

    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(data);
    hiddenElement.target = '_blank';
    
    //provide the name for the CSV file to be downloaded
    hiddenElement.download = filename
    hiddenElement.click();   
}


// DISPLAY LOG CHART
let listvalues = []
for (key in day2traces) {
    listvalues.push(parseInt(day2traces[key]))
}
var ctx = document.getElementById('logChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: Object.keys(day2traces),
        datasets: [{
            label: 'Nombre de traces par jour',
            data: listvalues,
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            borderColor: 'rgba(75, 192, 192, 1)',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
