interface("en");

function interface(lang) {
    console.log("Langue d'interface:",lang);
    thisPageLang = lang;
    var langspanList = document.getElementsByClassName("langspan");
    if (lang == "en") {
        for (i=0; i<langspanList.length; i++) {
            span = langspanList[i];
            span.innerHTML = langJson[span.id]["en"];
        }

    } else { // "fr" par défaut
        for (i=0; i<langspanList.length; i++) {
            span = langspanList[i];
            span.innerHTML = langJson[span.id]["fr"];
        }
        
    }
}




////////// CHARGEMENT DE L'ACTIVITÉ /////////////
//////////////////////////////////////////////
var nbmots;
var cptitem = 0;
var mots;
var currentAudio = "";

var rep = document.getElementById('rep');
var btnValider = document.getElementById("btnValider");

getActivity();
async function getActivity() {
    // PRÉPARATION DU COLIS
    var colis = {};
    //console.log(colis);

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // REQUÊTAGE SERVEUR
    const response = await fetch('/getActivitySwitchStress/', options)
    const data = await response.json();
    console.log(data);
    
    // LANCEMENT DE L'ACTIVITÉ
    nbmots = data["mots"].length;
    mots = data["mots"];

    document.getElementById('nomSerie').innerHTML = data["nom"];
    document.getElementById('nbMots').innerHTML = nbmots;

    document.getElementById('fenetreDeLancement').style.display = "block";
}

// Démarrage de la série
function demarrer() {
    document.getElementById('fenetreDeLancement').style.display = "none";
    document.getElementById('divConsigne').style.display = "block";
    document.getElementById('divReponse').style.display = "block";
    cptitem = 0;
    loadNext()
}

// charge le mot suivant
function loadNext() {
    document.getElementById("bravo").style.display = "none";
    if (cptitem < nbmots) {
        var w = "";
        for (i=0; i<mots[cptitem]["phonoPossibles"][0].length; i++) {
            var id = "id"+i;
            var phon = mots[cptitem]["phonoPossibles"][0][i][0].match(/.*(phon_\w+).*/)[1];
            w = w + "<span id='"+id+"' onclick='switchstress(this.id)' class='switchable " + phon +"'>" + mots[cptitem]["phonoPossibles"][0][i][1] + "</span>";
        }
        console.log("Mot à afficher:",w);
        rep.innerHTML = w;

        currentAudio = '/static/audio/switchstress/s0/'+mots[cptitem]["audio"];
        playAudio();
        cptitem = cptitem+1;
        document.getElementById('cptitem').innerHTML = cptitem;
    } else {
        rep.innerHTML = "<h1>Félicitations !</h1>";
        playEffect("success");
    }
    
}
    
// vérifie la réponse
function checkAnswer() {
    var phono = mots[cptitem-1]["phonoPossibles"][0]; // cptitem-1 parce qu'on a déjà incrémenté cptitem lors de loadNext()
    var erreur = false;

    console.log("PHONO:",phono);

    if (erreur) {
        playEffect("wrong");
    } else {
        playEffect("correct");
        document.getElementById("bravo").style.display = "block";
    }
}

// SOUNDS
function playAudio() {
    var audio = new Audio(currentAudio);
    audio.play();
}

function playEffect(effect) {
    var audio;
    if (effect == "select") audio = new Audio("/static/audio/effets/select.mp3");
    else if (effect == "delete") audio = new Audio("/static/audio/effets/del.mp3");
    else if (effect == "success") audio = new Audio("/static/audio/effets/success.mp3");
    else if (effect == "correct") audio = new Audio("/static/audio/effets/correctAnswer.mp3");
    else if (effect == "wrong") audio = new Audio("/static/audio/effets/wrongAnswer.mp3");
    audio.play();
}

////////

function switchstress(identifiant) {;
    if (document.getElementById(identifiant).classList.contains('stress1')) { // passe à stress2
        document.getElementById(identifiant).classList.remove('stress1');
        document.getElementById(identifiant).classList.add('stress2');
    } else if (document.getElementById(identifiant).classList.contains('stress2')) { // passe à normal (pas de stress)
        document.getElementById(identifiant).classList.remove('stress2');
    } else {
        document.getElementById(identifiant).classList.add('stress1'); // passe à stress1
    }
}