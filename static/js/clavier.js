// Exécuter interface.js avant.

// VARIABLES UTILISATEUR
var fidel = new Object(); // Fidel courant
var currentPhon = null; // Phonème sélectionné
const thisAppli = "phonographe"; // utilisé pour la synthèse vocale

/////////////////////////////////////////////////
//////////// RÉCUPÉRATION DE LA PAGE ////////////
////////////   SI PAGE ENREGISTRÉE   ////////////
/////////////////////////////////////////////////
// Contenur par défaut de la div textZone
var defg0 = '<span id="g0" class="startPoint" onclick="putAnchor(this.id)">&nbsp;</span>'
var defg0inner = '&nbsp;'
var defgEnd = '<span id="gEnd" onclick="putAnchorOnLast()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'
var defgEndinner = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
var textZoneDefault = defg0+defgEnd
document.getElementById('textZone').innerHTML = textZoneDefault

// Position curseur par défaut
var curseurPos = 'g0'

var pageId = '0'

if (dataPage == 0) {
    // Chargement d'une page vierge
    console.log("Chargement d'une page vierge.")
    var thisRawURL = thisURL.replace('/fr','').replace('/en','').replace('/zh','').replace('/dz','').replace('/shy','').replace('/de','');
    setLangFromUrl()
} else {
    console.log("Chargement de la page",dataPage.pageId)
    pageId = dataPage.pageId
    dateCreation = dataPage.dateCreation
    var thisRawURL = thisURL.replace('/'+pageId,'')
    
    document.getElementById('textZone').innerHTML = dataPage.textZoneContent

    if (dataPage.textZoneContent.match(/.*startPoint/)) {
        curseurPos = document.getElementsByClassName('startPoint')[0].id
    } else {
        curseurPos = 'g0'
    }

    if (dataPage.lang) {
        console.log("Langue enregistrée dans cette page :",dataPage.lang, dataPage.panneau, dataPage.fidel)
        selectLang(dataPage.lang, dataPage.panneau, dataPage.fidel)
    } else {
        console.log("Langue non enregistrée ; default=fr")
        selectLang("fr")
    }

    var cartesListe = Array.from(document.getElementsByClassName('carte'))
    cartesListe.forEach( (carte) => {
        carte.addEventListener("dblclick", function(e) {
            var tgt = e.target
            // on passe le curseurb sur la carte d'avant
            curseurPos = tgt.previousElementSibling.id
            tgt.previousElementSibling.classList.add('startPoint')
            playEffect("delete")
            tgt.remove()
            // saveTrace('del phon '+rectId)
        })

        carte.addEventListener('dragstart', () => {
            carte.classList.add('dragging')
            console.log('DRAGGING ', carte.id)
        })

        carte.addEventListener('dragend', () => {
            carte.classList.remove('dragging')
            console.log('STOP DRAGGING ', carte.id)
        })
    })
}


// GESTION DE LA CASSE
var maj = false; // par défaut minuscule
function toggleCasse(){
    maj = !maj
    document.getElementsByClassName('btnCasse')[0].classList.toggle('btnCasseSelected')
    recupPhon(currentPhon)
}

// PROSODIE 
// accent primaire
var stress1 = false
function toggleStress1(){
    if (stress2) toggleStress2()
    stress1 = !stress1
    console.log("stress1",stress1)
    document.getElementById('btnStress1').classList.toggle('btnStressSelected')
}
// accent secondaire
var stress2 = false
function toggleStress2(){
    if (stress1) toggleStress1()
    stress2 = !stress2
    console.log("stress2",stress2)
    document.getElementById('btnStress2').classList.toggle('btnStressSelected')
}


///////////////////////////////////////////////////
/////////////  Définition RecupPhon() /////////////
///////////////////////////////////////////////////
// Action lors du clique sur phonème du panneau phono
// (soit écrire le phonème, soit lister les graphies)
var modeGraphies = 1 // 0 : phono ; 1 : phonographie

function switchMode(){
    modeGraphies = !modeGraphies
    if (!modeGraphies) {
        // Mode PHONO
        console.log('Passage en mode PHONO')
        document.getElementById('modePhono').checked = false;
        modeGraphies = 0

        if (thisPageLang != "shy") {
            document.getElementById('graphies').style.display = 'none'
            document.getElementById('clavier').classList.remove('clavier')
            document.getElementById('clavier').classList.add('clavierSeul')

            document.documentElement.style.setProperty('--clavSize', '60%');
            document.documentElement.style.setProperty('--graphSize', '40%');

            hideGraphies();
        } else {
            // SI CHAOUI
            selectPanneau('phonoShypSb4');
        }

    } else {
        // Mode PHONOGRAPHIE
        console.log('Passage en mode PHONOGRAPHIE')
        document.getElementById('modePhono').checked = true;
        modeGraphies = 1

        if (thisPageLang != "shy") {
            document.getElementById('graphies').style.display = 'block'
            document.getElementById('clavier').classList.remove('clavierSeul')
            document.getElementById('clavier').classList.add('clavier')

            document.documentElement.style.setProperty('--clavSize', '50%');
            document.documentElement.style.setProperty('--graphSize', '50%');

            showGraphies();
        } else {
            // SI CHAOUI
            selectPanneau('phonoShyfSb4');
        }
    }
}

let hamzah = [
    "ِ",
    "َ",
    "ُ",
    "ّ",
    "ٔ"
]

var diphtonguesCorrespondance = {
    "rect_i_majschwa":"rect_i_majarobase",
    "rect_eschwi":"rect_ei_maj",
    "rect_eschwa":"rect_earobase",
    "rect_aschwi":"rect_ai_maj",
    "rect_aschwu":"rect_au_maj",
    "rect_o_majschwi":"rect_o_maji_maj",
    "rect_arobaseschwu":"rect_arobaseu_maj",
    "rect_u_majschwa":"rect_u_majarobase"
  }

function recupPhon(identifiant){
    console.log("IDENTI",identifiant)
    // Debug temporaire anglais diphtongues (les identifiants phonèmes du SVG ne correspondent pas aux noms des phonèmes ALeM)
    if (thisPageLang=="en") {
        if ([
            "rect_i_majschwa",
            "rect_eschwi",
            "rect_eschwa",
            "rect_aschwi",
            "rect_aschwu",
            "rect_o_majschwi",
            "rect_arobaseschwu",
            "rect_u_majschwa"
          ].includes(identifiant)) {
            identifiant = diphtonguesCorrespondance[identifiant]
          }
    }

    if (!modeGraphies) {
        //////////////////////////////
        //// IF NOT modeGraphies
        //// → écrire directement le phonème (image)
        document.getElementById('checkWordValid').style.display = "none";

        var rectId = identifiant
        var boucheId = rectId.replace("rect","bouche")
        var phonId = rectId.replace("rect", "phon")

        var newCarte = document.createElement('div')
        // newCarte.classList = "carte text " + phonId + " noTextClip";
        newCarte.draggable = true
        // if (identifiant != "space") newCarte.style.backgroundImage = "url('/static/cartes/"+ famille + "/" + phonId + ".jpg')"
        // if (identifiant != "space") newCarte.style.background = "url('/static/bouches/"+ boucheId + ".jpg'), linear-gradient(to bottom,  #7abcff 0%,#4096ee 100%);"
        
        
        newCarte.classList = "carte text " + phonId + " noTextClip";// on garde rectId, pour connaître le phonème mais sans appeler les styles de phonochromie-alem.css
        
        if (!identifiant.match(/space/)) { 
            var newCarteIn = document.createElement('div')
            newCarteIn.classList = "carteIn"
            newCarteIn.style.backgroundImage = `url('../../../media/phonocartes/do_${famille}/${phonId}.png')`; //"url('/static/cartes/"+ famille +"/"+ phonId + ".jpg')"}
            newCarte.appendChild(newCarteIn)
        } 
        
        newCarte.addEventListener("dblclick", function(e) {
            var tgt = e.target
            // on passe le curseurb sur la carte d'avant
            curseurPos = tgt.previousElementSibling.id
            tgt.previousElementSibling.classList.add('startPoint')
            playEffect("delete")
            tgt.remove()
            // saveTrace('del phon '+rectId)
        })

        newCarte.addEventListener('dragstart', () => {
            newCarte.classList.add('dragging')
            console.log('DRAGGING ', rectId)
        })

        newCarte.addEventListener('dragend', () => {
            newCarte.classList.remove('dragging')
            console.log('STOP DRAGGING ', rectId)
        })

        newCarte.setAttribute("id", phonId+"-g"+Math.random().toString(36).substring(2,9))
        newCarte.setAttribute('onclick', "putAnchor(this.id)")
        
        insertAfter(document.getElementById(curseurPos), newCarte)
        document.getElementById(curseurPos).classList.remove('startPoint')
        curseurPos = newCarte.id
        document.getElementById(curseurPos).classList.add('startPoint')
        
        playEffect("select")
        // saveTrace('add phon '+rectId)
    
    } else {
        ////////////////////////////////
        //// IF modeGraphies
        //// → lister les graphies possibles dans la partie droite de l'écran

        currentPhon = identifiant;

        if (thisPageLang != "dz" && thisPageLang != "shy" && thisPageLang != "de") {
            // Pour toutes les langues sauf l'arabe, le chaoui et l'allemand (cas à part traités plus bas)
            // On crée un tableau dans la zone des graphies à partir du fichier fidel envoyé par le serveur
    
            var graphZone = document.getElementById("graphiesZone")
            var gz = ''
            
            console.log(id2class[identifiant])
            var fid = fidel[id2class[identifiant]]
            //console.log(fid)
            if (bgWhite) gz += "<table class='graphiesZoneTable graphiesZoneTable-white"; else gz += "<table class='graphiesZoneTable"
            gz += "' id='graphiesZoneTable'>"
            for (let i in fid){
                gz += "<tr>"
                for (let j in fid[i]){
                    //console.log(fid[i][j])
                    gz += "<td>"
                    if (fid[i][j][2] == 1) {
                        var graphie = fid[i][j][0]
                        if (maj) graphie = graphie[0].toUpperCase()+graphie.slice(1)
                        if (fid[i][j].length == 4) phonId = fid[i][j][3]; else phonId = id2class[identifiant]; // Traitement des mélanges de phonèmes dans une même zone de graphies
                        gz += '<div onClick="writeGraph(\''+graphie+'\',\''+phonId+'\');" class="graph graph'+fid[i][j][1]+' '+phonId+'"> '+graphie+' </div>'
                    }
                    gz += "</td>"
                }
                gz += "</tr>"
            }
            gz += "</table>"

            graphZone.innerHTML = gz

        } else if (thisPageLang == "dz") {
            // Cas de l'ARABE ALGÉRIEN - traitement plus simple

            var graphZone = document.getElementById("graphiesZone")
            graphlist = api2ar[phon2api[identifiant.replace('rect_', 'phon_')]]
            // console.log(identifiant, graphlist)
            graphZone.innerHTML = "";

            var hanDiv = document.createElement("div");
            hanDiv.id = "hanDiv";
            hanDiv.style.width = "100%";
            hanDiv.style.height = "100%";
            hanDiv.classList = "d-flex justify-content-evenly align-content-around flex-wrap";
            document.getElementById('graphiesZone').innerHTML = "";
            document.getElementById('graphiesZone').appendChild(hanDiv);

            document.getElementById('graphiesZone').appendChild(hanDiv);
            
            for (g in graphlist) {
                newg = document.createElement('div')
                newg.innerHTML = graphlist[g]
                newg.classList.add('graph')
                newg.classList.add('graph6')
                newg.classList.add(identifiant.replace('rect_', 'phon_'))
                newg.classList.add('noUnderLine')

                if (hamzah.includes(graphlist[g])) {
                    newg.classList.add('noTextClipGraphZone')
                    newg.innerHTML = "◌"+newg.innerHTML;
                    newg.setAttribute('onClick', "writeGraph('"+graphlist[g]+"','"+identifiant.replace('rect_', 'phon_')+" noTextClip noUnderLine')")
                } else {
                    newg.setAttribute('onClick', "writeGraph('"+graphlist[g]+"','"+identifiant.replace('rect_', 'phon_')+" noUnderLine')")
                }

                hanDiv.appendChild(newg)
            }
        } else if (thisPageLang == "de") {
            // Allemand = fonctionnement inspiré de l'ARABE ALGÉRIEN
            var graphZone = document.getElementById("graphiesZone")
            graphlist = api2de[phon2api[identifiant.replace('rect_', 'phon_')]]
            console.log(identifiant, graphlist)
            graphZone.innerHTML = "";

            var hanDiv = document.createElement("div");
            hanDiv.id = "hanDiv";
            hanDiv.style.width = "100%";
            hanDiv.style.height = "100%";
            hanDiv.classList = "d-flex justify-content-evenly align-content-around flex-wrap";
            document.getElementById('graphiesZone').innerHTML = "";
            document.getElementById('graphiesZone').appendChild(hanDiv);

            document.getElementById('graphiesZone').appendChild(hanDiv);
            
            for (g in graphlist) {
                newg = document.createElement('div')
                var graphie = graphlist[g];
                if (maj) graphie = graphie[0].toUpperCase()+graphie.slice(1);
                newg.innerHTML = graphie;
                newg.classList.add('graph')
                newg.classList.add('graph6')
                newg.classList.add(identifiant.replace('rect_', 'phon_'))

                newg.setAttribute('onClick', "writeGraph('"+graphie+"','"+identifiant.replace('rect_', 'phon_')+"')")
                
                hanDiv.appendChild(newg)
            }
        }
        showGraphies()
    }
}

//fonction permettant d'écrire le choix de graphème
function writeGraph(graph,phon){
    document.getElementById('checkWordValid').style.display = "none";

    if (maj) {
        toggleCasse(); // retour minuscules automatique
        recupPhon(currentPhon)
    }

    // GESTION PROSODIE
    var stress = ''; // ajout du stress si besoin
    if (["en","de"].includes(thisPageLang)) {
        if (stress1) {
            stress = " stress1"
            toggleStress1()
        }
        else if (stress2) {
            stress = " stress2"
            toggleStress2()
        }
        //if (phon in ["phon_schwa","phon_schwi","phon_schwu","phon_schwju"]) stress = " schwa"
        else if (phon == "phon_schwa" || phon == "phon_schwi" || phon == "phon_schwu" || phon == "phon_schwju" || phon == "phon_schw6") {
            if (stress1) toggleStress1
            if (stress2) toggleStress2
            stress = " schwa"
        }
        else {
            stress = " unstressed"
        }
    }
    

    // SI LIAISON INTERNE (ex. février) : noTextClip
    var textClip = "";
    if (graph == "͜") textClip = " noTextClip";

    // SI ARABE
    if (thisPageLang == "dz") {
        stress = " arabe"
        graph = graph.replace(/ـ/g, '')
        console.log(graph, phon)
        // Éviter la ligature du "laa" (pour conserver la couleur du a court)
        // if (document.getElementById(curseurPos).classList.contains("phon_a") && document.getElementById(curseurPos).previousSibling.classList.contains("phon_l") && phon=="phon_a_long noUnderLine") {
        //     console.log("La+A")
        //     graph = "&zwj;"+graph;
        // }
        // if (document.getElementById(curseurPos).classList.contains("phon_l") && phon=="phon_a_long noUnderLine") {
        //     console.log("L+AA")
        //     graph = "&zwj;"+graph;
        // } 
        
        // Forcer la ligature "le" 
        // MARCHE PAS
        if (graph=="ﺄ" && document.getElementById(curseurPos).classList.contains("phon_l")) {
            graph = "&zwj;"+graph;
        }

        // Désactiver la modification auto de caractère pour voyelles courtes après shaddah ou autre NoTextClip
        if (document.getElementById(curseurPos).classList.contains("noTextClip")) {
            console.log('NoTextClip Detected')
            graph = "&zwj;"+graph;
        }
        // if (hamzah.includes(graph)) graph = "&zwj;"+graph;
    }

    // SI CHAOUI
    if (thisPageLang == "shy") {
        stress = " chaoui";
    }

    var graphSpan = document.createElement("span")
    graphSpan.setAttribute("id", phon+"-g"+Math.random().toString(36).substring(2,9))
    graphSpan.setAttribute("class", 'text '+phon+stress+textClip)
    graphSpan.setAttribute('onclick', "putAnchor(this.id)")
    graph = graph.replace("◌","")
    graphSpan.innerHTML = graph

    insertAfter(document.getElementById(curseurPos), graphSpan)
    document.getElementById(curseurPos).classList.remove('startPoint')
    curseurPos = graphSpan.id
    document.getElementById(curseurPos).classList.add('startPoint')

    if (document.body.clientWidth < 850) hideGraphies()
}

function initGraph(){
    document.getElementById("graphiesZone").innerHTML = '' //réinitialisation de la div de graphies
    currentPhon = null
    console.log("Réinitialisation de la table des graphies")
}

var switchStress = false
function toggleSwitchStress() {
    switchStress = !switchStress
    console.log("SwitchStress",switchStress)
}

function putAnchor(identifiant) {
    if (switchStress) {
        // Si le bouton switchStress est activé : putAnchor a pour effet de switcher le stress de la graphie au lieu de positionner le curseur
        if (document.getElementById(identifiant).classList.contains('schwa')) {
            console.log("Cette graphie est un schwa et peut pas être accentuée !")
        } else if (document.getElementById(identifiant).classList.contains('stress1')) { // passe à stress2
            document.getElementById(identifiant).classList.remove('stress1')
            document.getElementById(identifiant).classList.add('stress2')
        } else if (document.getElementById(identifiant).classList.contains('stress2')) { // passe à normal (pas de stress)
            document.getElementById(identifiant).classList.remove('stress2')
        } else {
            document.getElementById(identifiant).classList.add('stress1'); // passe à stress1
        }
    } else {
        // Sinon il positionne simplement le curseur sur la graphie cliquée
        document.getElementById(curseurPos).classList.remove('startPoint')
        curseurPos = identifiant
        console.log('Curseur positionné à '+curseurPos)
        document.getElementById(identifiant).classList.add('startPoint')
    }
}

function putAnchorOnLast() {
    document.getElementById(curseurPos).classList.remove('startPoint')
    var identifiant = document.getElementById('gEnd').previousElementSibling.id
    curseurPos = identifiant
    console.log('Curseur positionné à '+curseurPos)
    document.getElementById(identifiant).classList.add('startPoint')
}

function gotoPrecedent() {
    document.getElementById(curseurPos).classList.remove('startPoint')
    var identifiant = document.getElementById(curseurPos).previousElementSibling.id
    curseurPos = identifiant
    console.log('Curseur positionné à '+curseurPos)
    document.getElementById(identifiant).classList.add('startPoint')
}

function gotoNext() {
    document.getElementById(curseurPos).classList.remove('startPoint')
    var identifiant = document.getElementById(curseurPos).nextElementSibling.id
    curseurPos = identifiant
    console.log('Curseur positionné à '+curseurPos)
    document.getElementById(identifiant).classList.add('startPoint')
}

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling)
}

function showPunct(thisEl) {
    var listpunct = Array.from(document.getElementsByClassName('btnhideIfSmall'))
    listpunct.forEach( (el) => {
        el.style.display = "block"
        el.addEventListener('click', function () {
            listpunct.forEach( (el) => { el.style.display = "none" })
            document.getElementById("punctBtn").style.display = "block"
        })
    })
    thisEl.style.display = "none"
}

function writeSpace() {
    if (modeGraphies){
        writeGraph(' ','punct')
    } else {
        recupPhon('space')
    }
}

//fonction entrer
function writeEnter(){ 
    // <span id="g0" onclick="putAnchor(this.id)">&nbsp;</span> <span id="gEnd">&nbsp;</span>
    var graphSpan = document.createElement("span")
    graphSpan.setAttribute("id", "g"+Math.random().toString(36).substring(2,9))
    graphSpan.setAttribute('onclick', "putAnchor(this.id)")
    graphSpan.setAttribute('class', "text pause")
    graphSpan.innerHTML = '&nbsp;'
    insertAfter(document.getElementById(curseurPos), graphSpan)

    // Insertion de <br/>
    insertAfter(document.getElementById(curseurPos), document.createElement("br"))
    // Insertion du span de fin de ligne (pour permettre d'ajouter une espace visible en fin de ligne, par la suite si besoin de continuer la phrase)
    var sp = document.createElement("span")
    sp.innerHTML = '&nbsp;'
    insertAfter(document.getElementById(curseurPos), sp)

    document.getElementById(curseurPos).classList.remove('startPoint')
    curseurPos = graphSpan.id
    document.getElementById(curseurPos).classList.add('startPoint')
}

//fonction effacer
function erasePreviousSpan(){
    document.getElementById('checkWordValid').style.display = "none";
    document.getElementById('audio').innerHTML = "" // suppression de la mémoire de la synthèse vocale
    var elASupprimer = document.getElementById(curseurPos)
    var previousEl = elASupprimer.previousElementSibling

    if (elASupprimer.id != 'g0') {
        console.log('suppr: '+elASupprimer.id+' | prev: '+previousEl)
        elASupprimer.remove()
        while (previousEl.id == '') {
            console.log('previousEl is null.')
            previousEl = previousEl.previousElementSibling
            console.log('prev: '+previousEl)
            previousEl.nextElementSibling.remove()
        }
        
        playEffect("delete")
        curseurPos = previousEl.id
        document.getElementById(curseurPos).classList.add('startPoint')
        
    } else {
        console.log('g0 ne peut pas être supprimé !')
    }
    
}

// fonction Tout Effacer
function eraseAll() {
    document.getElementById('checkWordValid').style.display = "none";
    var r = window.confirm(langJson['sp_confirmErase'][thisPageLang])
    if (r) {
        console.log("Reset All")
        playEffect('delete')
        curseurPos = 'g0'
        document.getElementById('textZone').innerHTML = textZoneDefault
        document.getElementById('audio').innerHTML = ""
    }
}

// EXPORT PNG (use html2canvas.js et download.js)
// var btnn = $('#div2png')
// btnn.click(function() {

//     window.scrollTo(0,0)
//     // Formatage de la div textZone
//     var zone = document.getElementById('textZone')
//     document.getElementById(curseurPos).classList.remove('startPoint')
//     if (document.getElementById('g0')) document.getElementById('g0').innerHTML = ''
//     if (document.getElementById('gEnd')) document.getElementById('gEnd').innerHTML = ''

//     // Création d'une div temporaire d'export
//     var zoneExp = document.createElement('div')
//     zoneExp.innerHTML = zone.innerHTML
//     zoneExp.id = 'zoneExp'
//     zoneExp.classList.add('textZoneExp')
//     if (bgWhite) {
//         zoneExp.classList.add('textZoneExpWhite')
//     }
    
    
//     // Conversion png
//     document.body.appendChild(zoneExp)
//     html2canvas(document.querySelector("#zoneExp")).then(canvas => {
//         var dataURL = canvas.toDataURL('image/png')
//         download(dataURL,'texte.png','image/png')
//     })
//     document.body.removeChild(zoneExp)

//     // Rétablissement formatage de textZone
//     document.getElementById(curseurPos).classList.add('startPoint')
//     if (document.getElementById('gEnd')) document.getElementById('gEnd').innerHTML = defgEndinner; else document.getElementById('textZone').innerHTML += defgEnd
//     if (document.getElementById('g0')) document.getElementById('g0').innerHTML = defg0inner; else document.getElementById('textZone').innerHTML = defg0 + document.getElementById('textZone').innerHTML
// })


//////////////// BACK GROUND WHITE ////////////////
var bgWhite = false
var phonA = getComputedStyle(document.documentElement).getPropertyValue('--phon_a')

function bg2white() {
    if (!bgWhite) {
        
        // SI ON PASSE DU NOIR AU BLANC
        document.getElementById('textZoneBack').classList.add('textZone-white')
        document.getElementById('textZoneBack').classList.add('graphContours')
        document.getElementById('graphiesZone').classList.add('graphiesZone-white')
        var tabbl = document.getElementById('graphiesZoneTable')
        if (tabbl) tabbl.classList.add('graphiesZoneTable-white')
        document.getElementById('graphiesZone').classList.add('graphContours')

        if (thisPageLang!='zh'){
            document.documentElement.style.setProperty('--phon_a', '#000000')
        }
        //setKeyboards('white')
        bgWhite = true
    } else {
        // SI ON PASSE DU BLANC AU NOIR
        document.getElementById('textZoneBack').classList.remove('textZone-white')
        document.getElementById('textZoneBack').classList.remove('graphContours')
        document.getElementById('graphiesZone').classList.remove('graphiesZone-white')
        var tabbl = document.getElementById('graphiesZoneTable')
        if (tabbl) tabbl.classList.remove('graphiesZoneTable-white')
        document.getElementById('graphiesZone').classList.remove('graphContours')

        document.documentElement.style.setProperty('--phon_a', phonA)
        //setKeyboards('black')
        bgWhite = false
    }
}

var gEndSave = new Object()
function toggleContentEditable() {
    var textZone = document.getElementById('textZone')
    var btnContentEd = document.getElementById('btnContentEditable')
    btnContentEd.classList.toggle("glyphiconSelected")
    if (textZone.contentEditable == "true") {
            textZone.contentEditable = "false"
            textZone.appendChild(gEndSave)
    } else {
            textZone.contentEditable = "true"
            gEndSave = document.getElementById('gEnd')
            textZone.removeChild(gEndSave)
    }
}


//////////////////////////////////////
////////// PARAMÉTRAGE POPUP /////////
////////// POUR COPIER TEXTE /////////
//////////////////////////////////////
// Get the modal
var modal = document.getElementById("myModal")
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0]

// Fonction pour ouvrir le PopUp 
function getPopUp() {
    if (bgWhite) bg2white();
    var outputContent = document.getElementById('textZone').innerHTML;
    var finalOutput = ""; // contiendra la chaîne de caractères à copier coller
    
    // On récupère la liste des <span>
    const regexSpan = /<span id=".*?" class="(.*?)".*?>(.*?)<\/span>|<br>/gm;
    console.log(regexSpan.exec(outputContent));
    let m;
    while ((m = regexSpan.exec(outputContent)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regexSpan.lastIndex) {
            regexSpan.lastIndex++;
        }
        console.log(m[1],m[2]);
        if (m[0] == "<br>") {
            finalOutput += "<br>";
        } else if (m[2] != "&nbsp;") {
            var classs = m[1].split(' ');
            var expFonte = "font-family: Ubuntu, SimKai, DejaVu Sans, Times, Calibri;";
            var expCoulBase = "color:#cccccc;";

            var expSizeBase = "font-size:30pt;";
            var expSizeStress1 = "font-size:45pt;";
            var expSizeStress2 = "font-size:36pt;";
            var expSizeSchwa = "font-size:20pt;";

            var expBold = "font-weight: bold;";
            var expNoBold = "font-weight: normal;";
            var expLong = "text-decoration: underline;";

            newOutputSpan = "<span style='" + expFonte + expBold + expSizeBase; // PAR DEFAUT
            
            if (classs.includes("punct")) newOutputSpan = "<span style='" + expFonte + expSizeBase;
            if (classs.includes("unstressed") || classs.includes("arabe") || classs.includes("chaoui")) newOutputSpan = "<span style='" + expFonte + expNoBold + expSizeBase;
            if (classs.includes("schwa")) newOutputSpan = "<span style='" + expFonte + expNoBold + expSizeSchwa;
            if (classs.includes("stress1")) newOutputSpan = "<span style='" + expFonte + expSizeStress1;
            if (classs.includes("stress2")) newOutputSpan = "<span style='" + expFonte + expSizeStress2;

            if (classs.includes("punct")) newOutputSpan += expCoulBase;
            // COULEURS
            phonClass = "";
            for (i=0; i<classs.length; i++) {
                if (classs[i].match(/phon_.*/)) phonClass = classs[i];
            }
            if (phonClass.match(/.*_long/) && !classs.includes("arabe")) newOutputSpan += expLong;
            if (phonClass != "") newOutputSpan += phon2color(phonClass);

            newOutputSpan += "'>"+m[2]+"</span>";

            finalOutput += newOutputSpan;
            
        }
    }

    console.log("finalOutput :",finalOutput)
    
	function phon2color(p1){
        if (p1 in bicol2colcol) {
            var col1 = getComputedStyle(document.documentElement).getPropertyValue(bicol2colcol[p1][0])
			var col2 = getComputedStyle(document.documentElement).getPropertyValue(bicol2colcol[p1][1])
			var res = 'background-color:'+col1+';color:'+col2+';-webkit-text-stroke-width: 0.5px;-webkit-text-stroke-color: #000000;'
        } else {
            if (p1.slice(p1.length-5)=="_long") p1 = p1.slice(0,p1.length-5);
			var col = getComputedStyle(document.documentElement).getPropertyValue('--'+p1)
			var res = 'color:'+col+';-webkit-text-stroke-width: 0.5px;-webkit-text-stroke-color: #000000;'
        }
		return res
    }

    
    finalOutputWhite = finalOutput.replace(/#ffffff/g,'#000000')
    finalOutput = finalOutput.replace(/-webkit-text-stroke-width: 0.5px;-webkit-text-stroke-color: #000000;/g,''); // on vire les strokes pour fond noir

	document.getElementById('pLienDiv').innerHTML = finalOutput
    document.getElementById('pLienDivCode').innerHTML = finalOutput
	document.getElementById('pLienDivWhite').innerHTML = finalOutputWhite
	document.getElementById('pLienDivWhiteCode').innerHTML = finalOutputWhite
    modal.style.display = "block"
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none"
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none"
    }
}

// COPIE AUTO DU LIEN DANS PRESSE-PAPIER
function toClipBoard(containerid,tooltipid) {
    //var copyText = document.getElementById("pLienDiv")
    // copyText.select()
    // copyText.setSelectionRange(0, 99999)
	// document.execCommand("copy")

	if (document.selection) {
		var range = document.body.createTextRange()
		range.moveToElementText(document.getElementById(containerid))
		range.select().createTextRange()
		document.execCommand("copy")
	} else if (window.getSelection) {
		var range = document.createRange()
		range.selectNode(document.getElementById(containerid))
		window.getSelection().empty()
		window.getSelection().addRange(range)
		document.execCommand("copy")
	}
	
    
	var tooltip = document.getElementById(tooltipid) 
	tooltip.innerHTML = "Copié !"
}
    
function outFunc(tooltipid) {
    var tooltip = document.getElementById(tooltipid)
    tooltip.innerHTML = "Copier"
}


//////////////////////////////////////////////
////////// SAUVEGARDE DE LA PAGE /////////////
//////////////////////////////////////////////
async function savePage() {
    // PRÉPARATION DU COLIS
    // Texte
    textZoneContent = document.getElementById('textZone').innerHTML

    // Panneau / Fidel
    var pan = document.getElementById('selectPanneau').value
    var fid = document.getElementById('selectFidel').value

    // Date
    var today = new Date()
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds()
    var dateTime = date+' '+time

    // Id
    if (pageId == '0') {
        pageId = 'id-' + Math.random().toString(36).substring(2,9)
        dateCreation = dateTime
    }

    // ON EMBALLE TOUT ÇA
    var colis = {
        pageId: pageId,
        ipCli: '',
        dateCreation: dateCreation,
        dateModif: dateTime,
        textZoneContent: textZoneContent,
        lang: thisPageLang,
        panneau: pan,
        fidel: fid
    }
    //console.log(colis)

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    }
    
    // ENVOI
    const response = await fetch('/export/', options)
    const data = await response.json()
    console.log(data)

    // OUVERTURE POPUP AVEC LE LIEN POUR RECHARGER LA PAGE
    getPopUpSave(data.identifiant)
}


//////////////////////////////////////
////////// PARAMÉTRAGE POPUP /////////
///////////// SAUVEGARDE /////////////
//////////////////////////////////////
// Get the modal
var modalSave = document.getElementById("myModalSave")
// Get the <span> element that closes the modal
var span2 = document.getElementsByClassName("close")[1]

// Fonction pour ouvrir le PopUp 
function getPopUpSave(lien) {
    document.getElementById('pLienSave').value = thisRawURL.replace(/#\/?$/,'') + lien
    modalSave.style.display = "block"
}

// When the user clicks on <span> (x), close the modal
span2.onclick = function() {
    modalSave.style.display = "none"
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modalSave) {
        modalSave.style.display = "none"
    }
}

// COPIE AUTO DU LIEN DANS PRESSE-PAPIER
function myFunctionSave() {
    var copyText = document.getElementById("pLienSave")
    copyText.select()
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy")
    
    var tooltipSave = document.getElementById("myTooltipSave")
    tooltipSave.innerHTML = "Copié !"
}
    
function outFuncSave() {
    var tooltipSave = document.getElementById("myTooltipSave")
    tooltipSave.innerHTML = "Copier"
}

function openInNewTab() {
    var URL = document.getElementById("pLienSave").value
    window.open(URL, '_blank')
}

function toggleFullScreen() {
    if (document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen) {
        document.exitFullscreen()
        document.getElementById("toggleFullScreenBtn").innerHTML = `<path d="m 5.828,10.172 c -0.1952499,-0.195191 -0.5117501,-0.195191 -0.707,0 -2.6618502,1.94423 -4.096,6.322035 -4.096,1.328 0,-0.666666 -1,-0.666666 -1,0 v 3.975 c 0,0.276142 0.22385763,0.5 0.5,0.5 H 4.5 c 0.6666664,0 0.6666664,-1 0,-1 -5.04044252,0.686128 -0.5920511,-2.175949 1.328,-4.096 0.195191,-0.19525 0.195191,-0.51175 0,-0.707 z m 4.344,-4.344 c 0.19525,0.195191 0.51175,0.195191 0.707,0 2.66185,-1.9442299 4.096,-6.32203458 4.096,-1.328 0,0.6666664 1,0.6666664 1,0 V 0.525 c 0,-0.27614237 -0.223858,-0.5 -0.5,-0.5 H 11.5 c -0.666666,10e-9 -0.666666,1 0,1 5.040443,-0.68612773 0.592051,2.1759489 -1.328,4.096 -0.195191,0.1952499 -0.195191,0.5117501 0,0.707 z"/>`
    } else { 
        document.documentElement.requestFullscreen();
        document.getElementById("toggleFullScreenBtn").innerHTML = `<path d="m 0.20589831,15.794102 c 0.19525,0.195191 0.51175,0.195191 0.707,0 2.66184999,-1.94423 4.09599999,-6.3220349 4.09599999,-1.328 0,0.666666 1,0.666666 1,0 v -3.975 c 0,-0.276143 -0.223858,-0.5000003 -0.5,-0.5000003 h -3.975 c -0.66666599,0 -0.66666599,1.0000003 0,1.0000003 5.040443,-0.686128 0.592051,2.175949 -1.32799999,4.096 -0.195191,0.19525 -0.195191,0.51175 0,0.707 z" />
                                                                    <path d="m 15.709659,0.29034053 c -0.19525,-0.195191 -0.511749,-0.195191 -0.706999,0 -2.66185,1.94422997 -4.096,6.32203517 -4.096,1.32799997 0,-0.66666597 -1.0000002,-0.66666597 -1.0000002,0 v 3.9750003 c 0,0.276143 0.2238582,0.5000003 0.5000002,0.5000003 h 3.975 c 0.666666,0 0.666666,-1.0000004 0,-1.0000004 -5.0404432,0.6861281 -0.592051,-2.1759492 1.327999,-4.09600017 0.195191,-0.19525 0.195191,-0.51175 0,-0.707 z" />`
    }
}


///////////////////////////////////
///////// RESPONSIVE TOOLS ////////

function hideGraphies() {
    document.getElementById('graphies').style.display = "none"
}
function showGraphies() {
    document.getElementById('graphies').style.display = "block"
}



// SOUNDS
function playEffect(effect) {
    var audio
    if (effect == "select") audio = new Audio("/static/audio/effets/select.mp3")
    else if (effect == "delete") audio = new Audio("/static/audio/effets/del.mp3")
    else if (effect == "success") audio = new Audio("/static/audio/effets/success.mp3")
    else if (effect == "correct") audio = new Audio("/static/audio/effets/correctAnswer.mp3")
    else if (effect == "wrong") audio = new Audio("/static/audio/effets/wrongAnswer.mp3")
    audio.play()
}


////////////////////////
// ÉCRITURE EN MANDARIN

var currentSyll = []; // [ "phon_p_h", "phon_a", "ton1"] 
let syll0 = {
    "initiale": { "rect":"", "phon":""},
    "glide": { "rect":"", "phon":""},
    "tonale": { "rect":"", "phon":""},
    "finale": { "rect":"", "phon":""},
    "tone": 0,
    "api": "",
    "pinyin": ""
}
let syll = syll0;
let syllFinie = {};

function zhPhon(rectId) {
    var phon = rectId.replace('rect_', 'phon_').replace('-t', '').replace('-f', '')
    if (gradientToRight.includes(phon)) { phon = phon+'-h' } // gradients à l'horizontal pour affriquées
    console.log("zhPhon",rectId, phon)

    if (listinitiales.includes(rectId)) {
        console.log("RÉINITIALISATION SYLL");
        syllFinie = syll;
        syll = {
            "initiale": { "rect":"", "phon":""},
            "glide": { "rect":"", "phon":""},
            "tonale": { "rect":"", "phon":""},
            "finale": { "rect":"", "phon":""},
            "tone": 0,
            "api": "",
            "pinyin": ""
        };
        currentSyll = [];
    }
    currentSyll.push(phon)

    if (listinitiales.includes(rectId)) syll.initiale.phon = phon;
    else if (listglides.includes(rectId)) syll.glide.phon = phon;
    else if (listtonales.includes(rectId) || listvoyelles.includes(rectId)) syll.tonale.phon = phon;
    else if (listfinales.includes(rectId)) syll.finale.phon = phon;

    var currentAPI = ""
    for (var ph=0; ph<currentSyll.length; ph++) {
        var phon = currentSyll[ph];
        console.log("PHON=",phon);
        if (phon.replace("-h","") in phon2api) {
            currentAPI += phon2api[phon.replace("-h","")];
        } else if (["0","1","2","3","4","5"].includes(phon[8])) {
            currentAPI += phon[8];
            syll.tone = phon[8];
            
            console.log("RÉINITIALISATION SYLL");
            syllFinie = syll;
            syllFinie.api = currentAPI;
            syllFinie.pinyin = getPinyin(currentAPI);
            syll = {
                "initiale": { "rect":"", "phon":""},
                "glide": { "rect":"", "phon":""},
                "tonale": { "rect":"", "phon":""},
                "finale": { "rect":"", "phon":""},
                "tone": 0,
                "api": "",
                "pinyin": ""
            };
            currentSyll = [];
        } else console.log("MANQUE",phon,"DANS PHON2API !")
    }
    console.log(currentAPI);
    
    var [charlist, freqlist] = getChars(currentAPI);

    if (charlist.length>0) {
        var hanDiv = document.createElement("div");
        hanDiv.id = "hanDiv";
        hanDiv.style.width = "100%";
        hanDiv.style.height = "100%";
        hanDiv.classList = "d-flex justify-content-evenly align-content-around flex-wrap";
        document.getElementById('graphiesZone').innerHTML = "";
        document.getElementById('graphiesZone').appendChild(hanDiv);

        document.getElementById('graphiesZone').appendChild(hanDiv);
        for(let char=0; char<charlist.length; char++) {
            console.log(charlist[char],freqlist[char]);
            var newHan = writeHan(charlist[char],getSize(freqlist[char]));
            newHan.setAttribute("onclick","writeGraphZh('"+charlist[char]+"','')");
            hanDiv.appendChild(newHan);
        }
        showGraphies()
    }
}
function recupTone(tonId){
    zhPhon(tonId)
}

function getSize(freq){
    var size = 0;
    if (freq>=100) size = 4;
    else if (freq<100 && freq>=10) size = 3;
    else if (freq<10 && freq>=5) size = 2;
    else size = 1
    return size;
}

function getChars(api) {
    var charlist = [];
    var freqlist = [];
    var x = ""; // api sans ton
    var y = ""; // ton seul
    if (["0","1","2","3","4","5"].includes(api[api.length-1])) {
        x = api.slice(0,api.length-1);
        y = api[api.length-1];
    }
    if (x in syllonAPI2pinyin && syllonAPI2pinyin[x]+y in syll2chars) {
        var pinyin = syllonAPI2pinyin[x]+y;
        var chars = syll2chars[pinyin];
        var keys = Object.keys(chars);
        keys.sort();
        for(var char=0; char<keys.length; char++) {
            charlist.push(keys[char]);
            freqlist.push(chars[keys[char]]);
        }
    }
    return [charlist, freqlist]
}

function getPinyin(api) {
    var pinyin = "";
    var x = ""; // api sans ton
    var y = ""; // ton seul
    if (["0","1","2","3","4","5"].includes(api[api.length-1])) {
        x = api.slice(0,api.length-1);
        y = api[api.length-1];
    }
    if (x in syllonAPI2pinyin && syllonAPI2pinyin[x]+y in syll2chars) {
        pinyin = syllonAPI2pinyin[x]+y;
    }
    return pinyin
}

function writeHan(char,size=0){
    var newHan = document.createElement("div");
    newHan.classList = "align-self-center han han"+size;
    newHan.appendChild(makeNewSyll(syllFinie));
    var newZi = document.createElement("div");
    newZi.classList = "hanzi graphContoursW zi"+size;
    newZi.innerHTML = char;
    newHan.appendChild(newZi);

    return newHan
}

function writeGraphZh(char, phon){
    var rectId = char

    var newCarte = document.createElement('div')
    newCarte.classList = "carteZh text";
    newCarte.draggable = true
    if (char != "space") newCarte.appendChild(writeHan(char));
    newCarte.setAttribute("data-api",syllFinie.api);
    newCarte.setAttribute("data-pinyin",syllFinie.pinyin);

    newCarte.addEventListener('dragstart', () => {
        newCarte.classList.add('dragging')
        console.log('DRAGGING ', rectId)
    })

    newCarte.addEventListener('dragend', () => {
        newCarte.classList.remove('dragging')
        console.log('STOP DRAGGING ', rectId)
    })

    newCarte.setAttribute("id", "syllzh-g"+Math.random().toString(36).substring(2,9))
    newCarte.setAttribute('onclick', "putAnchor(this.id)")
    
    insertAfter(document.getElementById(curseurPos), newCarte)
    document.getElementById(curseurPos).classList.remove('startPoint')
    curseurPos = newCarte.id
    document.getElementById(curseurPos).classList.add('startPoint')
    
    playEffect("select")
    if (document.body.clientWidth < 850) hideGraphies();
    // saveTrace('add phon '+rectId)
}

function makeNewSyll(syllFinie) {
    let newSyll = document.createElement('div')
    newSyll.classList = "syll"

    let newInitiale = document.createElement('div')
    let newGlide = document.createElement('div')
    let newTonale = document.createElement('div')
    let newFinale = document.createElement('div')

    var structure = [0,0,0,0];
    if (syllFinie.initiale.phon != "") structure[0]=1;
    if (syllFinie.glide.phon != "") structure[1]=1;
    if (syllFinie.tonale.phon != "") structure[2]=1;
    if (syllFinie.finale.phon != "") structure[3]=1;
    var structString = structure.join("");

    if (structString == "0") {
        prop = "_1"
    } else if (syllFinie.tonale.phon == "") {
        prop = "_" + (structString.match(/1/g).length+1)
    } else {
        prop = "_" + structString.match(/1/g).length
    }

    if (structString.length >= 4 && structString[structString.length-4] == 1) newInitiale.classList = prop + ' ' + syllFinie.initiale.phon + ' noTextClip'
    if (structString.length >= 3 && structString[structString.length-3] == 1) newGlide.classList = prop + ' ' + syllFinie.glide.phon + ' noTextClip'
    if (structString.length >= 2 && structString[structString.length-2] == 1) newTonale.classList = prop + ' ' + syllFinie.tonale.phon + ' noTextClip'
    if (structString.length >= 1 && structString[structString.length-1] == 1) newFinale.classList = prop + ' finale ' + syllFinie.finale.phon + ' noTextClip'

    newSyll.append(newInitiale, newGlide, newTonale, newFinale)

    let newDivTons = document.createElement('div')
    newDivTons.classList.add('divTons')
    if (syllFinie.tone == 1){ addTone(newDivTons, 't1'); addTone(newDivTons, 't4')}
    if (syllFinie.tone == 2){ addTone(newDivTons, 't2'); addTone(newDivTons, 't4')}
    if (syllFinie.tone == 3){ addTone(newDivTons, 't2'); addTone(newDivTons, 't8'); addTone(newDivTons, 't4')}
    if (syllFinie.tone == 4){ addTone(newDivTons, 't1'); addTone(newDivTons, 't6')}

    function addTone(el, toneEl) {
        let newTone = document.createElement('div')
        newTone.classList.add('ton')
        newTone.style.display = 'block'
        newTone.classList.add(toneEl)
        el.appendChild(newTone)
    }
    newSyll.appendChild(newDivTons)

    return newSyll
}

let listinitiales = [
    'rect_p',
    'rect_p_h',
    'rect_m',
    'rect_f',
    'rect_t',
    'rect_t_h',
    'rect_n',
    'rect_l',
    'rect_s',
    'rect_ts',
    'rect_t_hs',
    'rect_s_retr',
    'rect_ts_retr',
    'rect_t_hs_retr',
    'rect_z_retr',
    'rect_s_slash',
    'rect_ts_slash',
    'rect_t_hs_slash',
    'rect_k',
    'rect_x',
    'rect_k_h'
]

let gradientToRight = [
    'phon_ts',
    'phon_t_hs',
    'phon_ts_retr',
    'phon_t_hs_retr',
    'phon_ts_slash',
    'phon_t_hs_slash'
]

let listglides = [
    'rect_j',
    'rect_h_maj',
    'rect_w'
]

let listvoyelles = [
    'rect_i',
    'rect_y',
    'rect_1',
    'rect_u',
    'rect_7',
    'rect_o',
    'rect_a',
    'rect_e',
    'rect_e_maj',
    'rect_wo',
    'rect_m_maj7',
    'rect_7_rho',
    'rect_a_rho'
]

let listtonales = [
    'rect_i-t',
    'rect_y-t',
    'rect_u-t',
    'rect_e-t',
    'rect_7-t',
    'rect_o-t',
    'rect_e_maj-t',
    'rect_a-t'
]

let listfinales = [
    'rect_rho',
    'rect_j-f',
    'rect_w-f',
    'rect_rho-f',
    'rect_n-f',
    'rect_n_maj-f'
]


// FEEDBACK PHONOLOGIQUE & ORTHOGRAPHIQUE
//  - récupère la liste des mots (orthographiques, phonologiques) de #textZone
//  - requête wikicolor checkWord pour savoir si ortho existe, phono existe et ortho-phono existe
//  - feedback : 
//         - soulignement hachuré si phono existe pas
//         - soulignement plein si ortho existe pas

// var checkWordCheck = false; // quand true, checkWord à chaque writeGraph()
// function checkCheckWord(){
//     if (checkWordCheck){
//         checkWordCheck=false;
//         document.getElementById("checkWordCheck").classList.remove("checkWordCheck");
//     } else {
//         checkWordCheck=true;
//         document.getElementById("checkWordCheck").classList.add("checkWordCheck");
//     }
// }

function checkWord() {
    var ww = []; // contiendra liste de mots [] contenant liste phonographies ["m","phon_m"]
    var rep = document.getElementById("textZone").children;

    var w = []; // phonographie du mot courant
    for (r of rep) {
        if (r.classList.contains("punct") || r.classList.contains("pause") || r.classList.contains("space") || r.id == "gEnd" || r.innerText == "‿" || r.innerText.slice(r.innerText.length-1)=="’") {
            if (r.innerText.slice(r.innerText.length-1)=="’") w.push([r.innerText, r.classList[1], r.id, 0])
            if (w.length>0) {
                ww.push(w);
            }
            w = [];
        } else if (r.classList[0]=="carteZh") {
            w.push([r.children[0].children[1].innerText, r.dataset.pinyin, r.id, 0])
        } else if (r.innerText != "" && r.innerText != " " && r.id.slice(0,5)=="phon_") {
            var rinner = r.innerText
            var stress = 0
            if (r.innerText == "͜") rinner = "";
            if (r.classList.contains('stress1')) stress = 1
            if (r.classList.contains('stress2')) stress = 2
            w.push([rinner, r.classList[1], r.id, stress])
        
        } else if (r.classList[0]=="carte") {
            var stress = 0
            if (r.classList.contains('stress1')) stress = 1
            if (r.classList.contains('stress2')) stress = 2
            w.push(["",r.classList[2],r.id, stress])
        }
    
    }
    console.log(ww);

    var wordPhonoSpansList = []

    if (['fr','en','zh'].includes(thisPageLang)) {
        let cptw = 0;
        for (w of ww) {
            cptw++;
            var word = "";
            var phono = "";
            var listSpans = []; // tous les spans concernés, qu'il faudra souligner le cas échéant
            for (p of w) {
                word += p[0];
                if (thisPageLang=="zh") phono += " "+p[1];
                else {
                    if (p[3]==1) phono += "ˈ"
                    if (p[3]==2) phono += "ˌ"
                    
                    if (thisPageLang=="en" && p[1]=="phon_e") phono += phon2api["phon_e_maj"]
                    else phono += phon2api[p[1]];
                }
                listSpans.push(p[2]);
            }
            var fin = false;
            if (cptw == ww.length) fin = true;
            document.getElementById('checkWordValid').style.display = "none";
            document.getElementById('checkWordLoading').style.display = "block";
            wordPhonoSpansList.push( {'w':word, 't':phono.trim(), 'spans':listSpans} );
        }
        reqWiki(wordPhonoSpansList,thisPageLang);
    } else {
        window.alert("Cette fonctionnalité n'est pas encore disponible pour cette langue");
    }
    addStat("checkword",thisPageLang);
}

async function reqWiki(wordPhonoSpansList,lang) {
    // wordPhonoSpansList = {
    //     {
    //         'w':"orthographe",
    //         't':"transcriptionAPI",
    //         'spans':['id1','id2','id3']
    //     },
    //     {w2},
    //     {w3}
    // }
    console.log("reqWiki",wordPhonoSpansList);

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({'wordPhonoSpansList':wordPhonoSpansList, 'lang':lang})
    }
	const response = await fetch(`https://wikicolor.alem-app.fr/checkWordList/`, options); // `http://127.0.0.1:8000/checkWordList/`
    const data = await response.json();

    console.log(data);
    
    for (let x=0; x<data.rep.length; x++){
        var word = data.rep[x];
        for (span of wordPhonoSpansList[x].spans){
            var el = document.getElementById(span);
            var title = []
    
            el.classList.remove("FBortho");
            el.classList.remove("FBphonoCarte");
            el.classList.remove("FBphonoGraphCarte");
            el.classList.remove("FBphono");
            el.classList.remove("FBphonoGraph");
    
                
            if (!word.t) {
                if (el.classList[0]=="carte" || el.classList[0]=="carteZh") el.classList.add("FBphonoCarte"); // que bouches et mandarin
                else el.classList.add("FBphono");
                title.push("problème de phonologie")
            }
            
            if (!word.w) {
                if (!el.classList.contains("carte")) el.classList.add("FBortho"); // tous sauf bouches
                title.push("problème d'orthographe")
    
            } else if (word.t && !word.wt) {
                if (el.classList[0]=="carte" || el.classList[0]=="carteZh") el.classList.add("FBphonoGraphCarte"); // que bouches et mandarin
                else el.classList.add("FBphonoGraph");
                title.push("orthographe et phonologie existantes mais pas pour le même mot")
            }
            
            el.title = title.join("\n");
        }
    }

    document.getElementById('checkWordLoading').style.display = "none";
    document.getElementById('checkWordValid').style.display = "block";
    
}