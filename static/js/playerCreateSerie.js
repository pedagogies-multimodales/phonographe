
var codeDiv = document.getElementById('code');
var nomDiv = document.getElementById('nom');
var descDiv = document.getElementById('description');

function verifCode(){
    if (!codeDiv.value.match(/^[a-zA-Z0-9-_+]+$/)) {
        codeDiv.classList.remove('is-valid');
        codeDiv.classList.add('is-invalid');
    } else {
        codeDiv.classList.remove('is-invalid');
        codeDiv.classList.add('is-valid');
    }
}

function saveNewSerie(){
    if (codeDiv.classList.contains("is-valid") && nomDiv.value.length>0) {
        sendSerie(codeDiv.value, nomDiv.value, descDiv.value)
    } else {
        window.alert('Indiquez un code et un nom de série pour pouvoir la créer.')
    }
}

async function sendSerie(code,nom,desc) {
    // ON EMBALLE TOUT ÇA
    var colis = {
        "code": code,
        "nom": nom,
        "description": desc
    }

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
    const response = await fetch('/player/saveNewSerie/', options);
    const data = await response.json();
    
    console.log(data);
    
    if (data.rep) window.location = "/player/serie-"+code+"/edit";
    else window.alert(data.msg);
}

function goBack() {
    window.history.back();
    // window.location = "/player/"
}