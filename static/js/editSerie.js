
// Activation du Drag and Drop sortable JQUERY
// https://jqueryui.com/sortable/
$( function() {
    $( "#contenuSerie" ).sortable();
} );


// Initialisation (remise à zéro des checkbox si enregistrées dans cache du navigateur)
document.getElementById('cbAudioDeb').checked = false;
document.getElementById('cbImageDeb').checked = false;
document.getElementById('cbVideoDeb').checked = false;
document.getElementById('cbGraphieDeb').checked = false;
document.getElementById('cbAudioFin').checked = false;
document.getElementById('cbImageFin').checked = false;
document.getElementById('cbVideoFin').checked = false;
document.getElementById('cbGraphieFin').checked = false;
document.getElementById('cbPhonoFin').checked = false;
document.getElementById('cbSyntheseVocale').checked = false;
document.getElementById('cbPanneauPartiel').checked = false;

document.getElementById('description').value = serie.description;

// CHARGEMENT SERIE EXISTANTE
var listeMotsGen = []
if (serie.nom.length == 0) {
    console.log("Création d'une nouvelle série");
} else {
    console.log('Édition de la série',serie.nom);
    addStat("phonoplayer-editserie",thisPageLang);

    for (i=0; i<listeMots.length; i++) {
        listeMotsGen.push(listeMots[i].motGenerique)
    }
    document.getElementById('listemots').value = listeMotsGen.join(', ');
    document.getElementById('listePhonemesVisibles').value = apiVisibles.join(', ');

    if (serie.lang != "null") {document.getElementById('langue').value = serie.lang}
    document.getElementById("visibility").value = serie.visibility;

    // Checkage des cb
    if (serie.audioDeb == 1) {document.getElementById('cbAudioDeb').checked = true; toggleMedia(document.getElementById('cbAudioDeb'));}
    if (serie.audioDeb == null && serie.videoDeb == 0) {document.getElementById('cbAudioDeb').checked = true; toggleMedia(document.getElementById('cbAudioDeb'));}
    if (serie.imageDeb == 1) {document.getElementById('cbImageDeb').checked = true; toggleMedia(document.getElementById('cbImageDeb'));}
    if (serie.videoDeb == 1) {document.getElementById('cbVideoDeb').checked = true; toggleMedia(document.getElementById('cbVideoDeb'));}
    if (serie.graphieDeb == 1) {document.getElementById('cbGraphieDeb').checked = true; toggleMedia(document.getElementById('cbGraphieDeb'));}

    // if (serie.audioFin == 1) {document.getElementById('cbAudioFin').checked = true; toggleMedia(document.getElementById('cbAudioFin'));}
    if (serie.imageFin == 1) {document.getElementById('cbImageFin').checked = true; toggleMedia(document.getElementById('cbImageFin'));}
    // if (serie.videoFin == 1) {document.getElementById('cbVideoFin').checked = true; toggleMedia(document.getElementById('cbVideoFin'));}
    if (serie.graphieFin == 1) {document.getElementById('cbGraphieFin').checked = true; toggleMedia(document.getElementById('cbGraphieFin'));}
    if (serie.phonoFin == 1) {document.getElementById('cbPhonoFin').checked = true; toggleMedia(document.getElementById('cbPhonoFin'));}

    if (serie.syntheseVocale == 1) document.getElementById('cbSyntheseVocale').checked = true;
    if (serie.panneauPartiel == 1) document.getElementById('cbPanneauPartiel').checked = true;

    generate();
}

function toggleMedia(el) {
    if (el.checked) {
        document.documentElement.style.setProperty('--divMotCell'+el.id.slice(2), "block");
    } else {
        document.documentElement.style.setProperty('--divMotCell'+el.id.slice(2), "none");
    }    
}


function generate(){
    // récupération de la liste de mots (split sur "," et trim)
    var listemots = document.getElementById('listemots').value;
    listemots = listemots.split(',');

    document.getElementById('contenuSerie').innerHTML = ""; // Réinitialisation de la liste des mots
    
    // pour chaque mot, on crée une entrée (divMot) et on requête Wikicolor pour récupérer les phono possibles
    for(i=0; i<listemots.length; i++){
        var mot = listemots[i].trim().toLowerCase();

        addMot(mot,i);
    }
}

function openModalAddMot() {
    // ouvre une petite fenêtre pour entrer un mot à ajouter à la fin de la liste
    document.getElementById('popAddwMot').value = '';
    
    var myModal = new bootstrap.Modal(document.getElementById('popAddw'));
    myModal.show();
    getAPIkeyboard('fr');
}

function openModalAddPron(motId) {
    // ouvre une petite fenêtre pour entrer une prononciation à ajouter au mot i passé en argument
    document.getElementById('popAddPronId').innerHTML = motId;
    
    var myModal = new bootstrap.Modal(document.getElementById('popAddPron'));
    myModal.show();
    getAPIkeyboard('fr');
}

function addPron(motId,trans=[]) {
    var jphono = document.getElementById(motId).children.length;
    console.log(motId, jphono, trans);
    addPhono(motId, jphono, trans);
}

function toList(s) {
    // Fait une liste de liste à partir d'une string du type : "phon_d, phon_r_maj, phon_o, phon_m\nphon_d, phon_r_maj, phon_o_maj, phon_m"
    s = s.replace(/\s+/g,'') // suppr espaces
    ss = s.split('\n') // split en mots si plusieurs lignes (séparés par \n)
    nw = []
    ss.forEach((w) => { nw.push(w.split(',')) })
    console.log("toList",s,nw)
    return nw
}

function addMot(mot,trans=[],i) {
    console.log(mot, trans, i)
    i = Math.floor(Math.random() * 10000);
    var divMot = document.createElement('div');
    divMot.id = 'divMot-' + i;
    divMot.classList = 'ui-state-default d-flex flex-row align-items-center divMot';
    divMot.style.position = 'relative';
    var divMotSuppr = document.createElement('button');
    divMotSuppr.id = "divMotSuppr-"+i;
    divMotSuppr.classList = "btn btn-danger";
    divMotSuppr.style = "position: absolute; right: -30px; top: 10px; border-radius: 0%; border-top-right-radius: 50%; border-bottom-right-radius: 50%; width: 30px; padding: 5px;"
    divMotSuppr.type = "button";
    divMotSuppr.onclick = function() {this.parentElement.remove();};
    divMotSuppr.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg>'
    divMot.appendChild(divMotSuppr);

    document.getElementById('contenuSerie').appendChild(divMot);

    var divMotGrab = document.createElement('div');
    divMotGrab.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="30" height="50" fill="currentColor" class="bi bi-grip-vertical" viewBox="0 0 16 16">  <path d="M7 2a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM7 5a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM7 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm-3 3a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm-3 3a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/></svg>';
    divMotGrab.classList = 'align-self-stretch d-flex align-items-center';
    divMotGrab.style.cursor = "grab";
    divMot.appendChild(divMotGrab);

    var divMotGen = document.createElement('div');
    divMotGen.id = 'divMotGen-' + i;
    divMotGen.classList = 'p-2 align-self-stretch divMotCellMot d-flex align-items-center';
    divMotGen.style.cursor = "grab";
    divMotGen.innerHTML = mot;
    divMot.appendChild(divMotGen);

    var divMotInfo = document.createElement('div');
    divMotInfo.classList = "d-flex flex-column";
    divMot.appendChild(divMotInfo);

    var divMotPhonoPlus = document.createElement('div');
    divMotPhonoPlus.id = 'divMotPhonoPlus-' + i;
    divMotPhonoPlus.classList = 'd-flex flex-column mt-2 align-items-center';
    divMot.appendChild(divMotPhonoPlus);

        var divMotPhono = document.createElement('div');
        divMotPhono.id = 'divMotPhono-' + i;
        divMotPhono.classList = 'd-flex flex-row divMotPhono';
        divMotPhonoPlus.appendChild(divMotPhono);

        

    var divMotInfoFin = document.createElement('div');
    divMotInfoFin.classList = "d-flex flex-column";
    divMot.appendChild(divMotInfoFin);
    
    ///////
    /////// ICÔNES MÉDIA
    ///////
    /////////////////////////////// DÉBUT
    // AUDIO DÉBUT
    var divMotAudio = document.createElement('div');
    divMotAudio.id = 'divMotAudioDeb-' + i;
    divMotAudio.classList = 'p-2 align-self-stretch divMotCellMedia divMotCellAudioDeb';
    divMotAudio.style.position = "relative";
    divMotAudio.innerHTML = `<button id="divMotAudioDebVierge-${i}" onclick="openFileList('audioDeb', ${i})" type="button" class="btn btn-outline-primary"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-volume-up" viewBox="0 0 16 16">  <path d="M11.536 14.01A8.473 8.473 0 0 0 14.026 8a8.473 8.473 0 0 0-2.49-6.01l-.708.707A7.476 7.476 0 0 1 13.025 8c0 2.071-.84 3.946-2.197 5.303l.708.707z"/>  <path d="M10.121 12.596A6.48 6.48 0 0 0 12.025 8a6.48 6.48 0 0 0-1.904-4.596l-.707.707A5.483 5.483 0 0 1 11.025 8a5.483 5.483 0 0 1-1.61 3.89l.706.706z"/>  <path d="M10.025 8a4.486 4.486 0 0 1-1.318 3.182L8 10.475A3.489 3.489 0 0 0 9.025 8c0-.966-.392-1.841-1.025-2.475l.707-.707A4.486 4.486 0 0 1 10.025 8zM7 4a.5.5 0 0 0-.812-.39L3.825 5.5H1.5A.5.5 0 0 0 1 6v4a.5.5 0 0 0 .5.5h2.325l2.363 1.89A.5.5 0 0 0 7 12V4zM4.312 6.39 6 5.04v5.92L4.312 9.61A.5.5 0 0 0 4 9.5H2v-3h2a.5.5 0 0 0 .312-.11z"/></svg></button>`;
    divMotInfo.appendChild(divMotAudio);
    
    if (listeMotsGen.includes(mot) && listeMots[listeMotsGen.indexOf(mot)].audioDeb != "") {
        if (listeMots[listeMotsGen.indexOf(mot)].hasOwnProperty('audio_R')) {
            // Cas de l'ancienne structure de série
            linkMedia(i,'audioDeb',listeMots[listeMotsGen.indexOf(mot)].audio);
        } else {
            linkMedia(i,'audioDeb',listeMots[listeMotsGen.indexOf(mot)].audioDeb);
        }
    }

    // VIDÉO DÉBUT
    var divMotVideo = document.createElement('div');
    divMotVideo.id = 'divMotVideoDeb-' + i;
    divMotVideo.classList = 'p-2 align-self-stretch divMotCellMedia divMotCellVideoDeb';
    divMotVideo.style.position = "relative";
    
    divMotVideo.innerHTML = `<button id="divMotVideoDebVierge-${i}" onclick="openFileList('videoDeb', ${i})" type="button" class="btn btn-outline-primary"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-film" viewBox="0 0 16 16">   <path d="M0 1a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V1zm4 0v6h8V1H4zm8 8H4v6h8V9zM1 1v2h2V1H1zm2 3H1v2h2V4zM1 7v2h2V7H1zm2 3H1v2h2v-2zm-2 3v2h2v-2H1zM15 1h-2v2h2V1zm-2 3v2h2V4h-2zm2 3h-2v2h2V7zm-2 3v2h2v-2h-2zm2 3h-2v2h2v-2z"/> </svg></button>`;
    
    divMotInfo.appendChild(divMotVideo);
    
    if (listeMotsGen.includes(mot) && listeMots[listeMotsGen.indexOf(mot)].videoDeb != "") {
        if (listeMots[listeMotsGen.indexOf(mot)].hasOwnProperty('audio_R')) {
            // Cas de l'ancienne structure de série
            linkMedia(i,'videoDeb',listeMots[listeMotsGen.indexOf(mot)].video);
        } else {
            linkMedia(i,'videoDeb',listeMots[listeMotsGen.indexOf(mot)].videoDeb);
        }
    }

    /////////////////////////////// FIN
    // IMAGE FIN
    var divMotImageFin = document.createElement('div');
    divMotImageFin.id = 'divMotImageFin-' + i;
    divMotImageFin.classList = 'p-2 align-self-stretch divMotCellMedia divMotCellImageFin';
    divMotImageFin.style.position = "relative";
    
    divMotImageFin.innerHTML = `<button id="divMotImageFinVierge-${i}" onclick="openFileList('imageFin', ${i})" type="button" class="btn btn-outline-primary"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-card-image" viewBox="0 0 16 16">  <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>  <path d="M1.5 2A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13zm13 1a.5.5 0 0 1 .5.5v6l-3.775-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12v.54A.505.505 0 0 1 1 12.5v-9a.5.5 0 0 1 .5-.5h13z"/></svg></button>`;
    
    divMotInfoFin.appendChild(divMotImageFin);
    
    if (listeMotsGen.includes(mot) && listeMots[listeMotsGen.indexOf(mot)].imageFin != "") {
        linkMedia(i,'imageFin',listeMots[listeMotsGen.indexOf(mot)].imageFin);
    }

    // GRAPHIE FIN
    var divMotGraphieFin = document.createElement('div');
    divMotGraphieFin.id = 'divMotGraphieFin-' + i;
    divMotGraphieFin.classList = 'p-2 align-self-stretch divMotCellMedia divMotCellGraphieFin';
    divMotGraphieFin.style.position = "relative";
    
    divMotGraphieFin.innerHTML = `<button id="divMotGraphieFinVierge-${i}" onclick="showWysiwygModal('graphieFin', ${i})" type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-richtext" viewBox="0 0 16 16"><path d="M7 4.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0zm-.861 1.542 1.33.886 1.854-1.855a.25.25 0 0 1 .289-.047l1.888.974V7.5a.5.5 0 0 1-.5.5H5a.5.5 0 0 1-.5-.5V7s1.54-1.274 1.639-1.208zM5 9a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm0 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1H5z"/><path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/></svg>
                                </button>`;
    
    divMotInfoFin.appendChild(divMotGraphieFin);

    if (listeMotsGen.includes(mot) && listeMots[listeMotsGen.indexOf(mot)].graphieFin != "") {
        linkMedia(i,'graphieFin',listeMots[listeMotsGen.indexOf(mot)].graphieFin);
    }

    // PHONO FIN
    var divMotPhonoFin = document.createElement('div');
    divMotPhonoFin.id = 'divMotPhonoFin-' + i;
    divMotPhonoFin.classList = 'p-2 align-self-stretch divMotCellMedia divMotCellPhonoFin';
    divMotPhonoFin.style.position = "relative";
    
    divMotPhonoFin.innerHTML = `<button id="divMotPhonoFinVierge-${i}" onclick="" type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16"><path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/><path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/></svg>
                                <span class="contours3"><span class="phon_f">Ph</span><span class="phon_o">o</span><span class="phon_n">n</span><span class="phon_o">o</span><span class="phon_g">g</span><span class="phon_r">r</span><span class="phon_a">a</span><span class="phon_f">ph</span><span class="phon_i">ie</span></span>
                                </button>`;
    
    divMotInfoFin.appendChild(divMotPhonoFin);
    
    // if (listeMotsGen.includes(mot) && listeMots[listeMotsGen.indexOf(mot)].phonoFin != "") {
    //     // linkMedia(i,'imageFin',listeMots[listeMotsGen.indexOf(mot)].imageFin);
    //     divMotPhonoFin.innerHTML = listeMots[listeMotsGen.indexOf(mot)].phonoFin;
    // }
        


    if (listeMotsGen.includes(mot)) {
        if (listeMots[listeMotsGen.indexOf(mot)].hasOwnProperty('audio_R')) {
            makePhonoliste([[listeMots[listeMotsGen.indexOf(mot)].phono]], divMotPhono.id);    
        } else {
            makePhonoliste([listeMots[listeMotsGen.indexOf(mot)].phono], divMotPhono.id);
        }
    } else if (trans.length>0) {
        makePhonoliste(trans, divMotPhono.id);
    } else {
        getPhono(mot, divMotPhono.id);
    }


    // $('#divMot-'+i).hover( function() {
    //     $('#divMotSuppr-'+i).toggle();
    // })
}

function openFileList(media, i) {
    var targetDir = "ERROR";
    if (["audioDeb","audioFin"].includes(media)) {
        targetDir = "audio"
        document.getElementById('divBtnUpload').innerHTML = `<button type="button" onclick="uploadAudio('${media}')" id="btnUploadFile" class="btn btn-success" title="Uploader un ou plusieurs fichiers depuis votre ordinateur">Parcourir...</button>`
    } else if (["videoDeb","videoFin"].includes(media)) {
        targetDir = "video"
        document.getElementById('divBtnUpload').innerHTML = `<button type="button" onclick="uploadVideo('${media}')" id="btnUploadFile" class="btn btn-success" title="Uploader un ou plusieurs fichiers depuis votre ordinateur">Parcourir...</button>`
    } else if (["imageDeb","imageFin"].includes(media)) {
        targetDir = "image"
        document.getElementById('divBtnUpload').innerHTML = `<button type="button" onclick="uploadImage('${media}')" id="btnUploadFile" class="btn btn-success" title="Uploader un ou plusieurs fichiers depuis votre ordinateur">Parcourir...</button>`
    }
    
    var oldTargetDir = document.getElementById('modalTargetDir').innerText;
    document.getElementById('modalTargetDir').innerText = targetDir;
    
    document.getElementById('modalTypMedia').innerText = media;
    document.getElementById('modalI').innerText = i;
    
    var myModal = new bootstrap.Modal(document.getElementById('fileListModal'))
    document.getElementById('recordingsList').innerHTML = "";
    document.getElementById('recordfilename').value = "";
    myModal.show()
    
    if (oldTargetDir!=targetDir) getMediaListe(targetDir);
}

function reloadFiles() {
    getMediaListe(document.getElementById('modalTargetDir').innerText);
}

async function getMediaListe(targetDir) {
    document.getElementById("fileTableMeta").innerHTML = "";
    document.getElementById("loader").style.display = "block";
	// ON EMBALLE TOUT ÇA
    var colis = {
		targetDir: targetDir
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('/player/listMediaFiles/', options); 
    const data = await response.json();
    console.log(data);
    var fl=data["listfiles"];

    // $('#fileTable').DataTable( {
    //     destroy:true,
    //     data:fl,
    //     select:'single' 
    // } );

    document.getElementById("fileTableMeta").innerHTML = `
    <table class="table table-hover order-column" id="fileTable" data-order='[[ 1, "asc"]]' data-page-length='10'>
                        <thead>
                        <tr>
                            <th scope="col">Apperçu</th>
                            <th scope="col">Fichier</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody id="fileList">
                            <!-- généré automatiquement -->
                        </tbody>
                    </table>
                    `

    // document.getElementById('fileList').innerHTML = "";
    
    for (f in fl) {
        if (targetDir=="image") {
            document.getElementById('fileList').innerHTML += `<tr>
                                                                    <td><img class="fileListIm" src="../../${fl[f][0]}"</td>
                                                                    <td>${fl[f][0].replace(/.*-uploads\//,'')}</td>
                                                                    <td><span class="trash" onclick="removeFile('${fl[f][0]}','${targetDir}')">Supprimer</span></td>
                                                                </tr>`
        } else if (targetDir=="audio") {
            document.getElementById('fileList').innerHTML += `<tr>
                                                                    <td><div onclick="playAudio('../../${fl[f][0]}');" class="ecouter">🔊</div></td>
                                                                    <td>${fl[f][0].replace(/.*-uploads\//,'')}</td>
                                                                    <td><span class="trash" onclick="removeFile('${fl[f][0]}','${targetDir}')">Supprimer</span></td>
                                                                </tr>`
        } else if (targetDir=="video") {
            document.getElementById('fileList').innerHTML += `<tr>
                                                                    <td><a target="_blank" href="../../${fl[f][0]}">Voir</a></td>
                                                                    <td>${fl[f][0].replace(/.*-uploads\//,'')}</td>
                                                                    <td><span class="trash" onclick="removeFile('${fl[f][0]}','${targetDir}')">Supprimer</span></td>
                                                                </tr>`
        }
    }
    var myTable = $('#fileTable').DataTable({ destroy:true, select:'single'  });
    document.getElementById("loader").style.display = "none";
    myTable.draw()
    // myTable.ajax.reload();

}

// SOUNDS
function playAudio(path) {
    var audio = new Audio(path)
    audio.play()
}

function confirmSelectionMedia(file="") {
    var selectedMedia = ""
    if (file.length>0) {
        selectedMedia = file;
    } else if (document.querySelectorAll('.selected').length>0) {
        selectedMedia = document.querySelectorAll('.selected')[0].children[1].innerText;
    } else {
        document.getElementById('modalClose').click();
    }
    console.log("confirmSelectionMedia",selectedMedia);
    if (selectedMedia.length>0) {
        var typMedia = document.getElementById('modalTypMedia').innerText;
        var i = document.getElementById('modalI').innerText;
        linkMedia(i,typMedia, selectedMedia);
        document.getElementById('modalClose').click();
    }
}

function linkMedia(i,typMedia, mediaName) {
    var myMediaPlayer = "";
    var myMediaLink = "";
    var textMediaClass = "";
    var Media = typMedia.slice(0,1).toUpperCase() + typMedia.slice(1);
    var mySuppr = `<button id="divMot${Media}PleinSuppr-${i}" onclick="this.parentElement.parentElement.remove();document.getElementById('divMot${Media}Vierge-${i}').style.display='block';" type="button" class="btn btn-secondary" style="width:20px;height:20px;padding:0px;border-radius:50%;position:absolute;top:-5px;right:-5px;display:none"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 3 16 16">  <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>  <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/></svg></button>`;
    
    if (typMedia=="audioDeb" || typMedia=="audioFin") {
        myMediaPlayer = ` <audio controls><source src="../../../media/audio-uploads/${mediaName}" type="audio/mpeg">Votre navigateur ne permet pas d'afficher le lecteur audio.</audio> `;
        myMediaLink = `<div class="divMotCell${Media}Link mediaLink me-auto" id="fileuploaded-${i}a" onclick="openFileList('${typMedia}', ${i})">${mediaName}</div>`;
    }

    if (typMedia=="imageDeb" || typMedia=="imageFin") {
        myMediaPlayer = `<img style="max-width: 100px;max-height: 100px;margin:auto;" src="../../../media/image-uploads/${mediaName}">`;
        myMediaLink = `<div class="divMotCell${Media}Link mediaLink me-auto" id="fileuploaded-${i}b" onclick="openFileList('${typMedia}', ${i})">${mediaName}</div>`;
    }

    if (typMedia=="videoDeb" || typMedia=="videoFin") {
        myMediaPlayer = `<video width="100%" height="100%" controls><source src="../../../media/video-uploads/${mediaName}" type="video/mp4">Votre navigateur ne permet pas d'afficher le lecteur vidéo.</video>`;
        myMediaLink = `<div class="divMotCell${Media}Link mediaLink me-auto" id="fileuploaded-${i}c" onclick="openFileList('${typMedia}', ${i})">${mediaName}</div>`;
    }

    if (typMedia=="graphieDeb" || typMedia=="graphieFin") {
        myMediaPlayer = `<div onclick="showWysiwygModal('${typMedia}', ${i})">${mediaName}</div>`;
        textMediaClass = "textMediaDiv";
    }
    
    document.getElementById('divMot'+Media+'Vierge-'+i).style.display = "none";
    var ancienMedia = document.getElementById('divMot'+Media+'Plein-'+i);
    if (typeof(ancienMedia) != 'undefined' && ancienMedia != null) {
        document.getElementById('divMot'+Media+'-'+i).removeChild(ancienMedia);
    }
    document.getElementById('divMot'+Media+'-'+i).innerHTML += `<div id="divMot${Media}Plein-${i}" class="card mediaCard ${textMediaClass}" style="position:relative">${myMediaPlayer}<div class="card-body d-flex mediaCardBody">${myMediaLink}${mySuppr}</div></div>`;
    $('#divMot'+Media+'Plein-'+i).hover( function() {
        $('#divMot'+Media+'PleinSuppr-'+i).toggle();
    })
}


async function getPhono(mot, targetDiv) {
	// ON EMBALLE TOUT ÇA
    var colis = {
		inText: mot,
		lang: "fr"
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('https://wikicolor.alem-app.fr/getPhonoOf/', options); //'http://127.0.0.1:9000/getPhonoOf/', options); //
    const data = await response.json();
    console.log(data);
    
    makePhonoliste(data.outText, targetDiv);
}

function makePhonoliste(data, targetDiv){   
    for(imot=0; imot < data.length; imot++){
        // pour chaque mot, on crée une divMotPhonoPlus qui contient une divMotPhonoTable (avec les prononciations possibles) et un bouton PLUS
        var divMotPhonoPlus = document.createElement('div');
        divMotPhonoPlus.classList = 'd-flex flex-column align-items-center';
        document.getElementById(targetDiv).appendChild(divMotPhonoPlus);

        var divMotPhonoTable = document.createElement('div');
        divMotPhonoTable.id = targetDiv + '-' + imot;
        divMotPhonoTable.classList = 'd-flex flex-column';
        divMotPhonoPlus.appendChild(divMotPhonoTable);

        var btnPhonoPlus = document.createElement('div');
        btnPhonoPlus.innerHTML = `<button type="button" class="btn" style="border-radius:50%" title="Ajouter une prononciation possible" onclick="openModalAddPron('${divMotPhonoTable.id}')">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
            </svg>
        </button>`
        divMotPhonoPlus.appendChild(btnPhonoPlus);
        
        for(jphono=0; jphono < data[imot].length; jphono++){
            if (data[imot][jphono][0] != 'phon_echec' && data[imot][jphono][0] != 'phon_neutre'){
                addPhono(divMotPhonoTable.id, jphono, data[imot][jphono]);
            }
        }
    }
    
}

function addPhono(divMotPhonoTableId, jphono, phono) {
    var phono_i = document.createElement('div');
    phono_i.classList = "px-4 d-flex align-items-center";
    phono_i.id = divMotPhonoTableId + '-' + jphono;

    var phono_i_check = document.createElement('div');
    phono_i_check.classList = "d-flex align-items-center";
    var checkedOrNot = "checked";
    
    phono_i_check.innerHTML = '<input id="check-'+ divMotPhonoTableId + '-' + jphono +'" type="checkbox" class="form-check-input" '+ checkedOrNot +'>';
    phono_i.appendChild(phono_i_check);

    var phono_i_phono = document.createElement('div');
    phono_i_phono.id = divMotPhonoTableId + '-' + jphono + '-phons';
    phono_i_phono.classList = "d-flex m-2";
    phono_i.appendChild(phono_i_phono);

    var phono_i_phonoText = document.createElement('div');
    phono_i_phonoText.id = divMotPhonoTableId + '-' + jphono + '-phonsText';
    phono_i_phonoText.style.display = 'none';
    phono_i.appendChild(phono_i_phonoText);

    document.getElementById(divMotPhonoTableId).appendChild(phono_i);

    var phono_i_phonoTextList = [];
    for(kphon=0; kphon < phono.length; kphon++){
        if (phono[kphon] != 'phon_echec' && phono[kphon] != 'phon_neutre'){
            if (Object.keys(api2phon).includes(phono[kphon])) {
                // SI API
                appendPhonCard(api2phon[phono[kphon]], phono_i_phono);
                phono_i_phonoTextList.push(api2phon[phono[kphon]]);
            } else {
                // SI PHON
                appendPhonCard(phono[kphon], phono_i_phono);
                phono_i_phonoTextList.push(phono[kphon]);
            }
        }
    };
    phono_i_phonoText.innerHTML = phono_i_phonoTextList.join(',');
}


function appendPhonCard(phonId, targetDiv) {
    var newCarteDiv = document.createElement('div');
    newCarteDiv.classList = "createSerieCarteDiv " + phonId + " noTextClip";

    var newCarte = document.createElement('div');
    newCarte.classList = "createSerieCarte";
    newCarte.style.backgroundImage = `url('../../../media/phonocartes/do_bouches/`+ phonId + `.png')` + ',' + `url('../../../media/phonocartes/do_formes/`+ phonId + `.png')`; // Fallback : do_formes
    newCarte.addEventListener("click", function() {
        playPhon(phonId);
      });

    newCarteDiv.appendChild(newCarte);
    targetDiv.appendChild(newCarteDiv);
}

// AUDIO - Son de chaque phonème
function playPhon(phonId) {
    var audio = new Audio('../../../media/audioKinephones/fr_'+phonId+'-f.mp3');
    audio.play();
}

function makeSerieMots(){
    document.getElementById('actionBlock').style.display = "" ;
    var exempleSerie  = {
        "dateCreation" : "",
        "dateModification" : "",
        "nom" : "",
        "code" : "",
        "description" : "",
        "auteur" : "",
        "lang" : "",
        "tag" : "",
        "visibility" : "",

        "panneau" : "",
        "fidel" : "",
        "typeActivite" : "",
        "droitsModif" : "",
    
        "audioDeb" : "",
        "imageDeb" : "",
        "videoDeb" : "",
        "graphieDeb" : "",

        "audioFin" : "",
        "imageFin" : "",
        "videoFin" : "",
        "graphieFin" : "",
        "phonoFin" : "",

        "syntheseVocale" : "",
        "panneauPartiel" : "",
        "phonVisibles" : "",
        "mots" : [
            {
                "motGenerique": "ponte",
                "phono": [
                    ["phon_p", "phon_o_maj_nas", "phon_t"]
                ],
            
                "audioDeb" : "filename.ext",
                "imageDeb" : "filename.ext",
                "videoDeb" : "filename.ext",
                "graphieDeb" : "text",
            
                "audioFin" : "filename.ext",
                "imageFin" : "filename.ext",
                "videoFin" : "filename.ext",
                "graphieFin" : "text",
            
                "phonographies": [
                     [["phon_f","f"],["phon_u","ou"],["phon_r_maj","r"],["phon_s_maj","ch"],["phon_e_maj","e"],["phon_t","tte"]]
                ]
            }],
    }

    var newPhonVisibles = []
    var listApi = document.getElementById('listePhonemesVisibles').value.replace(/\s/g, '').split(',');
    for (api of listApi) {
        for (let phon in phon2api) {
            if (phon2api[phon] == api) newPhonVisibles.push(phon)
        }
    }

    var thisSerie  = {
        "nom" : document.getElementById('nom').value,
        "code" : document.getElementById('code').value,
        "description" : document.getElementById('description').value,
        "auteur" : document.getElementById('auteur').value,
        "dateCreation": document.getElementById('dateCreation').value,
        "visibility": document.getElementById('visibility').value,
        "lang": document.getElementById('langue').value,

        "panneau" : document.getElementById('panneau').value,
        "fidel" : "",
        "typeActivite" : "",
        "droitsModif" : "",
    
        "audioDeb" : document.getElementById('cbAudioDeb').checked ? "1" : "0",
        "imageDeb" : document.getElementById('cbImageDeb').checked ? "1" : "0",
        "videoDeb" : document.getElementById('cbVideoDeb').checked ? "1" : "0",
        "graphieDeb" : document.getElementById('cbGraphieDeb').checked ? "1" : "0",

        "audioFin" : document.getElementById('cbAudioFin').checked ? "1" : "0",
        "imageFin" : document.getElementById('cbImageFin').checked ? "1" : "0",
        "videoFin" : document.getElementById('cbVideoFin').checked ? "1" : "0",
        "graphieFin" : document.getElementById('cbGraphieFin').checked ? "1" : "0",
        "phonoFin" : document.getElementById('cbPhonoFin').checked ? "1" : "0",

        "syntheseVocale" : document.getElementById('cbSyntheseVocale').checked ? "1" : "0",
        "panneauPartiel" : document.getElementById('cbPanneauPartiel').checked ? "1" : "0",
        "phonVisibles" : JSON.stringify(newPhonVisibles),
        "mots" : [],
    }

    // Parcours de chaque mot pour récupérer les médias, les transcriptions possibles etc.
    var listeDivMots = document.getElementsByClassName('divMot');
    for (i=0; i<listeDivMots.length; i++){

        var phono = []; // contiendra la liste des transcriptions possibles pour le mot courant

        ///// Récupération de la liste des transcriptions possibles (si submot checked, puis toutes combinaisons possible entre les submots)
        var entreeTable = []; // contiendra la liste des submots avec leurs phono sélectionnées (pour générer toutes les combi à la fin)
        var liste_submots = document.querySelectorAll('.divMotPhono')[i].children;

        // POUR CHAQUE MOT DE CETTE DIVMOT
        for(j=0; j<liste_submots.length; j++){

            entreeTable.push([]); // création d'un nouveau submot
            
            var liste_phono = liste_submots[j].children[0].children;

            // POUR CHAQUE PHONO DE CE MOT
            for(k=0; k<liste_phono.length; k++){
                
                // SI CETTE PHONO EST SELECTIONNÉE (checked)
                if(liste_phono[k].firstChild.firstChild.checked){
                    var phonsText = liste_phono[k].children[2].innerText.split(',');
                    entreeTable[entreeTable.length-1].push(phonsText);
                }
            }

            // Suppression du dernier mot si celui-ci est vide (que des phon_echec ou phon_neutre)
            if(entreeTable[entreeTable.length-1].length==0){
                entreeTable.pop();
            }

        }
        
        // GENERATION DE TOUTES LES COMBINAISONS POSSIBLE POUR CETTE DIVMOT
        // ex. "le chat" → l,@,S,a ; l,@,tS,a,t
        var ancienneListe = []; // contiendra toutes les combinaisons des mots précédents entre eux et seront combinés à chaque phono du mot courant
        var nouvelleListe = []; // à chaque nouveau mot, la nouvelleListe est passée à l'ancienneListe (mémoire de un mot)

        for(x=0; x<entreeTable.length; x++){ // itération sur les submots de entreeTable (l@, Sa/tSat)

            ancienneListe = nouvelleListe; // on bascule la liste vers la mémoire
            nouvelleListe = []

            if(ancienneListe.length == 0){ // si la liste des précombinaisons est vide (c'est donc le premier submot)
                for(yphono=0; yphono<entreeTable[x].length; yphono++){ // itération sur les phonos du submot courant
                    nouvelleListe.push(entreeTable[x][yphono]);
                }

            } else {
                for(yphono=0; yphono<entreeTable[x].length; yphono++){ // itération sur les phonos du submot courant
                    for(zprecombi=0; zprecombi<ancienneListe.length; zprecombi++){ // itération sur les précombinaisons déjà constituées
                        nouvelleListe.push(ancienneListe[zprecombi].concat(entreeTable[x][yphono]));
                    }
                }
            }
        }
        
        phono = nouvelleListe;
        
        let xa=listeDivMots[i].querySelector('.divMotCellAudioDebLink')?.innerHTML;
        let xb=listeDivMots[i].querySelector('.divMotCellImageDebLink')?.innerHTML;
        let xc=listeDivMots[i].querySelector('.divMotCellVideoDebLink')?.innerHTML;
        let xd=listeDivMots[i].querySelector('.divMotCellAudioFinLink')?.innerHTML;
        let xe=listeDivMots[i].querySelector('.divMotCellImageFinLink')?.innerHTML;
        let xf=listeDivMots[i].querySelector('.divMotCellVideoFinLink')?.innerHTML;
        let xg=listeDivMots[i].querySelector('.textMediaDiv')?.firstChild.innerHTML;
        
        var thisMot = {
            "motGenerique": document.querySelectorAll('.divMotCellMot')[i].innerText,
            "phono": phono,
        
            "audioDeb" : xa!=undefined ? xa : "",
            "imageDeb" : xb!=undefined ? xb : "" ,
            "videoDeb" : xc!=undefined ? xc : "" ,
            "graphieDeb" : "",
        
            "audioFin" : xd!=undefined ? xd : "" ,
            "imageFin" : xe!=undefined ? xe : "" ,
            "videoFin" : xf!=undefined ? xf : "" ,
            "graphieFin" : xg!=undefined ? xg : "" ,
        
            "phonographies": []
        }
        
        thisSerie["mots"].push(thisMot);
    }
    console.log(thisSerie);
    saveSerie(thisSerie);
}

async function saveSerie(thisSerie, confirm=false) {
	// ON EMBALLE TOUT ÇA
    var colis = {
		serie: thisSerie,
		serieOrigin: serie["code"],
        updateExisting: false
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('/player/saveSerie/', options);
    const data = await response.json();
    console.log(data);
    document.getElementById('actionBlock').style.display = "none" ;
    if (data.saved) window.alert("La série a bien été enregistrée !")
    else window.alert("Impossible d'enregistrer la série !!")
}


// UPLOAD FILES WITH JQUERY
// https://simpleisbetterthancomplex.com/tutorial/2016/11/22/django-multiple-file-upload-using-ajax.html
function uploadAudio(media){
    $("#audiofileupload").click();
    /* INITIALIZE THE FILE UPLOAD COMPONENT */
    $("#audiofileupload").fileupload({
        dataType: 'json',
        done: function (e, data) {  /* 3. PROCESS THE RESPONSE FROM THE SERVER */
        console.log(data);
        
        confirmSelectionMedia(data.result.chemins[0].split("/")[3])
        }
    });

    //$("#fileupload-"+target).click();
};

function uploadImage(media){
    $("#imagefileupload").click();
    /* INITIALIZE THE FILE UPLOAD COMPONENT */
    $("#imagefileupload").fileupload({
        dataType: 'json',
        done: function (e, data) {  /* 3. PROCESS THE RESPONSE FROM THE SERVER */
        console.log(data);
        
        confirmSelectionMedia(data.result.chemins[0].split("/")[3])
        }
    });

    //$("#fileupload-"+target).click();
};
  


function play(el,typ) {
    if (typ == "audio") {
        var audio = new Audio("../../media/audio-uploads/"+el.innerText);
        audio.play();
    } else if (typ == "image") {

    }
}


function delSerie() {
    
    var myModal = new bootstrap.Modal(document.getElementById('supprModal'))
    document.getElementById("serieCode").innerHTML = serie.nom; 
    myModal.show()
    
    // if (window.confirm("Êtes-vous sûr⋅e de vouloir supprimer définitivement la série "+serie.nom+" ?")) {
    //     sudoDelSerie(serie.code);
    // }
}

async function sudoDelSerie(serieCode) {

    console.log("suppression!",serieCode);

	// ON EMBALLE TOUT ÇA
    var colis = {
		serieCode: serieCode
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('/_delSerie/', options); 
    const data = await response.json();
    console.log(data);
    if (data.deleted) {
        if (window.confirm("La série a bien été supprimée.")) window.location = '/player/';
    } else {
        window.alert('Impossible de supprimer la série !')
    }
}

function goBack() {
    // if (String(window.location).endsWith('/createSerie/')) window.location = "/player/";
    // else window.history.back();
    window.location = "/player/"
}


function showWysiwygModal(mediaType, i=0) {
    // mediaType = "graphieFin"
    // i correspond à l'id de la div mot cible
    if (document.getElementById("divMotGraphieFinPlein-"+i) != null) {
        document.querySelectorAll('.fr-view')[0].innerHTML = document.getElementById("divMotGraphieFinPlein-"+i).firstChild.innerHTML;
    } else {  document.querySelectorAll('.fr-view')[0].innerHTML = "" }
    document.getElementById('wyMedia').innerHTML = mediaType;
    document.getElementById('wyI').innerHTML = i;
    
    var varElements = document.querySelectorAll("*");
    for(let varElement of varElements) { 
        if(varElement.style['z-index'] == 9999) {
            varElement.style.display = 'none';
        }
    }
    document.querySelectorAll('.fr-toolbar')[0].style.backgroundColor = "lightgray"; 

    var myModal = new bootstrap.Modal(document.getElementById('wysiwygModal'))
    myModal.show()
}

function enregistrerTexte() {
    var mediaType = document.getElementById('wyMedia').innerHTML;
    var i = document.getElementById('wyI').innerHTML;
    linkMedia(i,mediaType, document.querySelectorAll('.fr-view')[0].innerHTML)
    document.getElementById('wyAnnuler').click();
}

function getPhonList() {
    var cartes = document.querySelectorAll('.noTextClip');
    var phonList = [];
    cartes.forEach( (carte) => {
        carte.classList.forEach( (c) => { if(c.match(/phon_.*/) && !phonList.includes(phon2api[c])) phonList.push(phon2api[c]) } );
    });

    document.getElementById('listePhonemesVisibles').value = phonList.join(', ');
}

function removeFile(filePath,mediaType) {
    var fileName = filePath.replace(/.*-uploads\//,'')
    if (window.confirm(`Êtes-vous sûr⋅e de vouloir supprimer ${fileName} ?`)) {
        sendRemoveOrder(filePath,mediaType)
    }
}

async function sendRemoveOrder(filePath,mediaType) {
	// ON EMBALLE TOUT ÇA
    var colis = {
		"filePath":filePath,
        "mediaType":mediaType
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('/_delFile/', options); 
    const data = await response.json();
    console.log(data);
    document.getElementById('reloadFileList').click();
}