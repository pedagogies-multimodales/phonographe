$(document).ready( function () {
    // Formattage du tableau
    // https://datatables.net/manual/options
    
    getSeries();
} );

async function getSeries() {
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    
    // ENVOI
	const response = await fetch('/player/getSeries/', options);
    const data = await response.json();
    var series = JSON.parse(data.series);
    var users = JSON.parse(data.users);
    console.log(series);
    console.log(users);

    cptPublic = 0; // nb of public series
    cptMy = 0; // nb of series made by myself or for which I have edition rights

    var myusers = {};
    for (i=0; i<users.length; i++){
        myusers[users[i].pk] = users[i].fields.username;
    }

    for (i=0; i<series.length; i++){
        if (series[i].fields.visibility == "public" || series[i].fields.visibility == null) {
            cptPublic++;
            document.getElementById("tbody").innerHTML += `
            <tr style="cursor: pointer;" onclick="getModalSerieInfo('${series[i].fields.code}')">
                <td class="hide">${series[i].fields.code}</td>
                <td>${series[i].fields.lang}</td>
                <th scope="row">${series[i].fields.nom}</th>
                <td>${ myusers[series[i].fields.auteur] }</td>
                <td class="serieDescr">${series[i].fields.description}</a></td>
                <td>${ formatDate(series[i].fields.dateCreation) }</td>
                <td>${ formatDate(series[i].fields.dateModification) }</td>
            </tr>`
            // <img class="rounded-circle serieAuteurImage" src="${ users[series[i].fields.auteur] }"> 
        }
        if (myusers[series[i].fields.auteur] == userInfo.nom) {
            cptMy++;
            if (series[i].fields.visibility == "public" || series[i].fields.visibility == null) {
                serieVisibility = `<span class="badge rounded-pill bg-success">public</span>`
            } else {
                serieVisibility = `<span class="badge rounded-pill bg-secondary">private</span>`
            }
            document.getElementById("tbodyMy").innerHTML += `
            <tr style="cursor: pointer;" onclick="getModalSerieInfo('${series[i].fields.code}')">
                <td>${ serieVisibility }</td>
                <td class="hide">${series[i].fields.code}</t>
                <td>${series[i].fields.lang}</td>
                <th scope="row">${series[i].fields.nom}</th>
                <td>${ myusers[series[i].fields.auteur] }</td>
                <td class="serieDescr">${series[i].fields.description}</a></td>
                <td>${ formatDate(series[i].fields.dateCreation) }</td>
                <td>${ formatDate(series[i].fields.dateModification) }</td>
            </tr>`
        }
        
    }

    document.getElementById('nbSeriesPub').innerHTML = cptPublic;
    document.getElementById('nbSeriesMy').innerHTML = cptMy;

    $('#loader').hide();
    $('#seriesTable').DataTable();
    $('#loaderMy').hide();
    if (userInfo.nom != "") {
        $('#seriesTableMy').DataTable();
    } else {
        document.getElementById('btnConnexionSeriesMy').style.display = "block";
    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('/')+" "+d.getHours()+":"+d.getMinutes();
}

//////////////////////////////////////
////////// PARAMÉTRAGE POPUP /////////
////////// POUR COPIER TEXTE /////////
//////////////////////////////////////
// Get the modal
var modal = document.getElementById("modalSerieInfo")

// Fonction pour ouvrir le PopUp 
function getModalSerieInfo(code) {
    document.getElementById('loaderTestWindow').style.display = "";
    document.getElementById('modalBtnPlay').href = "";
    document.getElementById('modalBtnPlay').style.display = "none";
    document.getElementById('modalBtnEdit').href = "";
    document.getElementById('modalBtnEdit').style.display = "none";
    document.getElementById('modalBtnCopy').href = "";
    document.getElementById('modalBtnCopy').style.display = "none";
    getSerieInfo(code);
}
function closeModalSerieInfo() {
    modal.style.display = "none"
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none"
    }
}


async function getSerieInfo(code) {
    // Afficher la modal initialisée
    document.getElementById('modalSerieInfoNom').innerHTML = "...";
    document.getElementById('modalDescription').innerHTML = "...";
    document.getElementById('modalAuteurIm').src = "...";
    document.getElementById('modalAuteur').innerHTML = "...";
    document.getElementById('modalDateCreation').innerHTML = "...";
    document.getElementById('modalDateModification').innerHTML = "...";
    document.getElementById('modalCode').innerHTML = "...";
    document.getElementById('modalUrl').innerHTML = "...";
    document.getElementById('modalBtnPlay').style.display = "none";
    document.getElementById('modalBtnEdit').style.display = "none";
    document.getElementById('modalBtnCopy').style.display = "none";
    document.getElementById('loaderTestWindow').style.display = "";
    

    modal.style.display = "block";
    // Paramètres d'envoi
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    // ENVOI
	const response = await fetch('/player/serie-'+code+'/info', options);
    const data = await response.json();
    console.log(data);
    document.getElementById('modalSerieInfoNom').innerHTML = data.serieInfo.nom;
    document.getElementById('modalDescription').innerHTML = data.serieInfo.description;
    document.getElementById('modalAuteurIm').src = data.serieInfo.auteurIm;
    document.getElementById('modalAuteur').innerHTML = data.serieInfo.auteur;
    document.getElementById('modalDateCreation').innerHTML = data.serieInfo.dateCreation;
    document.getElementById('modalDateModification').innerHTML = data.serieInfo.dateModification;
    document.getElementById('modalCode').innerHTML = data.serieInfo.code;
    document.getElementById('modalUrl').innerHTML = "https://phonographe.alem-app.fr/player/serie-"+ data.serieInfo.code +"/play";
    document.getElementById('modalBtnPlay').style.display = "";
    document.getElementById('loaderTestWindow').style.display = "none";
    document.getElementById('modalBtnPlay').href = window.location.origin + "/player/serie-"+ data.serieInfo.code +"/play";
    if (userInfo.superadmin || userInfo.nom == data.serieInfo.auteur) {
        document.getElementById('modalBtnEdit').href = window.location.origin + "/player/serie-"+ data.serieInfo.code +"/edit";
        document.getElementById('modalBtnEdit').style.display = "block";
    }
    if (userInfo.admin) {
        document.getElementById('modalBtnCopy').href = window.location.origin + "/player/serie-"+ data.serieInfo.code +"/copy";
        document.getElementById('modalBtnCopy').style.display = "block";
    }
}


// Copie de l'url de la série dans presse-papier
function copyUrl() {
    var copyText = document.getElementById("modalUrl").innerText;
    navigator.clipboard.writeText(copyText);
    document.getElementById('modalCopyBtn').innerHTML = "Copié !";
  } 
// On réinitialise le bouton "copier"
function endCopy() {
    setTimeout(() => {document.getElementById('modalCopyBtn').innerHTML = "Copier"}, 8000);
}