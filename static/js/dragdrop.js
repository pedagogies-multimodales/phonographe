// VARIABLES DISPONIBLES
// var cartes -> liste des cartes disponibles ; mis à jour à chaque modification de rep
// const rep -> div d'écriture

// https://www.youtube.com/watch?v=jfYWwQrtzzY&t=102s
// ajouter DragDropTouch.js pour compatibilité tactile

if (thisAppli == "phonographe") {
    targ = document.getElementById('textZone')
} else {
    targ = document.getElementById(ligneCible)
}

targ.addEventListener('dragover', e => {
    // mise à jour de la ligne cible
    if (thisAppli == "phonoplayer") {
        targ = document.getElementById(ligneCible)
    }

    e.preventDefault()
    const afterElement = getDragAfterElement(e.clientX)
    
    const carte = document.querySelector('.dragging')
    if (afterElement == null) {
        targ.append(carte)
    } else {
        targ.insertBefore(carte, afterElement)
    }
})

function getDragAfterElement(x) {
    const draggableElements = [...targ.querySelectorAll('.carte:not(.dragging)')]
    return draggableElements.reduce((closest, child) => {
        const box = child.getBoundingClientRect()
        const offset = x - box.left - box.width / 2
        if (offset < 0 && offset > closest.offset) {
            return { offset: offset, element: child }
        } else {
            return closest
        }
    }, { offset: Number.NEGATIVE_INFINITY }).element
}