// Fonctions utilisées par le player automatique (suggestion de mots aléatoires)
var cheat = []
var urls = []
var phono = []

// Prevent from reloading page when submitting this form
var form = document.getElementById("tryWordForm");
function handleForm(event) { event.preventDefault(); } 
form.addEventListener('submit', handleForm);



var showText = false;
function toggleText() {
    showText = !showText;
    if (showText) {
        document.getElementById('divTextMot').style.display = ''
        document.getElementById('eyeX').style.display = ''
        document.getElementById('eyeO').style.display = 'none'
    } else {
        document.getElementById('divTextMot').style.display = 'none'
        document.getElementById('eyeX').style.display = 'none'
        document.getElementById('eyeO').style.display = ''
    }
}


// Get the modal Liste des réponses
var modalRep = document.getElementById("modalRep")

// Fonction pour ouvrir le PopUp 
function getModalRep() {
    document.getElementById('repList').innerHTML = ""
    // document.getElementById('repListDico').href = `https://wikicolor.alem-app.fr/dicoSearch/?dicoLang=${thisPageLang}&mot=${mots[mots.length-1].motGenerique}&trans=&motCond=equalsTo&transCond=includes`
    var reps = []
    var rx = 0
    phono[0].forEach((r)=>{
        if (!reps.includes(r.join(', '))) {
            rx ++
            reps.push(r.join(', '))
            r.forEach((phon)=>{
                document.getElementById('repList').innerHTML += `<div class="d-flex align-items-center" id="rep${rx}"></div>`
                insertStaticCard(phon, document.getElementById('rep'+rx))
            })
        }
    })
    modalRep.style.display = "block"
}
function closeModalRep() {
    modalRep.style.display = "none"
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modalRep) {
        modalRep.style.display = "none"
    }
}


function insertStaticCard(phon,targetDiv) {
    var str1 = false
    var str2 = false
    var phonId = phon.split(' ')[0]
    if (phon.split(' ')[1]=='stress1') str1 = true
    else if (phon.split(' ')[1]=='stress2') str2 = true

    var newCarte = document.createElement('div')
    newCarte.title = `[${phon2api[phon.split(' ')[0]]}] ${phon}`
    newCarte.style.cursor = "normal"
    newCarte.classList = "carte " + phonId + " noTextClip";// on garde rectId, pour connaître le phonème mais sans appeler les styles de phonochromie-alem.css
    if (str1) {
        newCarte.classList.add('stress1')
    } else if (str2) {
        newCarte.classList.add('stress2')
    }

    if (phonId.includes('phon_schw')) newCarte.classList.add('schwa')

    if (!phon.match(/espace/)) { 
        var newCarteIn = document.createElement('div')
        newCarteIn.classList = "carteIn"
        newCarteIn.style.backgroundImage = `url('../../../media/phonocartes/do_${famille}/${phonId}.png')` //+ `, url('../../../media/phonocartes/do_formes/${phonId}.png')`; //"url('/static/cartes/"+ famille +"/"+ phonId + ".jpg')"}
        newCarte.appendChild(newCarteIn)
    } 

    targetDiv.appendChild(newCarte)
}

var alemAudioList = getAlemAudioList()

async function getAlemAudioList() {
	// récupère une liste de mots dont l'enregistrement audio est disponible sur le serveur ALEM
    var colis = {
		lang: thisPageLang
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('/player/getAlemAudioList/', options);
    const data = await response.json();
    alemAudioList = data["alemAudioList"]
}

/////////////////////////////////////
/////////////////////////////////////

function getRandomWord() {
    var randomWord = "";
    if (thisPageLang == "en") {
        randomWord = lexiqueEn[Math.floor(Math.random() * lexiqueEn.length)]; // FULLY RANDOM
    } else { 
        randomWord = lexiqueFr[Math.floor(Math.random() * lexiqueFr.length)]; // FULLY RANDOM
    }
    console.log("Get random word:",randomWord);
    
    return randomWord
}


async function loadWord({mot="", random=false} = {}) {
    console.log("loadWord()",mot,random)
    document.getElementById('tryAWord').value = "";

    var urlsOK = false
    var phonoOK = false

    if (random) mot = await selectBestFollowing()
    
    while (!urlsOK || !phonoOK) {
        document.getElementById('divTextMot').children[0].innerText = mot;
        console.log("Tentative get(urls,phono)", mot)
        
        if (alemAudioList.includes(mot)) {
            await getAlemAudio(mot).then((result) => {
                urls = result['urls'];
                if (urls.length>0) {
                    urlsOK = true
                }
            })
        } else {
            await getWikiAudio(mot).then((result) => {
                urls = result['urls'];
                if (urls.length>0) {
                    urlsOK = true
                }
            })
        }

        await getPhono(mot).then(result => {
            phono = result['outText'];

            // if original phons transcription isn't included in phono, add it
            if (!phono.includes(myHistory[myHistory.length-1].phons)) {
                phono[0].push(myHistory[myHistory.length-1].phons)
                phonoOK = true
            }

            if (phono[0][0][0] != "phon_inconnu") {
                phonoOK = true
            }
        })

        if (!urlsOK || !phonoOK) {
            myHistory[myHistory.length-1].bug = true
            mot = await selectBestFollowing()
        }
    }

    formatReponses(phono).then(formattedPhono => {
        console.log('RESULT',formattedPhono);
        if (formattedPhono && formattedPhono[0].length>0) {
            
            mots[0].motGenerique = mot
            mots[0].phono = formattedPhono[0]
            mots[0].audioDeb = urls
            
            if (!myHistory[myHistory.length-1].words.includes(mot)) {
                // current word was entered manually
                myHistory.push({
                    api: formattedPhono[0][0].map(x => phon2api[x.replace(" stress1","").replace(" stress2","").replace(" schwa","")]),
                    words: [mot],
                    phons: formattedPhono[0][0],
                    score: 1,
                    errorAt: [],
                    bug: false
                })
            }
            
            cptitem = 0
            makePage()
        } else {
            loadWord({random:true})
        }
    })
}


// prépare la page (équivalent de loadNext() avec des bricoles en plus pour le mode automatique)
function makePage() {
    document.getElementById("bravo").style.display = "none"
    
    saveTrace(`next ${mots[cptitem]["motGenerique"]}`)
    rep.innerHTML = textZoneDefault
    curseurPos = 'g0'

    document.querySelectorAll('.altAudio').forEach((el)=> document.getElementById('btnAudio').removeChild(el))
    
    document.getElementById('btnAudio').innerHTML = ""

    for (i=0;i<urls.length;i++) {
       document.getElementById('btnAudio').innerHTML += `<button onclick="playAudioId(${i})" type="button" class="btn btn-outline-primary btn-lg btnAudio" style="position:relative">
        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" style="background-color:transparent" class="bi bi-volume-up-fill my-1" viewBox="0 0 16 16">
            <path d="M11.536 14.01A8.473 8.473 0 0 0 14.026 8a8.473 8.473 0 0 0-2.49-6.01l-.708.707A7.476 7.476 0 0 1 13.025 8c0 2.071-.84 3.946-2.197 5.303l.708.707z"/>
            <path d="M10.121 12.596A6.48 6.48 0 0 0 12.025 8a6.48 6.48 0 0 0-1.904-4.596l-.707.707A5.483 5.483 0 0 1 11.025 8a5.483 5.483 0 0 1-1.61 3.89l.706.706z"/>
            <path d="M8.707 11.182A4.486 4.486 0 0 0 10.025 8a4.486 4.486 0 0 0-1.318-3.182L8 5.525A3.489 3.489 0 0 1 9.025 8 3.49 3.49 0 0 1 8 10.475l.707.707zM6.717 3.55A.5.5 0 0 1 7 4v8a.5.5 0 0 1-.812.39L3.825 10.5H1.5A.5.5 0 0 1 1 10V6a.5.5 0 0 1 .5-.5h2.325l2.363-1.89a.5.5 0 0 1 .529-.06z"/>
          </svg>
          <div style="position:absolute; bottom:0px; right:0px">${urls[i][0]}</div>
    </button>`
    }
    
    var ukOK = false;
    for (url of urls) {
        if (!ukOK && ["uk","gb","rp"].includes(url[0])) {
            currentAudio = url[1]
            playAudio()
            ukOK = true
        }
    }
    if (!ukOK) {
        currentAudio = urls[0][1]
        playAudio()
    }

    cptitem = cptitem+1
    document.getElementById('cptitem').innerHTML = cptitem
}



// REQUEST WIKICOLOR FOR TRANSCRIPTION OF WORD
async function getPhono(mot) {
	// OUTPUT: "outText": liste(mots) de liste(phonologies) de liste(phonemes) [+stress1,stress2,schwa]
    
    var colis = {
		inText: mot,
		lang: thisPageLang
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('https://wikicolor.alem-app.fr/getPhonoOf/', options); //'http://127.0.0.1:9000/getPhonoOf/', options); //
    const data = await response.json();
    console.log(data["outText"])
    return data
}

// GET THE AUDIO FILE FROM ALEM
async function getAlemAudio(mot) {
    // Si on a l'enregistrement du mot sur le serveur, on le récupère directement
    // OUTPUT: urls: [ ["uk","https://...mot.mp3"], ["us", "https://...mot.mp3"], ...]

    var colis = {
		inText: mot,
		lang: thisPageLang
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('/player/getAlemAudio/', options);
    const data = await response.json();
    console.log(data);
    return data
}

var lang2wikiCode = {
    "en": ["LL-Q1860", "eng"],
    "fr": ["LL-Q150", "fra"],
    "de": ["LL-Q188", "deu"],
    "spanish": ["LL-Q1321", "spa"],
    "italian": ["LL-Q652", "ita"],
    "portuguese": ["LL-Q5146", "por"],
    "russian": ["LL-Q7737", "rus"],
    "chinese": ["LL-Q7850", "zho"],
    "japanese": ["LL-Q5287", "jpn"],
    "korean": ["LL-Q9176", "kor"],
    "arabic": ["LL-Q13955", "ara"],
    "hindi": ["LL-Q1568", "hin"],
    "dutch": ["LL-Q7411", "nld"],
    "swedish": ["LL-Q9027", "swe"],
    "polish": ["LL-Q809", "pol"],
    "turkish": ["LL-Q5895", "tur"],
    "vietnamese": ["LL-Q10853", "vie"],
    "ukrainian": ["LL-Q41939", "ukr"],
    "greek": ["LL-Q3104", "ell"],
    "hebrew": ["LL-Q808", "heb"]
  }

// GET THE AUDIO FILE FROM THE WIKIMEDIA
async function getWikiAudio(mot) {
    // OUTPUT: urls: [ ["en","https://...mot.wav"], ["en", "https://...mot.wav"], ...]
    var data = { urls: [] }

    var [langCode1, langCode2] = lang2wikiCode[thisPageLang]
    const qurl = `https://commons.wikimedia.org/w/api.php?action=query&format=json&origin=*&prop=imageinfo&generator=search&utf8=1&iiprop=url%7Cextmetadata&iiextmetadatafilter=LicenseShortName&gsrsearch=intitle:/${langCode1}+%5C%28${langCode2}%5C%29-%5B%5E-%5D*-${mot}\.wav/&gsrnamespace=6&gsrlimit=3&gsrwhat=text`
    
    console.log(qurl)
    // ENVOI
	const response = await fetch(qurl);
    const rep = await response.json();
    if("query" in rep) {
        const pages = rep.query.pages;
        // Extraction des informations pour chaque page
        for (var page in pages){
            const url = pages[page].imageinfo[0].url;
            data['urls'].push([thisPageLang, url]);
        };
    }
    
    console.log(data);
    return data
}



// Format les phono de WikiColor pour les rendre compatibles (bicolores, tolérances etc.)
async function formatReponses(phono) {
    phono[0] = phono[0].filter(function(value, index, arr){ 
        return value[0] != "phon_echec";
    });

    console.log('PrePhono', phono)

    ////////////////////////////////////
    // REFORMATAGE phono POUR ks kv el en... etc.  
    // également phon_wa w+a...
    phono[0].forEach((el)=>{

        var newEl = []
        for (i=0; i<el.length; i++) {
            var xphon = el[i].split(' ')[0];
            var xstress = el[i].split(' ')[1] || '';
            if (xstress != '') xstress = ' '+xstress

            if (xphon=="phon_a_majr"){ newEl.push("phon_a_maj"); newEl.push("phon_r_slash"+xstress);}
            else if (xphon=="phon_ai_majarobase" || xphon=="phon_ai_majschwa"+xstress){ newEl.push("phon_a"); newEl.push("phon_i_maj"); newEl.push("phon_schwa schwa");}
            else if (xphon=="phon_arobasel" || xphon=="phon_schwal"){ newEl.push("phon_schwa schwa"); newEl.push("phon_l");}
            else if (xphon=="phon_arobasem" || xphon=="phon_schwam"){ newEl.push("phon_schwa schwa"); newEl.push("phon_m");}
            else if (xphon=="phon_arobasen" || xphon=="phon_schwan"){ newEl.push("phon_schwa schwa"); newEl.push("phon_n");}
            else if (xphon=="phon_dz_maj"){ newEl.push("phon_d"); newEl.push("phon_z_maj");}
            else if (xphon=="phon_dz_slash"){ newEl.push("phon_d"); newEl.push("phon_z_slash");}
            else if (xphon=="phon_dz"){ newEl.push("phon_d"); newEl.push("phon_z");}
            else if (xphon=="phon_ef"){ newEl.push("phon_e"); newEl.push("phon_f"+xstress);}
            else if (xphon=="phon_er"){ newEl.push("phon_e"); newEl.push("phon_r_slash"+xstress);}
            else if (xphon=="phon_gz_maj"){ newEl.push("phon_g"); newEl.push("phon_z_maj");}
            else if (xphon=="phon_gz"){ newEl.push("phon_g"); newEl.push("phon_z")}
            else if (xphon=="phon_h_maj_i"){ newEl.push("phon_h_maj"); newEl.push("phon_i"+xstress);}
            else if (xphon=="phon_i_majr"){ newEl.push("phon_i_maj"); newEl.push("phon_r_slash"+xstress);}
            else if (xphon=="phon_j_maj"){ newEl.push("phon_n"); newEl.push("phon_j");}
            else if (xphon=="phon_jarobase" || xphon=="phon_jschwa"){ newEl.push("phon_j"); newEl.push("phon_schwa schwa");}
            else if (xphon=="phon_ju_long"){ newEl.push("phon_j"); newEl.push("phon_u_long"+xstress)}
            else if (xphon=="phon_ju_maj"){ newEl.push("phon_j"); newEl.push("phon_u_maj"+xstress);}
            else if (xphon=="phon_ju_majarobase" || xphon=="phon_ju_majschwa"){ newEl.push("phon_j"); newEl.push("phon_u_maj"+xstress); newEl.push("phon_schwa schwa");}
            else if (xphon=="phon_ju_majr"){ newEl.push("phon_j"); newEl.push("phon_u_maj"+xstress); newEl.push("phon_r_slash");}
            else if (xphon=="phon_ju"){ newEl.push("phon_j"); newEl.push("phon_u"+xstress);}
            else if (xphon=="phon_ks_maj"){ newEl.push("phon_k"); newEl.push("phon_s_maj");}
            else if (xphon=="phon_ks"){ newEl.push("phon_k"); newEl.push("phon_s")}
            else if (xphon=="phon_kv"){ newEl.push("phon_k"); newEl.push("phon_v")}
            else if (xphon=="phon_kw"){ newEl.push("phon_k"); newEl.push("phon_w")}
            else if (xphon=="phon_lj"){ newEl.push("phon_l"); newEl.push("phon_j");}
            else if (xphon=="phon_nj"){ newEl.push("phon_n"); newEl.push("phon_j");}
            else if (xphon=="phon_o_majr"){ newEl.push("phon_o_maj"); newEl.push("phon_r_slash"+xstress);}
            else if (xphon=="phon_schwaz"){ newEl.push("phon_schwa schwa"); newEl.push("phon_z");}
            else if (xphon=="phon_schwiz"){ newEl.push("phon_schwi schwa"); newEl.push("phon_z");}
            else if (xphon=="phon_schwju" || xphon=="phon_jschwu"){ newEl.push("phon_j"); newEl.push("phon_schwu schwa");}
            else if (xphon=="phon_sz"){ newEl.push("phon_s"); newEl.push("phon_z");}
            else if (xphon=="phon_ts_maj"){ newEl.push("phon_t"); newEl.push("phon_s_maj");}
            else if (xphon=="phon_ts"){ newEl.push("phon_t"); newEl.push("phon_s");}
            else if (xphon=="phon_tt_maj"){ newEl.push("phon_t"); newEl.push("phon_t_maj");}
            else if (xphon=="phon_u_majr"){ newEl.push("phon_u_maj"); newEl.push("phon_r_slash"+xstress);}
            else if (xphon=="phon_wa"){ newEl.push("phon_w"); newEl.push("phon_a"+xstress);}
            else if (xphon=="phon_wa_maj"){ newEl.push("phon_w"); newEl.push("phon_a_maj"+xstress);}
            else if (xphon=="phon_wai_maj" || xphon=="phon_waschwi"){ newEl.push("phon_w"); newEl.push("phon_a"+xstress); newEl.push("phon_schwi");}
            else if (xphon=="phon_war"){ newEl.push("phon_w"); newEl.push("phon_a"+xstress); newEl.push("phon_r_slash");}
            else if (xphon=="phon_we_maj_nas"){ newEl.push("phon_w"); newEl.push("phon_e_maj_nas"+xstress);}
            else if (xphon=="phon_wv_maj"){ newEl.push("phon_w"); newEl.push("phon_v_maj")+xstress;}
            else {
                newEl.push(el[i])
            }
        }
        if (!phono[0].includes(newEl)) phono[0].push(newEl);        
    })

    // TOLÉRANCES
    // e=E, E=e, a=V
    if (thisPageLang=="en") {
        phono[0].forEach((el)=>{
            var newEl = []
            for (i=0; i<el.length; i++) {
                if (el[i]=="phon_e"){
                    newEl.push("phon_e_maj")
                } else if (el[i]=="phon_e_maj"){
                    newEl.push("phon_e")
                } else if (el[i]=="phon_e stress1"){
                    newEl.push("phon_e_maj stress1")
                } else if (el[i]=="phon_e_maj stress1"){
                    newEl.push("phon_e stress1")
                } else if (el[i]=="phon_e stress2"){
                    newEl.push("phon_e_maj stress2")
                } else if (el[i]=="phon_e_maj stress2"){
                    newEl.push("phon_e stress2")
                } else {
                    newEl.push(el[i])
                }
            }
            if (!phono[0].includes(newEl)) phono[0].push(newEl);        
        })
        phono[0].forEach((el)=>{
            var newEl = []
            for (i=0; i<el.length; i++) {
                if (el[i]=="phon_a"){
                    newEl.push("phon_v_maj")
                } else {
                    newEl.push(el[i])
                }
            }
            if (!phono[0].includes(newEl)) phono[0].push(newEl);     
        })
        phono[0].forEach((el)=>{
            var newEl = []
            for (i=0; i<el.length; i++) { 
                if (el[i]=="phon_schwa schwa" && el[i+1]=="phon_n"){
                    newEl.push("phon_n")
                    i++
                } else {
                    newEl.push(el[i])
                }
            }
            if (!phono[0].includes(newEl)) phono[0].push(newEl); 
        })
    } else if (thisPageLang=="fr") {
        phono[0].forEach((el)=>{
            var newEl = []
            for (i=0; i<el.length; i++) {
                if (el[i]=="phon_e"){
                    newEl.push("phon_e_maj")
                } else if (el[i]=="phon_e_maj"){
                    newEl.push("phon_e")
                } else if (el[i]=="phon_o"){
                    newEl.push("phon_o_maj")
                } else if (el[i]=="phon_o_maj"){
                    newEl.push("phon_o")
                } else if (el[i]=="phon_9_nas_maj"){
                    newEl.push("phon_e_nas_maj")
                } else if (el[i]=="phon_a_maj"){
                    newEl.push("phon_a")
                } else {
                    newEl.push(el[i])
                }
            }
            if (!phono[0].includes(newEl)) phono[0].push(newEl);        
        })
    }

    phono = [...new Set(phono)] // remove duplicates
    
    ////////////////////////////////////
    console.log("PHONO",phono)
    return phono
    
}






/////// EN TRAVAUX :
// This request the server for best following word to present based on history and phonology 
var myHistory = [] // will contain all words that have been done. Format : [ ["a","p","i"], ["ortho1","ortho2"], score1/0 ]

async function selectBestFollowing() {

    var colis = {
		myHistory,
        lang : thisPageLang
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('/player/getNextFrom/', options);
    const data = await response.json();
    console.log(data);
    var [api, words] = data["nextItem"]
    
    myHistory.push({
        api,
        words,
        phons: apis2phons(api),
        score: 1,
        errorAt: [],
        bug: false
    })

    return data["nextItem"][1][0]
}

// convert list of IPA to list of phons
function apis2phons(apis) {
    return apis.map((api) => {
        let classe = "";

        if (api.startsWith("ˈ")) {
            classe = " stress1";
            api = api.slice(1); // Remove the leading stress marker
        } else if (api.startsWith("ˌ")) {
            classe = " stress2";
            api = api.slice(1);
        }

        const phon = api2phon[api];

        if (phon.startsWith("phon_schw")) {
            classe = " schwa"; // Override previous stress class
        }

        return phon + classe;
    });
}

// Print myHistory on the page
var myHistoryDiv1 = document.getElementById('myHistoryView1')
var myHistoryDiv0 = document.getElementById('myHistoryView0')
function printHistory() {
    if (myHistory.length>0){
        var newDiv = document.createElement("div")
        newDiv.innerText = myHistory[myHistory.length-1].words.join(", ")
        if (myHistory[myHistory.length-1].score==1) {
            myHistoryDiv1.appendChild(newDiv)
            myHistoryDiv1.scrollTop = myHistoryDiv1.scrollHeight;
        } else {
            myHistoryDiv0.appendChild(newDiv)
            myHistoryDiv0.scrollTop = myHistoryDiv0.scrollHeight;
        }
        
    }

    // Display statistics
    let nrep1 = myHistory.filter(item => item.score == 1).length
    let nrep0 = myHistory.filter(item => item.score == 0).length
    let nrep = myHistory.length
    document.getElementById("repStats1").innerHTML = `(${nrep1}/${nrep}, ${Math.round(nrep1 / nrep *100)}%)`
    document.getElementById("repStats0").innerHTML = `(${nrep0}/${nrep}, ${Math.round(nrep0 / nrep *100)}%)`
}
