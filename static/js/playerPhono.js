const thisAppli = "phonoplayer";
const thisURL = window.location.href;
var famille = "";

let stress1ponderator = 2
let stress2ponderator = 1.5

// Set Taille clavier
var clavSize = getComputedStyle(document.documentElement).getPropertyValue('--clavSize');
document.documentElement.style.setProperty('--clavSize', '60%');

// RÉCUPÉRATION DE LA SÉRIE À CHARGER dans templates/playerPhono.html

// PARAMÉTRAGE SÉRIE
var serieId = sc.code;
var serieName = sc.nom;
var random = false // true si série de mots aléatoires
if (["Englishrandom","FrancaisAleatoire"].includes(serieId)) {
    random = true;
    document.getElementById("historyDiv").style.display = "block" // Historique des réponses correctes et incorrectes
    document.getElementById("bravoDivShowRep").style.display = "block" // bouton pour afficher les réponses possibles dans la div Bravo
}

var audioDeb = "audioDeb" in sc ? sc.audioDeb : 0;
var imageDeb = "imageDeb" in sc ? sc.imageDeb : 0;
var videoDeb = "videoDeb" in sc ? sc.videoDeb : 0;
var graphieDeb = "graphieDeb" in sc ? sc.graphieDeb : 0;

var audioFin = "audioFin" in sc ? sc.audioFin : 0;
var imageFin = "imageFin" in sc ? sc.imageFin : 0;
var videoFin = "videoFin" in sc ? sc.videoFin : 0;
var graphieFin = "graphieFin" in sc ? sc.graphieFin : 0;
var phonoFin = "phonoFin" in sc ? sc.phonoFin : 0;
audioFin = 0;

var syntheseVocale = sc.syntheseVocale;
var panneauPartiel = sc.panneauPartiel;
var phonActifs = JSON.parse(sc.phonVisibles);
panneauPartiel = (panneauPartiel==1) ? true : false
syntheseVocale = (syntheseVocale==1) ? true : false

let videoMode = false
let audioMode = false

if (videoDeb==1) {
    videoMode = true
} else {
    audioMode = true
}

if (sc.lang) selectLang(sc.lang)
else selectLang("fr")
function selectLang(lang, p="default", f="default"){
    console.log('SelectLang()',lang)
    var phonolist = document.getElementById('selectPanneau')
    phonolist.innerHTML = ''
    document.getElementById('stressParam').style.display = 'none !important'
    document.querySelectorAll('.paramSerieAuto').forEach((el)=> { el.style.display = 'none !important' })

    if (lang=="fr"){
        var phonoFrDo = document.createElement('option')
        phonoFrDo.value = "phonoFrDo"
        phonoFrDo.innerHTML = "Panneaux Fr A. Do"
        phonolist.appendChild(phonoFrDo)

        famille = "bouches"
        interface("fr")

    } else if (lang=="en"){
        var phonoEnAlem = document.createElement('option')
        phonoEnAlem.value = "phonoEnAlem"
        phonoEnAlem.innerHTML = "ALeM British"
        phonolist.appendChild(phonoEnAlem)

        var phonoEnPronSciBr = document.createElement('option')
        phonoEnPronSciBr.value = "phonoEnPronSciBr"
        phonoEnPronSciBr.innerHTML = "PronSci British"
        phonolist.appendChild(phonoEnPronSciBr)

        famille = "formes"
        document.querySelectorAll('.stressBtn').forEach((el)=> { el.style.display = '' })
        interface("en")
    }
    if (random) document.querySelectorAll('.paramSerieAuto').forEach((el)=> { el.style.display = '' })
    selectPanneau(p)
}

function interface(lang) {
    console.log("Langue d'interface:",lang)
    thisPageLang = lang
    var langspanList = document.getElementsByClassName("langspan")
    if (lang == "en") {
        for (i=0; i<langspanList.length; i++) {
            span = langspanList[i]
            span.innerHTML = langJson[span.id]["en"]
        }

    } else { // "fr" par défaut
        for (i=0; i<langspanList.length; i++) {
            span = langspanList[i]
            span.innerHTML = langJson[span.id]["fr"]
        }
        
    }
}


function selectPanneau(p){
    if (p=="default" && thisPageLang=="fr") {
        p = "phonoFrDo"
    } else if (p=="default" && thisPageLang=="en") {
        p = "phonoEnAlem"
    }
    console.log("selectPanneau",p,thisPageLang)
    var svgEnPronSciBr = document.getElementById('svgEnPronSciBr'); // Panneau EN PronSci

    var svgEnAlem = document.getElementById('svgEnAlem'); // Panneau EN ALeM (fond couleurs)
    var pngCalqEnAlemLignes = document.getElementById('pngCalqEnAlemLignes'); // Panneau EN ALeM (lignes)
    var pngPochoirEnAlem = document.getElementById('pngPochoirEnAlem'); // Panneau EN ALeM (formes bouches)
    var svgClickEnAlem = document.getElementById('svgClickEnAlem'); // Panneau EN ALeM (zones clickables)

    var doCalques = document.getElementById('doCalques'); // boutons calques
    if (p == 'phonoFrDo') {
        svgEnAlem.style.display = 'none'
        pngCalqEnAlemLignes.style.display = 'none'
        pngPochoirEnAlem.style.display = 'none'
        svgClickEnAlem.style.display = 'none'

        doCalques.style.display = 'block'

    } else if (p == 'phonoEnAlem') {
        svgEnAlem.style.display = 'block'
        pngCalqEnAlemLignes.style.display = 'block'
        pngPochoirEnAlem.style.display = 'block'
        svgClickEnAlem.style.display = 'block'

        doCalques.style.display = 'none'
    }
}

/// ACTIONS LORS DU CLIC SUR PHONEME
var cartes = [] // initialisation de la liste des cartes disponible (0 au début)
var ligneCible = "rep"
changeLigne(document.getElementById(ligneCible).parentElement) // par défaut ligne active = rep

function changeLigne(el) {
    let newRep = el.firstElementChild
    console.log('Change ligne to '+ newRep.id)
    ligneCible = newRep.id
    if (ligneCible != 'rep') {
        document.getElementById(curseurPos).classList.remove('startPoint')
        curseurPos = newRep.lastElementChild.previousElementSibling.id
        document.getElementById(curseurPos).classList.add('startPoint')
    }

    document.querySelectorAll('.ligneCible').forEach((divRep) => { divRep.classList.remove('ligneCible')})
    el.classList.add('ligneCible')
    if (bacasable) { saveTrace(`passage ligne ${ligneCible}`) }
}


// // PROSODIE 
// // accent primaire
// var stress1 = false
// function toggleStress1(){
//     if (stress2) toggleStress2()
//     stress1 = !stress1
//     console.log("stress1",stress1)
//     document.getElementById('btnStress1').classList.toggle('btnStressSelected')
// }
// // accent secondaire
// var stress2 = false
// function toggleStress2(){
//     if (stress1) toggleStress1()
//     stress2 = !stress2
//     console.log("stress2",stress2)
//     document.getElementById('btnStress2').classList.toggle('btnStressSelected')
// }

// Conteneur par défaut de la div textZone
var defg0 = '<span id="g0" class="startPoint" onclick="putAnchor(this.id)">&nbsp;</span>'
var defg0inner = '&nbsp;' 
var defgEnd = '<span id="gEnd" onclick="putAnchorOnLast()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'
var defgEndinner = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
var textZoneDefault = defg0+defgEnd
document.getElementById('rep').innerHTML = textZoneDefault

// Position curseur par défaut
var curseurPos = document.getElementsByClassName('startPoint')[0].id



function recupPhon(identifiant) {
    var rectId = identifiant
    var phonId = rectId.replace("rect","phon")
    var repDiv = document.getElementById(ligneCible)

    var newCarte = document.createElement('div')
    newCarte.classList = "carte " + phonId + " noTextClip";// on garde rectId, pour connaître le phonème mais sans appeler les styles de phonochromie-alem.css
    newCarte.draggable = true

    let ponderateur = 1 // pondérateur de taille pour stress
    if (stress1) {
        ponderateur = stress1ponderator
        newCarte.classList.add('stress1')
        setStress(0)
    } else if (stress2) {
        ponderateur = stress2ponderator
        newCarte.classList.add('stress2')
        setStress(0)
    }

    if (phonId.includes('phon_schw')) newCarte.classList.add('schwa')

    if (!identifiant.match(/espace/)) { 
        var newCarteIn = document.createElement('div')
        newCarteIn.classList = "carteIn"
        newCarteIn.style.backgroundImage = `url('../../../media/phonocartes/do_${famille}/${phonId}.png')` //+ `, url('../../../media/phonocartes/do_formes/${phonId}.png')`; //"url('/static/cartes/"+ famille +"/"+ phonId + ".jpg')"}
        newCarte.appendChild(newCarteIn)
    } 
    
    newCarte.setAttribute("id", phonId+"-g"+Math.random().toString(36).substring(2,9))
    newCarte.setAttribute('onclick', "putAnchor(this.id)")

    newCarte.addEventListener("dblclick", function(e) {
        var tgt = e.target
        // on passe le curseurb sur la carte d'avant
        curseurPos = tgt.previousElementSibling.id
        tgt.previousElementSibling.classList.add('startPoint')
        playEffect("delete")
        tgt.remove()
        // saveTrace('del phon '+rectId)
    })

    newCarte.addEventListener('dragstart', () => {
        newCarte.classList.add('dragging')
    })

    newCarte.addEventListener('dragend', () => {
        newCarte.classList.remove('dragging')
        let listCartes = []
        for (i=0; i<repDiv.children.length; i++) listCartes.push(Array.from(repDiv.children[i].classList).join("."))
        // saveTrace(`drag to ${listCartes.join(' ')}`)
    })
    

    // repDiv.appendChild(newCarte)
    insertAfter(document.getElementById(curseurPos), newCarte)
    document.getElementById(curseurPos).classList.remove('startPoint')
    curseurPos = newCarte.id
    document.getElementById(curseurPos).classList.add('startPoint')
    // saveTrace(`add ${famille} ${rectId}`)
    playEffect("select")


    // Mise à jour de la table
    cartes = document.querySelectorAll('.carte') // mise à jour de la liste des cartes sur la table
    
}


var switchStress = false
function toggleSwitchStress() {
    switchStress = !switchStress
    console.log("SwitchStress",switchStress)
}

function putAnchor(identifiant) {
    if (switchStress) {
        // Si le bouton switchStress est activé : putAnchor a pour effet de switcher le stress de la graphie au lieu de positionner le curseur
        if (document.getElementById(identifiant).classList.contains('schwa')) {
            console.log("Cette graphie est un schwa et peut pas être accentuée !")
        } else if (document.getElementById(identifiant).classList.contains('stress1')) { // passe à stress2
            document.getElementById(identifiant).classList.remove('stress1')
            document.getElementById(identifiant).classList.add('stress2')
        } else if (document.getElementById(identifiant).classList.contains('stress2')) { // passe à normal (pas de stress)
            document.getElementById(identifiant).classList.remove('stress2')
        } else {
            document.getElementById(identifiant).classList.add('stress1'); // passe à stress1
        }
    } else {
        // Sinon il positionne simplement le curseur sur la graphie cliquée
        document.getElementById(curseurPos).classList.remove('startPoint')
        curseurPos = identifiant
        console.log('Curseur positionné à '+curseurPos)
        document.getElementById(identifiant).classList.add('startPoint')
    }
}

function putAnchorOnLast() {
    document.getElementById(curseurPos).classList.remove('startPoint')
    var identifiant = document.getElementById('gEnd').previousElementSibling.id
    curseurPos = identifiant
    console.log('Curseur positionné à '+curseurPos)
    document.getElementById(identifiant).classList.add('startPoint')
}

function gotoPrecedent() {
    document.getElementById(curseurPos).classList.remove('startPoint')
    var identifiant = document.getElementById(curseurPos).previousElementSibling.id
    curseurPos = identifiant
    console.log('Curseur positionné à '+curseurPos)
    document.getElementById(identifiant).classList.add('startPoint')
}

function gotoNext() {
    document.getElementById(curseurPos).classList.remove('startPoint')
    var identifiant = document.getElementById(curseurPos).nextElementSibling.id
    curseurPos = identifiant
    console.log('Curseur positionné à '+curseurPos)
    document.getElementById(identifiant).classList.add('startPoint')
}

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling)
}


//fonction effacer
function erasePreviousSpan(){
    document.getElementById('audio').innerHTML = "" // suppression de la mémoire de la synthèse vocale
    var elASupprimer = document.getElementById(curseurPos)
    var previousEl = elASupprimer.previousElementSibling

    if (elASupprimer.id != 'g0') {
        console.log('suppr: '+elASupprimer.id+' | prev: '+previousEl)
        elASupprimer.remove()
        while (previousEl.id == '') {
            console.log('previousEl is null.')
            previousEl = previousEl.previousElementSibling
            console.log('prev: '+previousEl)
            previousEl.nextElementSibling.remove()
        }
        
        playEffect("delete")
        curseurPos = previousEl.id
        document.getElementById(curseurPos).classList.add('startPoint')
        
    } else {
        console.log('g0 ne peut pas être supprimé !')
    }
    
}

// fonction Tout Effacer
function eraseAll() {
    console.log("Reset All")
    playEffect('delete')
    curseurPos = 'g0'
    document.getElementById('rep').innerHTML = textZoneDefault
    document.getElementById('audio').innerHTML = ""
}



////////// CHARGEMENT DE L'ACTIVITÉ /////////////
//////////////////////////////////////////////
var nbmots;
var cptitem = 0;
var currentAudio = ""
var currentAudioFin = "" // audio à lire à la fin, quand réponse correcte
var currentVideo = ""

const rep = document.getElementById('rep')
var btnValider = document.getElementById("btnValider")

if (videoMode) document.getElementById('btnVideo').style.display = 'block';
if (audioMode) document.getElementById('btnAudio').style.display = 'block';
if (random) document.getElementById('btnText').style.display = 'block';
if (syntheseVocale) document.getElementById('divSynthVoc').style.display = 'block';

// LANCEMENT DE L'ACTIVITÉ PAR DEFAUT (TRANSMISE AVEC LE TEMPLATE cf. views.py)
nbmots = mots.length

document.getElementById('nbMots').innerHTML = nbmots;
document.getElementById('fenetreDeLancement').style.display = "block"


// Démarrage de la série
function demarrer() {
    document.getElementById('fenetreDeLancement').style.display = "none"
    document.getElementById('divConsigne').style.display = "block"
    document.getElementById('divReponse').style.display = ""
    cptitem = 0
    if (audioFin == 1) {
        document.getElementById('bravo_audio').style.display = "block"
    }
    saveTrace(`start serie ${serieName}`)
    addStat("phonoplayer",thisPageLang);

    hidePhones()

    loadNext()
}

// charge le mot suivant
function loadNext() {
    document.getElementById("bravo").style.display = "none"
    rep.innerHTML = textZoneDefault
    curseurPos = 'g0'

    if (random) {
        loadWord({random:true})
    } else {
        if (cptitem < nbmots) {
            saveTrace(`next ${mots[cptitem]["motGenerique"]}`)
            curseurPos = document.getElementsByClassName('startPoint')[0].id
            currentAudio = "../../media/audio-uploads/" + (mots[cptitem]["audio"] || mots[cptitem]['audioDeb'])
            currentVideo = mots[cptitem]["video"] || mots[cptitem]['videoDeb']
            currentAudioFin = mots[cptitem]["audio_R"] || mots[cptitem]["audioFin"]

            if (audioDeb==1 && currentAudio.length>0) {
                playAudio()
            }
            if (videoDeb==1 && currentVideo.length>0) {
                document.getElementById('videoSrc').src = '../../media/video-uploads/'+currentVideo
                document.getElementById('video').load()
                playVideo()
            }
            cptitem = cptitem+1
            document.getElementById('cptitem').innerHTML = cptitem

        } else {
            getFelicitations()
            playEffect("success")
            saveTrace(`end serie ${serieName}`)
        } 
    }
}
    
// vérifie la réponse
function checkAnswer() {
    var mot = mots[cptitem-1]
    var phono = mot["phono"] // cptitem-1 parce qu'on a déjà incrémenté cptitem lors de loadNext()
    var erreur = true;
    
    myrep = [];
    // Réinitialisation des feedbacks (décoloration cartes + suppr carte "?")
    rep.childNodes.forEach((el) => {
        el.classList.remove("subPhon");
        el.classList.remove("insPhon");
    })
    rep.querySelectorAll('.delPhon').forEach((el)=> {rep.removeChild(el)})

    rep.childNodes.forEach((el) => { 
        if (el.classList.length > 1) {
            var phonclass = el.classList[1].replace("rect","phon")
            if ([...el.classList].includes('stress1')) phonclass+=' stress1'
            else if ([...el.classList].includes('stress2')) phonclass+=' stress2'
            else if ([...el.classList].includes('schwa')) phonclass+=' schwa'
            myrep.push(phonclass);
        }
    })
    console.log("myrep:",myrep)

    if (!Array.isArray(phono[0])) {
        // Si premier élément n'est pas isArray, c'est qu'il y a qu'une phono enregistrée (ancienne série), dans ce cas on reformatte en liste de phono [ ["a","b"] ]
        phono = [ phono ];
    }
    
    // TOLÉRANCES PHONOLOGIQUES
    // ajouter une référence possible pour certains cas (ex. phon_J → phon_nj)
    phono.forEach( (ref) => {
        if (ref.includes("phon_j_maj")) {
            var newref = []
            ref.forEach((phon) => { newref.push(phon.replace("phon_j_maj", "phon_nj")) })
            phono.push(newref)
        }
    })
    console.log(phono)

    comps = [];
    phono.forEach( (ref) => {
        // pour chaque phono de référence possible
        var comp = compareRepRef(myrep,ref);
        var newComp = {
            "comp": comp,
            "nbok": comp.filter(item => item.type==="OK").length,
            "correct": comp.filter(item => item.type==="OK").length > 0 && comp.filter(item => item.type==="OK").length == comp.length && comp.filter(item => item.type==="OK").length == ref.length
        }
        comps.push(newComp)
    })
    comps.sort((a, b) => { return b.nbok - a.nbok; }); // on met la réponse la plus proche en première (nbok plus grand)
    console.log(comps)

    comps.forEach( comp => { if (comp.correct) erreur=false })

    var listPhonExistants = []
    for (let phono of mots[cptitem-1].phono) {
        for (let phon of phono) listPhonExistants.push(phon);
    }

    // COLORATION DES CARTES
    for (i=0; i<comps[0].comp.length; i++) {
        if (comps[0].comp[i].type == "SUB" || comps[0].comp[i].type == "INS") {
            // si le phon existe dans le mot : orange
            // si le phon n'existe pas dans le mot : rouge
            if (listPhonExistants.includes(rep.childNodes[i+1].classList[1].replace("rect","phon"))) rep.childNodes[i+1].classList.add("subPhon");
            else rep.childNodes[i+1].classList.add("insPhon");

        } else if (comps[0].comp[i].type == "DEL") {
            var newCarte = document.createElement('div')
            newCarte.classList = "carte phon_inconnu delPhon noTextClip";// on garde rectId, pour connaître le phonème mais sans appeler les styles de phonochromie-alem.css
            newCarte.draggable = true
            newCarte.id = "delPhon"+Math.random().toString(36).substring(2,9)
            
            var newCarteIn = document.createElement('div')
            newCarteIn.classList = "carteIn"
            newCarteIn.style.backgroundImage = `url('../../../media/phonocartes/do_bouches/phon_inconnu.png')`;
            newCarte.appendChild(newCarteIn)
            
            newCarte.addEventListener("click", function(e) {
                var tgt = e.target.parentElement;
                // on passe le curseur sur la carte d'avant
                document.getElementById(curseurPos).classList.remove('startPoint')
                console.log("curseurPOS",curseurPos)
                curseurPos = tgt.previousElementSibling.id
                console.log("curseurPOS",curseurPos)
                tgt.previousElementSibling.classList.add('startPoint')
                playEffect("delete")
                tgt.remove()
            })
        
            newCarte.addEventListener('dragstart', () => {
                newCarte.classList.add('dragging')
            })
        
            newCarte.addEventListener('dragend', () => {
                newCarte.classList.remove('dragging')
                let listCartes = []
                for (i=0; i<repDiv.children.length; i++) listCartes.push(Array.from(repDiv.children[i].classList).join("."))
                // saveTrace(`drag to ${listCartes.join(' ')}`)
            })

            if (i<=rep.childNodes.length) rep.insertBefore(newCarte, rep.childNodes[i+1])
            else rep.appendChild(newCarte);
        }
        else if (comps[0].comp[i].type == "OK") rep.childNodes[i+1].classList.add("correctPhon")
    }

    if (!erreur) {
        playEffect("correct")
        setTimeout(playAudio_R(), 2000)
        getBravo(mot)
        saveTrace(`${mot["motGenerique"]}: validation=correct rep=${myrep.join(' ')}`)
        printHistory()
    } else {
        playEffect('wrong')
        saveTrace(`${mot["motGenerique"]}: validation=erreur rep=${myrep.join(' ')}`)
        myHistory[myHistory.length-1].score = 0
    }
    
}

function initAnswer(x=0) {
    if (x>0) { 
        document.getElementById('rep'+x).parentElement.parentElement.remove() 
    } else {
        var repx = document.getElementById('rep');
        repx.innerHTML = "";
        document.querySelectorAll('.divReponseMilieu')[x].appendChild(repx)
    }
    if (!document.body.contains(document.getElementById('rep'))) document.querySelector('.divReponseMilieu').innerHTML = `<div id="rep" class="d-flex align-items-center flex-wrap rep" style="background-color:lightblue"></div>` 
}

function compareRepRef(myrep, ref) {
    // inspiré du script python pour calculer le WER https://github.com/imalic3/python-word-error-rate/blob/master/word_error_rate.py

    // PREPARATION
    // On commence par faire une table au format suivant :
    //        (r e p) 
    //       0 1 2 3
    //  (r   1 0 0 0
    //   e   2 0 0 0
    //   f)  3 0 0 0

    var d = [...Array(ref.length+1).keys()];
    d.forEach((row) => { d[row] = [...Array(myrep.length+1).keys()] })
    for (i=0; i<d.length; i++) {
        for (j=0; j<d[i].length; j++) {
            if (i == 0) d[0][j] = j
            else if (j == 0) d[i][0] = i
            else d[i][j] = 0
        }
    }

    // REMPLISSAGE
    // on compare chaque item de myrep avec ref, si == alors on récupère la valeur d[i-1][j-1], sinon on récupère le minimum entre ( d[i-1][j-1]+1 ; d[i][j-1]+1 ; d[i-1][j]+1 ) 
    for (i=1; i<ref.length+1; i++) {
        for (j=1; j<myrep.length+1; j++) {
            if (ref[i-1] == myrep[j-1]) d[i][j] = d[i-1][j-1]
            else {
                substitution = d[i - 1][j - 1] + 1
                insertion = d[i][j - 1] + 1
                deletion = d[i - 1][j] + 1
                d[i][j] = Math.min(substitution, insertion, deletion)
            }
        }
    }

    // RÉCUPÉRATION DES INS DEL SUB
    // comparer les derniers items de myrep et ref, si pas == alors minimum entre ( d[i-1][j-1]+1 (SUB) ; d[i][j-1]+1 (INS) ; d[i-1][j]+1 (DEL) )
    x = ref.length
    y = myrep.length
    resultat = []

    while (true) {
        if (x == 0 && y == 0) break // FIN
        
        if (y == 0 && x>0) {
            x = x - 1
            resultat.push({ "item":ref[x], "type":"DEL" })
            
        } else if (x == 0 && y>0) {
            y = y - 1
            resultat.push({ "item":myrep[y], "type":"INS" })

        } else if (ref[x-1] == myrep[y-1]) {
            // IDENTIQUE: BONNE RÉPONSE
            x = x - 1
            y = y - 1
            resultat.push({ "item":myrep[y], "type":"OK" })
        } else if (d[x][y] == d[x-1][y-1]+1) {
            // SUBSTITUTION
            x = x - 1
            y = y - 1
            resultat.push({ "item":myrep[y], "type":"SUB", "ref":ref[x] })
        } else if (d[x][y] == d[x - 1][y] + 1) {
            // DELETION
            x = x - 1
            resultat.push({ "item":ref[x], "type":"DEL" })
        } else if (d[x][y] == d[x][y - 1] + 1) {
            // INSERTION
            y = y - 1
            resultat.push({ "item":myrep[y], "type":"INS" })
        } else {
            console.log('We got an error.')
            break
        }
    }

    resultat = resultat.reverse()

    // Si il y a des insertions avant le début du mot, ils ne rentrent pas dans le tableau donc contrôle supplémentaire


    return resultat
}


// AFFICHAGE BRAVO
function getBravo(mot) {
    var bravoDiv = document.getElementById("bravo")
    document.getElementById("bravo_text").innerHTML = ""
    
    if (imageFin=='1' && mot["imageFin"].length>0){
        // document.getElementById("bravo_img").src = "/static/im/series/" + mot["image"]["src"]
        document.getElementById("bravo_img").src = "../../media/image-uploads/" + mot["imageFin"]
    } else {
        document.getElementById("bravo_img").src = "/static/im/series/bravo.png"
    }

    if (phonoFin=='1'){
        // Vieille version (phonoFin)
        var phonographie = mot["phonographies"][0]
        for (i=0; i<phonographie.length; i++){
            document.getElementById("bravo_text").innerHTML += "<span class='graphContours "+ phonographie[i][0] +"'>"+ phonographie[i][1] +"</span>"
        }
    } else {
        if("graphieFin" in mot && !["undefined",""].includes(mot["graphieFin"])) {
            // Nouvelle version
            document.getElementById("bravo_text").innerHTML = mot["graphieFin"]
        } else {
            document.getElementById("bravo_text").innerHTML = "Bravo !"
        }
        if (random) {
            document.getElementById("bravo_text").innerHTML += "<br/><span style='font-size:2rem;font-weight:normal'>"+mot["motGenerique"]+"</span>"
        }
    }
    
    bravoDiv.style.display = "block"
}

// AFFICHAGE FÉLICITATIONS (fin)
function getFelicitations() {
    var bravoDiv = document.getElementById("bravo")
    document.getElementById("bravo_text").innerHTML = ""
    
    document.getElementById("bravo_img").src = "/static/im/series/felicitations.png"
    document.getElementById("bravo_text").innerHTML = "Félicitations !"

    document.getElementById("bravo_next").style.display = "none"
    document.getElementById("bravo_end").style.display = "block"
    document.getElementById("bravo_again").href = window.location.href

    bravoDiv.style.display = "block"
}

// SOUNDS
function playAudio() {
    var audio = new Audio(currentAudio)
    audio.play()
    //saveTrace("playAudio")
}

function playAudioId(i) {
    var audio = new Audio(urls[i][1])
    audio.play()
    saveTrace("Réécoute " + urls[i][0])
}

function playAudio_R() {
    if (audioFin==1 && currentAudioFin.length>0) {
        var audio = new Audio("../../media/audio-uploads/"+currentAudioFin)
        audio.play()
        // saveTrace("playAudioFin")
    }
}

// VIDEO
function playVideo() {
    document.getElementById('videoPlayer').style.display = "block"
    let video = document.getElementById('video')
    if (video.paused) {
        video.play(); 
    } else {
        video.pause(); 
    }
    // saveTrace("playVideo")
}

function playEffect(effect) {
    var audio
    if (effect == "select") audio = new Audio("/static/audio/effets/select.mp3")
    else if (effect == "delete") audio = new Audio("/static/audio/effets/del.mp3")
    else if (effect == "success") audio = new Audio("/static/audio/effets/success.mp3")
    else if (effect == "correct") audio = new Audio("/static/audio/effets/correctAnswer.mp3")
    else if (effect == "wrong") audio = new Audio("/static/audio/effets/wrongAnswer.mp3")
    audio.play()
}

// ENREGISTREMENT TRACES
async function saveTrace(action) {
    if (userName) {
        console.log("ENREGISTREMENT ", action)
        // ON EMBALLE TOUT ÇA
        var colis = {
            appli: thisAppli,
            action: {
                serieName,
                action
            },
            timestamp: new Date().toISOString() 
        }
        // Paramètres d'envoi
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(colis)
        }
        // ENVOI
        const response = await fetch('/_saveTrace/', options);
    }
}


// When the user clicks anywhere outside of the modal, close it
let videoPlayer = document.getElementById('videoPlayer')
window.onclick = function(event) {
    if (event.target == videoPlayer) {
        videoPlayer.style.display = "none"
    }
}

function initGraph() {
    // Rien. Pour éviter une erreur quand on clique dans le fond du tableau
    //recupPhon("espace")
}

// RECUPERATION DU SVG (CLICK FR)
var svgClick = document.getElementById("svgClick");
var svgDoc;
function hidePhones() {
    // get the inner DOM of alpha.svg
    svgDoc = svgClick.getSVGDocument();

    let frphonolist = ["rect_2", "rect_9", "rect_9_nas", "rect_a", "rect_a_maj", "rect_a_maj_nas", "rect_arobase", "rect_b", "rect_d", "rect_dz_maj", "rect_e", "rect_e_maj", "rect_e_maj_nas", "rect_f", "rect_g", "rect_gz", "rect_h_maj", "rect_ij", "rect_i", "rect_j", "rect_k", "rect_ks", "rect_l", "rect_m", "rect_nj", "rect_n", "rect_n_maj", "rect_o", "rect_o_maj", "rect_o_maj_nas", "rect_p", "rect_r_maj", "rect_s", "rect_s_maj", "rect_t", "rect_ts_maj", "rect_u", "rect_v", "rect_wa", "rect_we_maj_nas", "rect_w", "rect_y", "rect_z", "rect_z_maj"]

    if (panneauPartiel) {
        console.log('Désactivation des phonèmes non-cible')
        for (phon in phonActifs) phonActifs[phon] = phonActifs[phon].replace('phon', 'rect')

        // On désactive tous les phonèmes sauf ceux de la liste phonActifs
        for (i=0; i<frphonolist.length; i++) {
            if (!phonActifs.includes(frphonolist[i])) {
                let thisphon = svgDoc.getElementById(frphonolist[i])
                thisphon.style.fillOpacity = .7
                thisphon.classList.remove('svgPhonClick')
                thisphon.onclick = ""
                thisphon.onmouseover = ""
                thisphon.onmouseout = ""
            }
        }
    }

}

// RESTRICTIONS EXPÉ ALEXANDRE -- temporaire
// if (thisURL.match(/.*pA\/play/) || thisURL.match(/.*pAV\/play/) || thisURL.match(/.*serie-bac-a-sable\/play/)) {
//     console.log('Session pour expé Alexandre: restrictions.')
//     document.getElementById('retourHome').style.display = "none"
//     document.getElementById('bravo_quit').remove()
// }

// RESTRICTIONS BAC À SABLE -- pas de série de mots
var bacasable = false
if (thisURL.match(/.*serie-bac-a-sable\/play/)) {
    console.log('Session Bac À Sable')
    bacasable = true
    document.getElementById('toBacASable').style.display = "none"
    document.getElementById('fenetreDeLancement').style.display = "none"
    document.getElementById('divReponse').style.display = ""

    // Suppression divReponseGauche
    document.getElementsByClassName('divReponseGauche')[0].remove()

    // Reformattage divReponseMilieu
    document.getElementsByClassName('divReponseMilieu')[0].classList.add('bacAsable')

    // Mise à jour des boutons
    document.getElementById('btnEnter').style.display = "block"
    document.getElementById('btnValider').style.display = "none"
    saveTrace(`start serie ${serieName}`)
}

var cptline=1
function newLine(el){
    cptline+=1
    var newLine = el.parentElement.parentElement.cloneNode(true)
    newLine.id = "divReponse"+cptline
    newLine.firstElementChild.firstElementChild.innerHTML = textZoneDefault
    newLine.firstElementChild.firstElementChild.id = "rep"+cptline
    
    newLine.lastElementChild.firstElementChild.id = "btnEnter"+cptline;
    newLine.lastElementChild.children[1].id = "divSynthVoc"+cptline;
    newLine.lastElementChild.children[1].firstElementChild.setAttribute('onclick', "playIpa("+cptline+")");
    newLine.lastElementChild.children[1].lastElementChild.id = "debitParole"+cptline;

    el.parentElement.parentElement.parentElement.insertBefore(newLine, el.parentElement.parentElement.nextElementSibling)
    changeLigne(newLine.firstElementChild)
}


// GESTION DU STRESS
let stress1 = false
let stress2 = false
function setStress(nb){
    if (nb == 0) {
        // On passe tout à 0 sans se poser de question
        stress1 = false
        document.getElementById('stress1').classList.remove('stressOn')
        stress2 = false
        document.getElementById('stress2').classList.remove('stressOn')

    } else if (nb == 1) {
        if (!stress1) {
            // Si stress1 n'est pas encore actif
            stress1 = true
            document.getElementById('stress1').classList.add('stressOn')
            
        } else {
            // Si stress1 est déjà actif : on passe à 0
            stress1 = false
            document.getElementById('stress1').classList.remove('stressOn')
            
        }
        stress2 = false
        document.getElementById('stress2').classList.remove('stressOn')

    } else if (nb == 2) {
        stress1 = false
        document.getElementById('stress1').classList.remove('stressOn')
        if (!stress2) {
            stress2 = true
            document.getElementById('stress2').classList.add('stressOn')
            
        } else {
            stress2 = false
            document.getElementById('stress2').classList.remove('stressOn')
            
        }
    }
    
}

function toggleFullScreen() {
    if (document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen) {
        document.exitFullscreen()
        document.getElementById("toggleFullScreenBtn").innerHTML = `<path d="m 5.828,10.172 c -0.1952499,-0.195191 -0.5117501,-0.195191 -0.707,0 -2.6618502,1.94423 -4.096,6.322035 -4.096,1.328 0,-0.666666 -1,-0.666666 -1,0 v 3.975 c 0,0.276142 0.22385763,0.5 0.5,0.5 H 4.5 c 0.6666664,0 0.6666664,-1 0,-1 -5.04044252,0.686128 -0.5920511,-2.175949 1.328,-4.096 0.195191,-0.19525 0.195191,-0.51175 0,-0.707 z m 4.344,-4.344 c 0.19525,0.195191 0.51175,0.195191 0.707,0 2.66185,-1.9442299 4.096,-6.32203458 4.096,-1.328 0,0.6666664 1,0.6666664 1,0 V 0.525 c 0,-0.27614237 -0.223858,-0.5 -0.5,-0.5 H 11.5 c -0.666666,10e-9 -0.666666,1 0,1 5.040443,-0.68612773 0.592051,2.1759489 -1.328,4.096 -0.195191,0.1952499 -0.195191,0.5117501 0,0.707 z"/>`
    } else { 
        document.documentElement.requestFullscreen();
        document.getElementById("toggleFullScreenBtn").innerHTML = `<path d="m 0.20589831,15.794102 c 0.19525,0.195191 0.51175,0.195191 0.707,0 2.66184999,-1.94423 4.09599999,-6.3220349 4.09599999,-1.328 0,0.666666 1,0.666666 1,0 v -3.975 c 0,-0.276143 -0.223858,-0.5000003 -0.5,-0.5000003 h -3.975 c -0.66666599,0 -0.66666599,1.0000003 0,1.0000003 5.040443,-0.686128 0.592051,2.175949 -1.32799999,4.096 -0.195191,0.19525 -0.195191,0.51175 0,0.707 z" />
                                                                    <path d="m 15.709659,0.29034053 c -0.19525,-0.195191 -0.511749,-0.195191 -0.706999,0 -2.66185,1.94422997 -4.096,6.32203517 -4.096,1.32799997 0,-0.66666597 -1.0000002,-0.66666597 -1.0000002,0 v 3.9750003 c 0,0.276143 0.2238582,0.5000003 0.5000002,0.5000003 h 3.975 c 0.666666,0 0.666666,-1.0000004 0,-1.0000004 -5.0404432,0.6861281 -0.592051,-2.1759492 1.327999,-4.09600017 0.195191,-0.19525 0.195191,-0.51175 0,-0.707 z" />`
    }
}