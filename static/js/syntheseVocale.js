var currentIpa = "";
var currentLang = "";
var currentDebit = 80;
var voix = "f"

async function playIpa(line="") {
    
    var synth = false
    console.log("PLAYIPA", thisPageLang)

    if (thisAppli == "phonographe") {
        if (thisPageLang == 'fr' || thisPageLang == 'en' || thisPageLang == 'de'){
            // Si quelque chose est écrit...
            var phonEllist = document.getElementsByClassName('text')
            
            if (phonEllist.length > 0) {
                // on récupère tous les élément classe "text" et on récupère leur classe phon_*
                var ipa = "";
                for (e=0; e<phonEllist.length; e++){
                    // on parse les classe c de l'élement e
                    for (c=0; c<phonEllist[e].classList.length; c++){
                        if (phonEllist[e].classList[c].match(/stress1/)){
                            ipa+= 'ˈ'
                        } else if (phonEllist[e].classList[c].match(/stress2/)){
                            ipa+= 'ˌ'
                        }
                    }
                    
                    for (c=0; c<phonEllist[e].classList.length; c++){
                        // si la classe match "phon_.*"" alors on récupère l'équivalent api
                        if (phonEllist[e].classList[c].match(/phon_.*/)){
                            ipa += phon2api[phonEllist[e].classList[c]];
                        } else if (phonEllist[e].classList[c].match(/pause/)) {
                            // si la classe match "pause" on insert une espace (qui sera convertie en <break> dans text2speech.py)
                            ipa += ' ';
                        }
                    }
                }
                ipa = ipa.trim(); // suppression des espaces en début ou en fin
                if (ipa != currentIpa || currentLang != thisPageLang) { synth = true }
            }
        } else window.alert("Cette fonctionnalité n'est pas encore disponible pour cette langue !");
    
    } else if (thisAppli == "phonoplayer"){
        
        var rep = document.getElementById('rep'+line.toString())
        ipa = ""
        for (i=0; i<rep.children.length; i++) {
            // var repphon = rep.children[i].classList[1].replace("rect","phon")
            // if (repphon != "espace" && repphon != "phon_inconnu") { ipa += phon2api[repphon] }

            let newcontent = '';
            for (c=0; c<rep.children[i].classList.length; c++){
                // si la classe match "phon_.*"" alors on récupère l'équivalent api
                if (rep.children[i].classList[c].match(/phon_.*/)){
                    if (rep.children[i].classList[c] != "espace" && rep.children[i].classList[c] != "phon_inconnu") newcontent += phon2api[rep.children[i].classList[c]];
                }
                if (rep.children[i].classList[c].match(/pause/)) {
                    // si la classe match "pause" on insert une espace (qui sera convertie en <break> dans text2speech.py)
                    newcontent += ' ';
                }
                if (rep.children[i].classList[c].match(/stress1/)) newcontent = 'ˈ' + newcontent
                if (rep.children[i].classList[c].match(/stress2/)) newcontent = 'ˌ' + newcontent
            }
            ipa += newcontent
        }

        ipa = ipa.trim(); // suppression des espaces en début ou en fin
        ipa = ipa.replace(/undefined/g, '')

        if (ipa.length > 0 && ipa != currentIpa) { synth = true }
    
    }

    if (synth) {
        currentLang = thisPageLang;
        console.log(`Lecture de [${ipa}]`);
        currentIpa = ipa;
        
        // ON EMBALLE TOUT ÇA
        var colis = {
            ipa,
            lang: thisPageLang,
            voix: "f",
            'appli': thisAppli
        };
        
        // Paramètres d'envoi
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(colis)
        };
        
        // ENVOI
        const response = await fetch('https://phonographe.alem-app.fr/_playIpa/', options)
        const data = await response.json();
        readResponse(data["audio"], line);
    } else {
        if (document.getElementsByTagName("audio").length>0) {
            var sv_audio = document.getElementsByTagName("audio")[0];
            sv_audio.playbackRate = document.getElementById('debitParole'+line.toString()).value / 100;
            sv_audio.play();
        }
    }

    if (typeof saveTrace !== "undefined") { 
        saveTrace(`playIpa [${ipa}] ${colis.voix} ${colis.lang} ${document.getElementById('debitParole'+line.toString()).value} rep${line}`)
    }
}

function readResponse(response, line) {
    var source = '<source src="data:audio/mpeg;base64,' + response + '" type="audio/mpeg"></source>';
    var sv_audio = '<audio autoplay="true" controls>' + source + '</audio>';
	sv_audio.playbackRate = document.getElementById('debitParole'+line.toString()).value / 100;
    document.getElementById('audio').innerHTML = sv_audio;
}