import re, json

lexique = {}

with open('lexiqueEn.js','r') as lex:
    for line in lex:
        if not re.match('^\s+".*',line):
            continue
        line = line.strip()
        mot = line.replace('"','').replace(',','').replace(' ','')
        if mot not in lexique.keys():
            lexique[mot] = [[]]
        else:
            print("Doublon détecté dans lexiqueEn.js", mot)

cptmotsSyl = 0

with open("/home/sylvain/RESSOURCES_TAL/CMUdict/cmudict-0.7b","r", encoding="latin1") as inf:
    for line in inf:
        if line.startswith(";;;"):
            continue
        line = line.strip()
        l = line.split('  ')
        if len(l)==2:
            w, t = l 
            w = re.sub(r'\(\d\)','', w.lower())
            gab = re.sub(r'[^012]','',t)
            if len(gab)>0:
                cptmotsSyl += 1
                if w in lexique.keys() and len(gab) not in lexique[w][0]:
                    lexique[w][0].append(len(gab))

                            
print(cptmotsSyl)

print("Words still empty from lexiqueEn.js:")
for w, c in lexique.items():
    if len(c[0])==0:
        print(w, c)

print("Export...")
with open("lexiqueEnSyll.json", 'w', encoding='utf-8') as outf:
    json.dump(lexique, outf, ensure_ascii=False)

print('Done.')      
                