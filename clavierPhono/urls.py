"""clavierPhono URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views
from clavier import views as clavier_views
from users import views as user_views
from django.views.decorators.csrf import csrf_exempt

# Pour accéder aux média
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', user_views.register, name='register'),
    path('login/', user_views.loginView, name='login'), #auth_views.LoginView.as_view(template_name="users/login.html"), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name="users/logout.html"), name='logout'),
    path('profile/', user_views.profile, name='profile'),
    path('profile/delete', user_views.delete_account, name='delete_account'),

    path('reset_password/', auth_views.PasswordResetView.as_view(template_name="registration/password_reset_form.html"), name='password_reset'),
    path('reset_password/done', auth_views.PasswordResetDoneView.as_view(template_name="registration/password_reset_done.html"), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name="registration/password_reset_confirm.html"), name='password_reset_confirm'),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(template_name="registration/password_reset_complete.html"), name='password_reset_complete'),
    
    path('player/createSerie/', user_views.createSerie, name='createSerie'),
    path('player/listMediaFiles/', csrf_exempt(user_views.listMediaFiles), name='listeMediaFiles'),
    path('player/saveNewSerie/', csrf_exempt(user_views.saveNewSerie), name='saveNewSerie'),
    path('player/saveSerie/', csrf_exempt(user_views.saveSerie), name='saveSerie'),
    path('player/uploadAudio/', user_views.uploadAudio, name='uploadAudio'),
    path('player/uploadVideo/', user_views.uploadVideo, name='uploadVideo'),
    path('player/uploadImage/', user_views.uploadImage, name='uploadImage'),

    path('', clavier_views.newPage, name="home"),
    path('fr/', clavier_views.newPage),
    path('en/', clavier_views.newPage),
    path('zh/', clavier_views.newPage),
    path('dz/', clavier_views.newPage),
    path('shy/', clavier_views.newPage),
    path('de/', clavier_views.newPage),
    path('id-<str:pageId>/', clavier_views.loadPage),
    path('id-<str:pageId>/page', clavier_views.getPageContent),
    path('<str:pageLang>/id-<str:pageId>/', clavier_views.loadPage),
    path('export/', csrf_exempt(clavier_views.save2db)),

    path('player/', clavier_views.openPlayerHome, name='player'),
    path('player/getSeries/', csrf_exempt(clavier_views.getSeries), name='getSeries'),
    path('player/serie-<str:pk>/info', clavier_views.infoSerie, name='serie-info'),
    path('player/serie-<str:pk>/play', clavier_views.openPlayerPhono, name='serie-play'),
    path('player/serie-<str:pk>/edit', user_views.editSerie, name='serie-edit'),
    path('player/serie-<str:pk>/copy', user_views.copySerie, name='serie-copy'),

    path('player/getAlemAudioList/', csrf_exempt(clavier_views.getAlemAudioList), name='getAlemAudioList'),
    path('player/getAlemAudio/', csrf_exempt(clavier_views.getAlemAudio), name='getAlemAudio'),
    path('player/initPhonTrack/', csrf_exempt(clavier_views.initPhonTrack), name='initPhonTrack'),
    path('player/getNextFrom/', csrf_exempt(clavier_views.getNextFrom), name='getNextFrom'),

    path('_delSerie/', csrf_exempt(user_views.delSerie)),
    path('_delFile/', csrf_exempt(user_views.removeFile)),
    path('_saveTrace/', csrf_exempt(user_views.saveTrace)),
    path('_playIpa/', csrf_exempt(clavier_views.playIpa)),
    path('_getAppStats/', csrf_exempt(user_views.getAppStats)),
    path('_addRec/', csrf_exempt(user_views.addRec)),

    path('_getStats/', csrf_exempt(user_views.getTracesStats)),
    path('_getSynthVocTraces/', csrf_exempt(user_views.getSynthVocTraces)),
    path('_getSynthVocTracesJson/', csrf_exempt(user_views.getSynthVocTracesJson)),
    path('_getSynthVocRecords/', csrf_exempt(user_views.getSynthVocRecords)),

    path('users/', user_views.usersPage, name='users'),
    path('users/<str:username>/', user_views.userInfo, name='userInfo'),
    path('downloadTraces/', csrf_exempt(user_views.downloadTraces), name='downloadTraces'),
    path('addStat/', csrf_exempt(user_views.addStat)),

    path('stats/', user_views.appStatVis) # Page d'affichage des stats d'accès aux différentes applis ALeM (attention c'est lourd)
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
