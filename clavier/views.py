from django.shortcuts import render, HttpResponseRedirect
from django.http import JsonResponse
from django.views.generic import ListView, DetailView
from django.forms.models import model_to_dict

from .models import Serie

from .text2speech import lambda_handler
from users.views import saveTraceSynthVoc
from users.models import SynthVocRecord, User
from user_agents import parse
from django.db.models import Q
from django.core import serializers

from static.scripts.phonoWalker import fromTransAndHistGetNext, getWordsFromTrans, getLang2phonWeights

import subprocess, json, os, tempfile, re, requests, random
import urllib.request as urlreq
import urllib.parse as urlparse

##
## Fichier listant les views principales :
## affichage des pages de l'appli PhonoGraphe/Player.
## Les views relatives aux utilisateurs, gestions des séries et des traces sont listées dans users/views.py
##

# Dossier contenant les enregistrements audio de mots en anglais provenant de Oxford Learners' Dictionary
audioOxfordFolder = "../media/oxfordLearnerDictionary5000/"

### Chargement de la base de données locale (pages enregistrées)
dbFile = '../db/db_phonographe.json'
dbPhonographe = {}
dbActivities = {}
with open(dbFile, "r") as f:
    dbPhonographe = json.load(f)
print('La base de données locale contient',len(dbPhonographe),'pages enregistrées.')


### Chargement des Fidels (fichiers de correspondance phonie-graphie)
with open('static/json/fidel_do.json') as json_data:
    fidelDo = json.load(json_data)
with open('static/json/fidel_pronsci.json') as json_data:
    fidelPS = json.load(json_data)
with open('static/json/fidel_pronsciEnBr.json') as json_data:
    fidelEnPSUK = json.load(json_data)
fidels = {
    "fidelDo": fidelDo,
    "fidelPS": fidelPS,
    "fidelEnPSUK": fidelEnPSUK
}

#################################################################
#################################################################

#############
############# CLAVIER ÉCRITURE EN COULEURS
#############

# Nouvelle page vierge (clavier phonographique)
def newPage(request):
    updateTimeStr = updateTime()
    dataFidels = json.dumps(fidels)
    return render(request, 'clavier.html', {'updateTime': updateTimeStr, 'fidels':dataFidels, 'dataPage':0, 'titrePage':'phonographe'})

# Charger une page enregistrée dans la base de données locale
def loadPage(request, pageId):
    pageId = 'id-'+pageId
    dataFidels = json.dumps(fidels)
    if pageId in dbPhonographe.keys():
        print(pageId,'trouvé dans la base de données.')
        dataPage = json.dumps(dbPhonographe[pageId])
        updateTimeStr = updateTime()
        return render(request, 'clavier.html', {'updateTime': updateTimeStr, 'fidels':dataFidels, 'dataPage':dataPage})
    else:
        return HttpResponseRedirect('/')

# Récupérer le contenu brut d'une page enregistrée (format JSON)
def getPageContent(request, pageId):
    pageId = 'id-'+pageId
    print(pageId)
    if pageId in dbPhonographe.keys():
        print(pageId,'trouvé dans la base de données.')
        return JsonResponse(dbPhonographe[pageId])


# Fonctions pour afficher la date de la dernière mise à jour du PhonoGraphe
def get_git_date():
    cache_file = '/tmp/git_date_cache.txt'
    if os.path.exists(cache_file):
        with open(cache_file, 'r') as f:
            return f.read().strip()
    else:
        git_date = subprocess.check_output(["git", "log", "-1", "--format=%cd", "--date=short"]).decode('utf-8').strip()
        with open(cache_file, 'w') as f:
            f.write(git_date)
        return git_date

def updateTime():
    return "(Last update: "+str(get_git_date())+")"


# Récupération IP client
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

# Enregistrer la page dans la base de données locale
def save2db(request):
    ## Réception du colis
    colis = json.loads(request.body)
    ipCli = get_client_ip(request)
    
    ## Traitements
    pageId = colis['pageId']
    print(pageId)
    if pageId not in dbPhonographe.keys(): dbPhonographe[pageId] = {}
    dbPhonographe[pageId] = {
        "pageId": pageId,
        "ipCli": ipCli,
        "dateCreation": colis['dateCreation'],
        "dateModif": colis['dateModif'],
        "textZoneContent": colis['textZoneContent'],
        "lang": colis['lang'],
        "panneau": colis['panneau'],
        "fidel": colis['fidel']
    }

    ## Réécriture de la base de données
    with tempfile.NamedTemporaryFile(dir='.', delete=False, mode="w") as tmp:
        json.dump(dbPhonographe, tmp, ensure_ascii=False, indent=4)
        os.replace(tmp.name, dbFile)
    print("Base de données mise à jour.")

    ## Envoi réponse au client
    rep = {
        "reponse":"Serveur: Colis bien reçu !",
        "identifiant": pageId
    }
    
    return JsonResponse(rep)

#############
############# PLAYER
#############

# Afficher la page des séries/activités existantes
def openPlayerHome(request):
    updateTimeStr = updateTime()
    userInfo = {
        "nom":request.user.username,
        "superadmin":False,
        "admin":False,
        "droits": []
    }

    if request.user.groups.exists():
        if "superadmin" in [x.name for x in request.user.groups.all()]: userInfo["superadmin"] = True
        if "admin" in [x.name for x in request.user.groups.all()]: userInfo["admin"] = True
    
    return render(request, 'playerHome.html', {'updateTime': updateTimeStr, 'titrePage': 'phonoplayer', 'userInfo': json.dumps(userInfo)})


# Charger la liste des séries/activités existantes
def getSeries(request):
    if str(request.user) != "AnonymousUser":
        return JsonResponse({"users": serializers.serialize('json', User.objects.all(), fields=("id","username")),
                    "series":serializers.serialize('json', Serie.objects.filter(Q(visibility="public") | Q(visibility__isnull=True) | Q(auteur=request.user)), fields=('code','lang','nom','auteur','description','dateCreation','dateModification','tag','visibility'))})
    else:
        return JsonResponse({"users": serializers.serialize('json', User.objects.all(), fields=("id","username")),
                    "series":serializers.serialize('json', Serie.objects.filter(Q(visibility="public") | Q(visibility__isnull=True)), fields=('code','lang','nom','auteur','description','dateCreation','dateModification','tag','visibility'))})

# Récupérer les infos d'une série/activité spécifique
def infoSerie(request, pk):
    s = Serie.objects.filter(code=pk).first()
    serie = {
        "nom":s.nom,
        "dateCreation": s.dateCreation.strftime("%Y-%m-%d %H:%M:%S"),
        "dateModification": s.dateModification.strftime("%Y-%m-%d %H:%M:%S"),
        "code": s.code,
        "description": s.description,
        "auteur": s.auteur.username,
        "auteurIm": s.auteur.profile.image.url,

        "lang": s.lang if hasattr(s, 'lang') else 'fr',

        "audioDeb": s.audioDeb,
        "imageDeb": s.imageDeb,
        "videoDeb": s.videoDeb,
        "graphieDeb": s.graphieDeb,
        
        "audioFin": s.audioFin,
        "imageFin": s.imageFin,
        "videoFin": s.videoFin,
        "graphieFin": s.graphieFin,
        "phonoFin": s.phonoFin,
        
        "syntheseVocale": s.syntheseVocale,
        "panneauPartiel": s.panneauPartiel,
        "phonVisibles": s.phonVisibles,
        
        "mots": s.mots
    }
    return JsonResponse({ "serieInfo":serie })

# Charger une série/activité (jouer)
def openPlayerPhono(request, pk):
    updateTimeStr = updateTime()
    s = Serie.objects.filter(code=pk).first()
    serie = {
        "nom":s.nom,
        "dateCreation": s.dateCreation.strftime("%Y-%m-%d %H:%M:%S"),
        "dateModification": s.dateModification.strftime("%Y-%m-%d %H:%M:%S"),
        "code": s.code,
        "description": s.description,
        "auteur": s.auteur.username,

        "lang": s.lang if hasattr(s, 'lang') else 'fr',

        "audioDeb": s.audioDeb,
        "imageDeb": s.imageDeb,
        "videoDeb": s.videoDeb,
        "graphieDeb": s.graphieDeb,
        
        "audioFin": s.audioFin,
        "imageFin": s.imageFin,
        "videoFin": s.videoFin,
        "graphieFin": s.graphieFin,
        "phonoFin": s.phonoFin,
        
        "syntheseVocale": s.syntheseVocale,
        "panneauPartiel": s.panneauPartiel,
        "phonVisibles": s.phonVisibles,
        
        "mots": s.mots
    }
    print(serie)
    print("Demande ouverture série", serie["nom"], ' (', serie["code"], ')')
    return render(request, 'playerPhono.html', {'updateTime': updateTimeStr, "serie": serie, "serieJson": json.dumps(serie), 'titrePage': 'phonoplayer'})


# Synthèse vocale phonologique
def playIpa(request):
    colis = json.loads(request.body)
    ipa = colis['ipa']
    lang = colis['lang']
    voix = colis['voix']

    print(ipa, lang, voix)
    record = SynthVocRecord.objects.filter(ipa=ipa).filter(lang=lang).filter(voix=voix)
    if len(record)>0:
        print("Un enregistrement existe déjà dans la base de données. Envoi de cet enregistrement.")
        audioBytes = record[0].audio
        record[0].cptEcoute += 1
        record[0].save()
    else:
        print("Pas d'enregistrement existant. Demande de synthétisation.")

        if lang == "fr":
            loc = "Celine" if voix == "f" else "Mathieu"
        elif lang == "en":
            loc = "Amy" if voix == "f" else "Brian"
        elif lang == "de":
            loc = "Marlene" if voix == "f" else "Hans"
        elif lang == "zh":
            loc = "Zhiyu" if voix == "f" else "Zhiyu"

        print("Lecture de ["+ipa+"] débit 80%")
        audioBytes, lenssml = lambda_handler(ipa, loc)

        saveTraceSynthVoc({
            "user" : request.user,
            "userAgent" : parse(request.META['HTTP_USER_AGENT']),
            "userIp" : get_client_ip(request),
            "appli" : colis['appli'],
            "lenssml" : lenssml
        })

        newRecord = SynthVocRecord(ipa=ipa, lang=lang, voix=voix, cptEcoute=0, audio=audioBytes)
        newRecord.save()

    data = {
            "audio": audioBytes
    }

    return JsonResponse(data)




######
###### SÉRIES AUTOMATIQUES
###### (mots générés au fur et à mesure)
######

# Renvoie la liste des audio disponibles sur le server ALEM pour les séries automatiques
def getAlemAudioList(request):
    colis = json.loads(request.body)
    lang = colis['lang']

    if lang=="en":
        alemAudioList = re.findall( "^(\w+)__.*","\n".join(os.listdir(audioOxfordFolder)), re.MULTILINE)
    else:
        alemAudioList = []

    return JsonResponse({ "alemAudioList": list(set(alemAudioList)) })


# Récupération du fichier audio sur ALEM (anglais uniquement pour l'instant)
def getAlemAudio(request):
    colis = json.loads(request.body)
    mot = colis['inText']
    lang = colis['lang']
    
    data = {
        "urls": [],
        "phono": []
    }

    if lang == "en":
        files = re.findall( "^"+mot +"__(\w+).*","\n".join(os.listdir(audioOxfordFolder)), re.MULTILINE)
        for file in files:
            data['urls'].append([re.sub("_\d.*","",file), "../"+audioOxfordFolder + mot+'__'+file+".mp3"])

    return JsonResponse(data)


# Pour les séries automatiques
# Charger un mot initial aléatoire à partir de la liste ci-dessous :
lang2initialWord = {}

with open('static/temp/initialWordEn.json') as json_data:
    lang2initialWord["en"] = json.load(json_data)["initialWord"]
    
with open('static/temp/initialWordFr.json') as json_data:
    lang2initialWord["fr"] = json.load(json_data)["initialWord"]

# Charger le mot suivant dans les séries automatiques
def getNextFrom(request):

    colis = json.loads(request.body)
    myHistory = colis['myHistory']
    lang = colis['lang']

    if len(myHistory)>0:
        previousTranscription = myHistory[-1]["api"]
        print("Previous transcription:",previousTranscription)
    else:
        # previousTranscription = ['ɹ', 'ˈaɪ', 't']
        previousTranscription = lang2initialWord[lang][random.randrange(len(lang2initialWord[lang]))]
        nextItem = getWordsFromTrans(previousTranscription, lang)
        print("Random word:",nextItem)
        return JsonResponse({ "nextItem" : nextItem })

    # nextItem = [['ɹ', 'ˈaɪ', 't'], ["right", "rite", "wright", "write"]]
 
    nextItem = fromTransAndHistGetNext(previousTranscription, myHistory, lang)
    
    while nextItem==0 and len(myHistory)>0:
        myHistory.pop()
        previousTranscription = myHistory[-1]["api"]
        nextItem = fromTransAndHistGetNext(previousTranscription, myHistory, lang)
    if nextItem==0:
        # If still 0 (not more wore in History) then ask for random word
        print("No more history. Get random initialWord...")
        previousTranscription = lang2initialWord[lang][random.randrange(len(lang2initialWord[lang]))]
        nextItem = fromTransAndHistGetNext(previousTranscription, myHistory, lang)

    data = { "nextItem" : nextItem }

    return JsonResponse(data)

# Initialisation PhonTrack (suivi des erreurs phonologiques de l'utilisateur)
# Renvoie un dictionnaire des phonèmes de la langue cible avec pour chaque phonème
# la fréquence absolue (fréquence du dictionnaire), la fréquence de vues dans la série courante (init à 0), 
# la liste des phonèmes avec lesquels il y a eu confusion (init à liste vide)
def initPhonTrack(request):
    colis = json.loads(request.body)
    lang = colis['lang']

    phon2weight = getLang2phonWeights(lang)
    phonTrack = {}
    for phon, weight in phonTrack.items():
        phonTrack[phon] = {
            "absolute_weigth" : weight,
            "freq" : 0,
            "confused_with" : {}
        }

    return JsonResponse(phonTrack)