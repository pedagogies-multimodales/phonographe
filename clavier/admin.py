from django.contrib import admin
from .models import Serie, Audio, Image, SerieTag, SerieVisibility

# TUTO : https://www.youtube.com/watch?v=g5DTIiFAiSk 
admin.site.site_header = 'Tableau de bord du PhonoGraphe'

class SerieAdmin(admin.ModelAdmin):
    list_display = ('nom', 'auteur', 'dateCreation')
    list_filter = ('auteur',)
    fields = [field.name for field in Serie._meta.get_fields()]
    fields.remove('id')
    fields.remove('dateCreation')
    fields.remove('dateModification')
    readonly_fields = ('dateCreation', 'dateModification',)

# Register your models here.
admin.site.register(Serie, SerieAdmin)
admin.site.register(SerieTag)
admin.site.register(SerieVisibility)
admin.site.register(Audio)
admin.site.register(Image)