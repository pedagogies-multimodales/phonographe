from django.apps import AppConfig


class ClavierConfig(AppConfig):
    name = 'clavier'
