from djongo import models
from django.contrib.auth.models import User
from PIL import Image as PilImage

class SerieTag(models.Model):
    nom = models.CharField(max_length=20, primary_key=True)
    def __str__(self):
        return self.nom

class SerieVisibility(models.Model):
    nom = models.CharField(max_length=20, primary_key=True)
    def __str__(self):
        return self.nom

class Serie(models.Model):
    dateCreation = models.DateTimeField(auto_now_add=True, auto_now=False)
    dateModification = models.DateTimeField(auto_now_add=False, auto_now=True)
    nom = models.CharField(default='nouvelle série', max_length=100)
    code = models.CharField(default='nouvelle_serie', max_length=100)
    description = models.TextField(blank=True, max_length=500)
    auteur = models.ForeignKey(User, on_delete=models.PROTECT)
    lang = models.CharField(default='fr', max_length=5)
    tag = models.ForeignKey(SerieTag, on_delete=models.PROTECT)
    visibility = models.ForeignKey(SerieVisibility, on_delete=models.PROTECT)

    panneau = models.CharField(default='phonoFrDo', max_length=50)
    fidel = models.CharField(default='fidelDo', max_length=50)
    typeActivite = models.CharField(default='phonologique', max_length=50) # phonologique / phonographique /...
    droitsModif = models.TextField(default='[]') # liste des users ayant droit de modification autres que l'auteur
    
    audioDeb = models.CharField(default='0', max_length=1)
    imageDeb = models.CharField(default='0', max_length=1)
    videoDeb = models.CharField(default='0', max_length=1)
    graphieDeb = models.CharField(default='0', max_length=1)
    
    audioFin = models.CharField(default='0', max_length=1)
    imageFin = models.CharField(default='0', max_length=1)
    videoFin = models.CharField(default='0', max_length=1)
    graphieFin = models.CharField(default='0', max_length=1)
    phonoFin = models.CharField(default='0', max_length=1)
    
    syntheseVocale = models.CharField(default='0', max_length=1)
    panneauPartiel = models.CharField(default='0', max_length=1)
    phonVisibles = models.TextField(default='[]')
    mots = models.TextField(default='[]')


class Audio(models.Model):
    nom = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    upload_by = models.ForeignKey(User, on_delete=models.PROTECT)
    upload_at = models.DateTimeField(auto_now_add=True)
    source = models.CharField(max_length=500, blank=True)
    used_by = models.TextField(default="[]") # Liste des codes de séries qui utilisent ce média
    file = models.FileField(upload_to="audio-uploads/")

    def __str__(self):
        return self.nom


class Video(models.Model):
    nom = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    upload_by = models.ForeignKey(User, on_delete=models.PROTECT)
    upload_at = models.DateTimeField(auto_now_add=True)
    source = models.CharField(max_length=500, blank=True)
    used_by = models.TextField(default="[]") # Liste des codes de séries qui utilisent ce média
    file = models.FileField(upload_to="video-uploads/")

    def __str__(self):
        return self.nom


class Image(models.Model):
    nom = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    upload_by = models.ForeignKey(User, on_delete=models.PROTECT)
    upload_at = models.DateTimeField(auto_now_add=True)
    source = models.CharField(max_length=500, blank=True)
    used_by = models.TextField(default="[]") # Liste des codes de séries qui utilisent ce média
    file = models.FileField(upload_to="image-uploads/")

    def __str__(self):
        return self.nom

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = PilImage.open(self.file.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.file.path)

# Stucture Mot :
{
    "motGenerique": "ponte",
    "phono": [
        ["phon_p", "phon_o_maj_nas", "phon_t"]
    ],

    "audioDeb" : "filename.ext",
    "imageDeb" : "filename.ext",
    "videoDeb" : "filename.ext",
    "graphieDeb" : "text",

    "audioFin" : "filename.ext",
    "imageFin" : "filename.ext",
    "videoFin" : "filename.ext",
    "graphieFin" : "text",

    "phonographies": [
         [["phon_f","f"],["phon_u","ou"],["phon_r_maj","r"],["phon_s_maj","ch"],["phon_e_maj","e"],["phon_t","tte"]]
    ]
}