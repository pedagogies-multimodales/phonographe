import base64
import boto3
import os
from contextlib import closing

# Source : http://ipa-reader.xyz/

# Liste des phonèmes acceptés pour le français : https://docs.aws.amazon.com/polly/latest/dg/ph-table-french.html
# Liste des tags ssml disponibles : https://docs.aws.amazon.com/polly/latest/dg/supportedtags.html#s-tag 

def lambda_handler(ipa, loc):
    # init polly
    polly = boto3.client("polly")

    # get submitted text string and selected voice
    text = ipa
    voice = loc

    # strip out slashes if submitted with text string
    if "/" in text:
        text = text.replace("/", "")

    # generate phoneme tag for polly to read
    text = text.replace(' ', "'></phoneme><break/><phoneme alphabet='ipa' ph='")
    # phoneme = "<prosody rate='"+debit+"%'><phoneme alphabet='ipa' ph='"+text+"'></phoneme></prosody>" #<prosody rate='85%'></prosody>
    phoneme = "<prosody rate='80%'><phoneme alphabet='ipa' ph='"+text+"'></phoneme></prosody>" #<prosody rate='85%'></prosody>
    
    # send to polly, requesting mp3 back
    response = polly.synthesize_speech(
        OutputFormat="mp3",
        TextType="ssml",
        Text=phoneme,
        VoiceId=voice
    )

    print(response)
    # encode polly's returned audio stream as base64 and return
    if "AudioStream" in response:
        
        with closing(response["AudioStream"]) as stream:
            audio = base64.encodebytes(stream.read())

        return (audio.decode("ascii"),len(phoneme))