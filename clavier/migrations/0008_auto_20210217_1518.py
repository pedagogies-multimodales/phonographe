# Generated by Django 3.0.5 on 2021-02-17 15:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clavier', '0007_auto_20210216_2036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serie',
            name='description',
            field=models.TextField(blank=True, max_length=500),
        ),
        migrations.AlterField(
            model_name='serie',
            name='mots',
            field=models.TextField(default='[]'),
        ),
    ]
